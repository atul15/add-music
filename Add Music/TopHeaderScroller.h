//
//  TopHeaderScroller.h
//  bridgingEg
//
//  Created by Mac-Mini on 08/01/19.
//  Copyright © 2019 Fortune. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TopHeaderScroller : UIView
@property(nonatomic) NSArray *jsonTopBannerList;
-(void)toStartStopTimer:(BOOL)toStop;
@end

NS_ASSUME_NONNULL_END
