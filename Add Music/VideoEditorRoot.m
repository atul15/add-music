//
//  VideoEditorRoot.m
//  RecordAndUpload
//
//  Created by Mac-Mini on 24/01/19.
//  Copyright © 2019 Fortune. All rights reserved.
//

#import "VideoEditorRoot.h"
@import AVKit;
@interface VideoEditorRoot ()
{
    UIButton *btnPlay,*btnFullPage;
    CGSize scrSize;
    UIView *fullPagePlayerView;
    BOOL ifFullPageVisible;
}
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *topPlayerView;
@property (weak, nonatomic) IBOutlet UIView *downSideMainView;
@property(nonatomic) AVPlayer *halfPlayer,*fullpagePlayer;
@property(nonatomic)AVPlayerItem *playerItemTop,*playertemFullPage;
@property(nonatomic)AVPlayerLayer *playerLayerTop,*playerLayerFullPage;
@end

@implementation VideoEditorRoot

- (void)viewDidLoad
{
    [super viewDidLoad];
    ifFullPageVisible=NO;
    // Do any additional setup after loading the view.
    fullPagePlayerView=nil;
    self.fullpagePlayer=nil;
    scrSize=[UIScreen mainScreen].bounds.size;
    self.halfPlayer=[AVPlayer playerWithURL:_selectedVideoUrl];
    self.playerLayerTop=[AVPlayerLayer playerLayerWithPlayer:self.halfPlayer];
    self.playerLayerTop.frame = _topPlayerView.bounds;
    
    self.playerLayerTop.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [_topPlayerView.layer addSublayer:self.playerLayerTop];
    self.halfPlayer.muted = true;
    
    [self.playerLayerTop setBackgroundColor:[UIColor whiteColor].CGColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.halfPlayer currentItem]];
    [self.halfPlayer play];
    
    btnPlay=[[UIButton alloc] init];
    [_topPlayerView addSubview:btnPlay];
    [btnPlay setBackgroundColor:[UIColor clearColor]];
    [btnPlay addTarget:self action:@selector(btnPlayClicked) forControlEvents:UIControlEventTouchUpInside];
    [btnPlay setUserInteractionEnabled:NO];
    
    
    btnFullPage=[[UIButton alloc] init];
    [_mainView addSubview:btnFullPage];
    [btnFullPage setBackgroundColor:[UIColor redColor]];
    [btnFullPage addTarget:self action:@selector(btnFullPageClicked) forControlEvents:UIControlEventTouchUpInside];
    // [btnFullPage setUserInteractionEnabled:NO];
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    btnPlay.frame=CGRectMake((scrSize.width/2)-20, (_topPlayerView.frame.size.height/2)-20, 40, 40);
    
    //btnFullPage.frame=CGRectMake(scrSize.width-60, 20, 40, 40);
    btnFullPage.frame=CGRectMake(scrSize.width-60, (_topPlayerView.frame.origin.y+_topPlayerView.frame.size.height)-80, 40, 40);
    
    
}
-(void)btnPlayClicked
{
    [btnPlay setUserInteractionEnabled:NO];
    [btnPlay setBackgroundColor:[UIColor clearColor]];
    [self.halfPlayer play];
}
-(void)btnFullPageClicked
{
    NSLog(@"btnFullPageClicked");
    if(!ifFullPageVisible)
    {
        NSLog(@"btnFullPageClicked !ifFullPageVisible");
        [self.halfPlayer pause];
        if(fullPagePlayerView==nil)
        {
            fullPagePlayerView=[[UIView alloc] initWithFrame:CGRectMake(_topPlayerView.frame.origin.x, _topPlayerView.frame.origin.y, _topPlayerView.frame.size.width, _topPlayerView.frame.size.height)];
            [_mainView addSubview:fullPagePlayerView];
        }
        [_mainView bringSubviewToFront:btnFullPage];
        [fullPagePlayerView setBackgroundColor:[UIColor cyanColor]];
        if(self.fullpagePlayer!=nil)
        {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[self.fullpagePlayer currentItem]];
            [self.fullpagePlayer pause];
            self.fullpagePlayer=nil;
            [self.playerLayerFullPage removeFromSuperlayer];
            self.playerLayerFullPage=nil;
        }
        
        NSLog(@"_topPlayerView.frame.origin.x=%f _topPlayerView.frame.origin.y=%f _topPlayerView.frame.size.width=%f _topPlayerView.frame.size.height=%f",_topPlayerView.frame.origin.x, _topPlayerView.frame.origin.y, _topPlayerView.frame.size.width, _topPlayerView.frame.size.height);
        
        NSLog(@"fullPagePlayerView.frame.origin.x=%f fullPagePlayerView.frame.origin.y=%f fullPagePlayerView.frame.size.width=%f fullPagePlayerView.frame.size.height=%f",fullPagePlayerView.frame.origin.x, fullPagePlayerView.frame.origin.y, fullPagePlayerView.frame.size.width, fullPagePlayerView.frame.size.height);
        
        
        self.fullpagePlayer=[AVPlayer playerWithURL:_selectedVideoUrl];
        self.playerLayerFullPage=[AVPlayerLayer playerLayerWithPlayer:self.fullpagePlayer];
        self.playerLayerFullPage.frame = fullPagePlayerView.frame;
        [self.playerLayerFullPage setBackgroundColor:[UIColor blueColor].CGColor];
        self.playerLayerFullPage.videoGravity = AVLayerVideoGravityResizeAspectFill;
        [fullPagePlayerView.layer addSublayer:self.playerLayerFullPage];
        self.fullpagePlayer.muted = true;
        
        [self.playerLayerTop setBackgroundColor:[UIColor whiteColor].CGColor];
        
        self.fullpagePlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd2:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:[self.fullpagePlayer currentItem]];
        
        
        [btnPlay setUserInteractionEnabled:NO];
        [btnPlay setBackgroundColor:[UIColor clearColor]];
        
        [self.fullpagePlayer play];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            fullPagePlayerView.frame=CGRectMake(fullPagePlayerView.frame.origin.x, fullPagePlayerView.frame.origin.y, fullPagePlayerView.frame.size.width, _mainView.frame.size.height);
            self.playerLayerFullPage.frame = fullPagePlayerView.frame;
            btnFullPage.frame=CGRectMake(btnFullPage.frame.origin.x, _mainView.frame.size.height-(btnFullPage.frame.size.height+30), btnFullPage.frame.size.width, btnFullPage.frame.size.height);
        }
                         completion:^(BOOL finished)
         {
             
             self->ifFullPageVisible=YES;
             
         }];
        
    }
    else
    {
        NSLog(@"btnFullPageClicked ifFullPageVisible");
        [UIView animateWithDuration:0.3 animations:^{
            
            fullPagePlayerView.frame=CGRectMake(_topPlayerView.frame.origin.x, _topPlayerView.frame.origin.y, _topPlayerView.frame.size.width, _topPlayerView.frame.size.height);
            self.playerLayerFullPage.frame = fullPagePlayerView.frame;
            
            
            
            btnFullPage.frame=CGRectMake(scrSize.width-60, (_topPlayerView.frame.origin.y+_topPlayerView.frame.size.height)-80, 40, 40);
            
            
        }
                         completion:^(BOOL finished)
         {
             self->ifFullPageVisible=NO;
             
             if(self.fullpagePlayer!=nil)
             {
                 [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[self.fullpagePlayer currentItem]];
                 [self.fullpagePlayer pause];
                 self.fullpagePlayer=nil;
                 [self.playerLayerFullPage removeFromSuperlayer];
                 self.playerLayerFullPage=nil;
             }
             [self->fullPagePlayerView removeFromSuperview];
             self->fullPagePlayerView=nil;
             [self.halfPlayer play];
         }];
        
        
        
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    //  NSLog(@"hello playerItemDidReachEnd");
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
    [btnPlay setUserInteractionEnabled:YES];
    [btnPlay setBackgroundColor:[UIColor greenColor]];
}
- (void)playerItemDidReachEnd2:(NSNotification *)notification {
    NSLog(@"hello playerItemDidReachEnd2");
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
    // [btnPlay setUserInteractionEnabled:YES];
    //[btnPlay setBackgroundColor:[UIColor greenColor]];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
