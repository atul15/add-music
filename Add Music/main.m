//
//  main.m
//  Add Music
//
//  Created by Puneet Kalra on 06/01/19.
//  Copyright © 2019 Atul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
