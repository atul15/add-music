//
//  AudioViewer.m
//  RecordAndUpload
//
//  Created by Mac-Mini on 24/01/19.
//  Copyright © 2019 Fortune. All rights reserved.
//

#import "AudioViewer.h"
#import "VideoEditorRoot.h"
#import "SCWaveformView.h"
#import "ICGVideoTrimmerView.h"
#import "VideoFilterVC.h"
#import "ShareVideoVC.h"
#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]


@import AVKit;
static void *AVPlayerDemoPlaybackViewControllerStatusObservationContext = &AVPlayerDemoPlaybackViewControllerStatusObservationContext;

@interface AudioViewer ()<ICGVideoTrimmerDelegate,AVAudioRecorderDelegate>
{
    UIButton *btnPlay,*btnFullPage,*btnNext;
    UILabel *lblRecordingBtnText;
    CGSize scrSize;
    UIView *fullPagePlayerView;
    BOOL ifFullPageVisible;
    
    //
    AVPlayer *_player;
    id _observer;
    float rationScr;
    int waitCounter;
    //
    UISlider *playbackSlider;
    NSDictionary *recordSettings;

    NSString *recorderFilePath;
    NSMutableArray *soundFileArray;
    
    AVAudioRecorder *audioRecorder;
    NSURL *soundFileURL;
}
@property (weak, nonatomic) IBOutlet UIView *videioEditorContainer;
@property (weak, nonatomic) IBOutlet UIView *recorderPartContainer;
@property (nonatomic)NSTimer *timer;
@property (weak, nonatomic) IBOutlet UIView *topBtnView;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *topPlayerView;
@property (weak, nonatomic) IBOutlet UIView *downSideMainView;
@property(nonatomic) AVPlayer *halfPlayer,*fullpagePlayer;
@property(nonatomic)AVPlayerItem *playerItemTop,*playertemFullPage;
@property(nonatomic)AVPlayerLayer *playerLayerTop,*playerLayerFullPage;
@property (nonatomic) SCScrollableWaveformView *scrollableWaveformView;

@property (weak, nonatomic) IBOutlet UIButton *btnVideoEdit;


@property (nonatomic) IBOutlet ICGVideoTrimmerView *trimmerView;
@property (assign, nonatomic) CGFloat startTime;
@property (assign, nonatomic) CGFloat stopTime;

@property (strong, nonatomic) NSTimer *recorderWaitTimeCheckerTimer;
@property (strong, nonatomic) NSTimer *recorderStopTimeCheckerTimer;
@property(nonatomic)UIButton *btnWait,*btnReording,*btnStartRecording;
@property (assign, nonatomic) CGFloat videoPlaybackPosition;
@property (assign, nonatomic) BOOL isPlaying;

@property(nonatomic)UIView *recodingBtnContainer;


@end

@implementation AudioViewer
//audiotoshare
- (IBAction)saveBtnClicked:(id)sender {
    [self performSegueWithIdentifier:@"audiotoshare" sender:_selectedVideoUrl];
}
- (IBAction)newBtnClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)openVideoEditorPage:(id)sender {
   // [self performSegueWithIdentifier:@"audioviewtovideo" sender:_selectedVideoUrl];
    
    [self performSegueWithIdentifier:@"filterSegue" sender:_selectedVideoUrl];
}

-(void)openVideoPage2
{
    [self performSegueWithIdentifier:@"audioviewtovideo" sender:_selectedVideoUrl];
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"filterSegue"])
    {
        VideoFilterVC *controller = (VideoFilterVC *)segue.destinationViewController;
        controller.videoUrl=sender;
    }
   else if([segue.identifier isEqualToString:@"audioviewtovideo"]){
        VideoEditorRoot *controller = (VideoEditorRoot *)segue.destinationViewController;
        controller.selectedVideoUrl=sender;
    }
   else if([segue.identifier isEqualToString:@"audiotoshare"]){
       ShareVideoVC *controller = (ShareVideoVC *)segue.destinationViewController;
       controller.videoUrl=sender;
   }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"Video to Gif viewWillDisappear");
    [super viewWillDisappear:animated];
    
    
    [self.trimmerView setDelegate:nil];
}


-(void)initializeAllMediaContainer
{
    /*
     NSString *tempDir = NSTemporaryDirectory();
    NSString *soundFilePath = [tempDir stringByAppendingString: @"sound.caf"];
    NSString *outputSoundFilePath = [tempDir stringByAppendingString:@"soundO.caf"];
    
    soundFileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath];
    outputSoundFileURL = [[NSURL alloc] initFileURLWithPath:outputSoundFilePath];
    */
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"canTouch"];
    
    
    [[AVAudioSession sharedInstance]
     setCategory:AVAudioSessionCategoryPlayAndRecord
     error: nil];
    
    //    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient  error: nil];
    
    recordSettings = [[NSDictionary alloc] initWithObjectsAndKeys:
                      [NSNumber numberWithFloat:44100.0], AVSampleRateKey,
                      [NSNumber numberWithInt:kAudioFormatLinearPCM], AVFormatIDKey,
                      [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
                      [NSNumber numberWithInt:AVAudioQualityMax], AVEncoderAudioQualityKey,
                      nil];
    
  
    
    
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    ifFullPageVisible=NO;
    // Do any additional setup after loading the view.
    fullPagePlayerView=nil;
    self.fullpagePlayer=nil;
    
  
    soundFileArray=[[NSMutableArray alloc] init];
    scrSize=[UIScreen mainScreen].bounds.size;
    self.halfPlayer=[AVPlayer playerWithURL:_selectedVideoUrl];
    self.playerLayerTop=[AVPlayerLayer playerLayerWithPlayer:self.halfPlayer];
  //  self.playerLayerTop.frame = _topPlayerView.bounds;
    self.playerLayerTop.frame = CGRectMake(0, 0, scrSize.width, scrSize.width*(3.0/4.0));
    rationScr=scrSize.width*(3.0/4.0);
    self.playerLayerTop.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [_topPlayerView.layer addSublayer:self.playerLayerTop];
    self.halfPlayer.muted = true;
    
    [self.playerLayerTop setBackgroundColor:[UIColor clearColor].CGColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.halfPlayer currentItem]];
    [self.halfPlayer play];
    
    btnPlay=[[UIButton alloc] init];
    [_topPlayerView addSubview:btnPlay];
    [btnPlay setBackgroundImage:[UIImage imageNamed:@"Play button.pdf"] forState:UIControlStateNormal];
    [btnPlay addTarget:self action:@selector(btnPlayClicked) forControlEvents:UIControlEventTouchUpInside];
    [btnPlay setUserInteractionEnabled:NO];
    [btnPlay setHidden:YES];
    
    btnFullPage=[[UIButton alloc] init];
    [_mainView addSubview:btnFullPage];
    [btnFullPage setBackgroundImage:[UIImage imageNamed:@"Full screen.pdf"] forState:UIControlStateNormal];

    [btnFullPage addTarget:self action:@selector(btnFullPageClicked) forControlEvents:UIControlEventTouchUpInside];
    //[btnFullPage setUserInteractionEnabled:NO];
    
    
    
    [_topPlayerView bringSubviewToFront:self.topBtnView];
    
    [self.halfPlayer addObserver:self forKeyPath:@"status" options:0 context:AVPlayerDemoPlaybackViewControllerStatusObservationContext];

    [self initializeAllMediaContainer];
    [self addTrimmerCode];
    [self setRecodingBtnContainerView];
}


-(void)setRecodingBtnContainerView
{
    _recodingBtnContainer=[[UIView alloc] initWithFrame:CGRectMake(0, 5, scrSize.width, 60)];
    [_recorderPartContainer addSubview:_recodingBtnContainer];
    [_recodingBtnContainer setBackgroundColor:_topBtnView.backgroundColor];
    self.btnStartRecording=[[UIButton alloc] initWithFrame:CGRectMake((_recodingBtnContainer.frame.size.width/2)-65,10, 45, 40)];
    [self.btnStartRecording setBackgroundColor:[UIColor clearColor]];
    [self.btnStartRecording setBackgroundImage:[UIImage imageNamed:@"Music Icon.pdf"] forState:UIControlStateNormal];
    [self.btnStartRecording setContentMode:UIViewContentModeScaleAspectFit];
    [self.btnStartRecording setClipsToBounds:YES];

    [self.btnStartRecording addTarget:self action:@selector(startRecordingClicked) forControlEvents:UIControlEventTouchUpInside];
    [_recodingBtnContainer addSubview:self.btnStartRecording];
    
    
    lblRecordingBtnText=[[UILabel alloc] initWithFrame:CGRectMake((_btnStartRecording.frame.origin.x+5)+_btnStartRecording.frame.size.width,_recodingBtnContainer.frame.size.height/2-13, 160, 27)];
    [lblRecordingBtnText setTextColor:[UIColor whiteColor]];
    [lblRecordingBtnText setText:@"TAP TO ADD MUSIC"];
    [lblRecordingBtnText setFont:[UIFont systemFontOfSize:12 weight:UIFontWeightSemibold]];
     [_recodingBtnContainer addSubview:lblRecordingBtnText];
}
- (void)observeValueForKeyPath:(NSString*) path ofObject:(id)object change:(NSDictionary*)change context:(void*)context
{
    if (self.halfPlayer.status == AVPlayerStatusReadyToPlay) {
        [self.halfPlayer play];
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
        
    }
}
- (void)sliderValueChanged:(UISlider *)sender {
    [self.halfPlayer seekToTime:CMTimeMakeWithSeconds(sender.value, NSEC_PER_SEC) toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
}



-(void)startRecordingClicked
{
    [self.btnStartRecording setUserInteractionEnabled:NO];
    waitCounter=3;
    NSLog(@"Ready to startRecordingClicked");
    
    if(self.btnWait!=nil)
    {
        [self.btnWait removeFromSuperview];
        self.btnWait=nil;
    }
    self.recorderWaitTimeCheckerTimer=nil;
    self.btnWait=[[UIButton alloc] initWithFrame:btnPlay.frame];
    [btnPlay.superview addSubview:self.btnWait];
    [self.btnWait setBackgroundColor:[UIColor blackColor]];
    [self.btnWait setTitle:[NSString stringWithFormat:@"%d",waitCounter--] forState:UIControlStateNormal];
    [[self.btnWait layer] setCornerRadius:self.btnWait.frame.size.height/2];
    
    
    NSDate *now = [NSDate dateWithTimeIntervalSinceNow:0];
    NSString *caldate = [now description];
    recorderFilePath = [NSString stringWithFormat:@"%@/%@.caf", DOCUMENTS_FOLDER, caldate];
    
    [soundFileArray addObject:recorderFilePath];
    soundFileURL = [NSURL fileURLWithPath:recorderFilePath];
    
    
    audioRecorder = [[AVAudioRecorder alloc] initWithURL: soundFileURL
                                                settings: recordSettings
                                                   error: nil];
    
    [audioRecorder setMeteringEnabled:YES];
    
    [audioRecorder prepareToRecord];
    audioRecorder.delegate = self;
    
    
    self.recorderWaitTimeCheckerTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(setTextOfWaitByTimer) userInfo:nil repeats:YES];
    
    
}


-(void)setTextOfWaitByTimer
{
    NSLog(@"setTextOfWaitByTimer waitCounter=%d",waitCounter);
    [self.btnWait setTitle:[NSString stringWithFormat:@"%d",waitCounter] forState:UIControlStateNormal];
    if(waitCounter==0)
    {
        [self.btnStartRecording setUserInteractionEnabled:YES];
        [self.recorderWaitTimeCheckerTimer invalidate];
        self.recorderWaitTimeCheckerTimer=nil;
        if(self.btnWait!=nil)
        {
            [self.btnWait removeFromSuperview];
            self.btnWait=nil;
        
        }
        [audioRecorder record];
        [self startRecording];
    }
    waitCounter--;
}




- (void)startRecording{
    
   
    
   
    
   
    NSTimeInterval time1=CMTimeGetSeconds((self.halfPlayer.currentItem.asset.duration));

     self.recorderStopTimeCheckerTimer = [NSTimer scheduledTimerWithTimeInterval:time1 target:self selector:@selector(stopRecording) userInfo:nil repeats:YES];

    
}

- (void) stopRecording{
    
    [audioRecorder stop];
    NSLog(@"stopRecording");
    [self.recorderStopTimeCheckerTimer invalidate];
    self.recorderStopTimeCheckerTimer=nil;
    
    NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
    NSError *err = nil;
    NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
    [audioData length];
    NSLog(@"Length=%ld",[audioData length]);
    if(!audioData)
        NSLog(@"audio data: %@ %ld %@", [err domain], (long)[err code], [[err userInfo] description]);
    
    
    
    
   
    [self showSoundWave:url];
    
  
}



- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *) aRecorder successfully:(BOOL)flag
{
    NSLog (@"audioRecorderDidFinishRecording:successfully:");
    // your actions here
}
#pragma mark - ICGVideoTrimmerDelegate
- (void)trimmerView:(ICGVideoTrimmerView *)trimmerView didChangeLeftPosition:(CGFloat)startTime rightPosition:(CGFloat)endTime
{
    NSLog(@"didChangeLeftPosition");
    if (startTime != self.startTime)
    {
        [self seekVideoToPos:startTime];
    }
    self.startTime = startTime;
    self.stopTime = endTime;
}






- (void)seekVideoToPos:(CGFloat)pos
{
    NSLog(@"Video to gif seekVideoToPos");
    self.videoPlaybackPosition = pos;
    CMTime time = CMTimeMakeWithSeconds(self.videoPlaybackPosition, self.halfPlayer.currentTime.timescale);
    
    
    [playbackSlider setValue:self.startTime];
    
    [self.halfPlayer seekToTime:time toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
}

-(void)addTrimmerCode
{
    [_downSideMainView setBackgroundColor:[UIColor clearColor]];
    self.trimmerView=[[ICGVideoTrimmerView alloc] initWithAsset:[AVAsset assetWithURL:self.selectedVideoUrl]];
  //  self.trimmerView.frame=CGRectMake(10,  50, scrSize.width-20,100);
    
    self.trimmerView.frame=CGRectMake(_btnVideoEdit.frame.size.width+(_btnVideoEdit.frame.origin.x*2),  10, scrSize.width-(_btnVideoEdit.frame.size.width+(_btnVideoEdit.frame.origin.x*2)+6),_btnVideoEdit.superview.frame.size.height-20);

  //  [_downSideMainView addSubview:self.trimmerView];
    
    [_btnVideoEdit.superview addSubview:self.trimmerView];

    [self.trimmerView setThemeColor:[UIColor clearColor]];
    [self.trimmerView setThumbWidth:0.1];
    [self.trimmerView setShowsRulerView:NO];
    
    [self.trimmerView setTrackerColor:[UIColor clearColor]];
    [self.trimmerView setDelegate:self];
    
    [self.trimmerView resetSubviews];
    
    [self.trimmerView hideTracker:YES];
    
    [self.trimmerView setBackgroundColor:[UIColor clearColor]];
    playbackSlider=[[UISlider alloc] initWithFrame:CGRectMake(self.trimmerView.frame.origin.x,self.trimmerView.frame.origin.y+( self.trimmerView.frame.size.height/2-5), self.trimmerView.frame.size.width, 10)];
   // [_downSideMainView addSubview:playbackSlider];
    [_btnVideoEdit.superview addSubview:playbackSlider];

    playbackSlider.maximumValue = CMTimeGetSeconds(self.halfPlayer.currentItem.asset.duration);
    playbackSlider.minimumValue =0;
    [playbackSlider setThumbImage:[UIImage imageNamed:@"Cropping Pointer.pdf"] forState:UIControlStateNormal];
    [playbackSlider setMinimumTrackTintColor:[UIColor clearColor]];
    [playbackSlider setMaximumTrackTintColor:[UIColor clearColor]];
    [playbackSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
}

- (void)updateSlider {
    CGFloat val = playbackSlider.value + 0.1f;
    [playbackSlider setValue:val];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    btnPlay.frame=CGRectMake((scrSize.width/2)-25, (self.playerLayerTop.frame.size.height/2)-25, 50, 50);
    
    //btnFullPage.frame=CGRectMake(scrSize.width-60, 20, 40, 40);
    btnFullPage.frame=CGRectMake(scrSize.width-50, (_topPlayerView.frame.origin.y+self.playerLayerTop.frame.size.height)-50, 40, 40);
   // self.playerLayerTop.frame = CGRectMake(0, 0, scrSize.width, _topPlayerView.frame.size.height);
    
   // [self showSoundWave];
}
-(void)showSoundWave:(NSURL*)soundUrl
{
    NSLog(@"showSoundWave");
    
    self.scrollableWaveformView=[[SCScrollableWaveformView alloc] initWithFrame:CGRectMake(10, _recorderPartContainer.frame.size.height-80, scrSize.width-20, 70)];
    
   /* UIColor *topColor = [UIColor colorWithRed:14.0/255.0 green:185.0/255.0 blue:223.0/255.0 alpha:1.0];
    UIColor *bottomColor = [UIColor colorWithRed:41.0/255.0 green:242.0/255.0 blue:158.0/255.0 alpha:1.0];
    
    // Create the gradient
    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
    theViewGradient.colors = [NSArray arrayWithObjects: (id)topColor.CGColor, (id)bottomColor.CGColor, nil];
    theViewGradient.frame = CGRectMake(0, 0, self.scrollableWaveformView.frame.size.width, self.scrollableWaveformView.frame.size.height);
    
    //Add gradient to view
    [self.scrollableWaveformView.layer addSublayer:theViewGradient];
     */
    
    [_recorderPartContainer addSubview:self.scrollableWaveformView];
    [[self.scrollableWaveformView layer] setCornerRadius:5];
    [self.scrollableWaveformView setBackgroundColor:[UIColor colorWithRed:14.0/255.0 green:185.0/255.0 blue:223.0/255.0 alpha:1.0]];
    self.scrollableWaveformView.waveformView.precision = 1;
    self.scrollableWaveformView.waveformView.lineWidthRatio = 4;
    // self.scrollableWaveformView.waveformView.normalColor = [UIColor colorWithRed:0.8 green:0.3 blue:0.3 alpha:1];
    self.scrollableWaveformView.waveformView.normalColor = [UIColor colorWithRed:41.0/255.0 green:242.0/255.0 blue:158.0/255.0 alpha:1.0];
    
    self.scrollableWaveformView.waveformView.channelsPadding = 10;
    //self.scrollableWaveformView.waveformView.progressColor = [UIColor colorWithRed:1 green:0.2 blue:0.2 alpha:1];
    self.scrollableWaveformView.waveformView.progressColor = [UIColor whiteColor];
    
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:soundUrl options:nil];
    

    
    self.scrollableWaveformView.alpha = 1.0;
    
    self.scrollableWaveformView.waveformView.asset = asset;
    NSLog(@"hello assest=%@",asset);
    /*  CMTime progressTime = CMTimeMakeWithSeconds(
     self.slider.value * CMTimeGetSeconds(self.scrollableWaveformView.waveformView.asset.duration),
     100000);*/
    
    
    NSLog(@"current asest Duration=%f ",CMTimeGetSeconds(asset.duration));
    NSLog(@"current self.scrollableWaveformView.waveformView.asset.duration Duration=%f ",CMTimeGetSeconds(self.scrollableWaveformView.waveformView.asset.duration));
    
    CMTime progressTime = CMTimeMakeWithSeconds(
                                                0.1*CMTimeGetSeconds(self.scrollableWaveformView.waveformView.asset.duration),
                                                100000);
    
    
    self.scrollableWaveformView.waveformView.timeRange = CMTimeRangeMake(CMTimeMakeWithSeconds(1, 10000), progressTime);
    
    _player = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithAsset:asset]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_playReachedEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:_player.currentItem];
    
    __unsafe_unretained AudioViewer *mySelf = self;
    _observer = [_player addPeriodicTimeObserverForInterval:CMTimeMake(1, 60) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
        mySelf.scrollableWaveformView.waveformView.progressTime = time;
    }];
    
    [_player play];
}
- (void)_playReachedEnd:(NSNotification *)notification {
    if (notification.object == _player.currentItem) {
        
       // [_player seekToTime:kCMTimeZero];
        //[_player play];
    }
}


-(void)btnPlayClicked
{
    [btnPlay setUserInteractionEnabled:NO];
    [btnPlay setBackgroundColor:[UIColor clearColor]];
    [self.halfPlayer play];
    if(self.timer!=nil||[self.timer isValid])
    {
        [self.timer invalidate];
        self.timer=nil;
    }
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
    
    
}
-(void)btnFullPageClicked
{
    NSLog(@"btnFullPageClicked");
    if(!ifFullPageVisible)
    {
        NSLog(@"btnFullPageClicked !ifFullPageVisible");
        [self.halfPlayer pause];
        if(fullPagePlayerView==nil)
        {
            fullPagePlayerView=[[UIView alloc] initWithFrame:CGRectMake(_topPlayerView.frame.origin.x, _topPlayerView.frame.origin.y, _topPlayerView.frame.size.width, _topPlayerView.frame.size.height)];
            [_mainView addSubview:fullPagePlayerView];
        }
        [_mainView bringSubviewToFront:btnFullPage];
        [fullPagePlayerView setBackgroundColor:[UIColor cyanColor]];
        if(self.fullpagePlayer!=nil)
        {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[self.fullpagePlayer currentItem]];
            [self.fullpagePlayer pause];
            self.fullpagePlayer=nil;
            [self.playerLayerFullPage removeFromSuperlayer];
            self.playerLayerFullPage=nil;
        }
        
        NSLog(@"_topPlayerView.frame.origin.x=%f _topPlayerView.frame.origin.y=%f _topPlayerView.frame.size.width=%f _topPlayerView.frame.size.height=%f",_topPlayerView.frame.origin.x, _topPlayerView.frame.origin.y, _topPlayerView.frame.size.width, _topPlayerView.frame.size.height);
        
        NSLog(@"fullPagePlayerView.frame.origin.x=%f fullPagePlayerView.frame.origin.y=%f fullPagePlayerView.frame.size.width=%f fullPagePlayerView.frame.size.height=%f",fullPagePlayerView.frame.origin.x, fullPagePlayerView.frame.origin.y, fullPagePlayerView.frame.size.width, fullPagePlayerView.frame.size.height);
        
        
        self.fullpagePlayer=[AVPlayer playerWithURL:_selectedVideoUrl];
        self.playerLayerFullPage=[AVPlayerLayer playerLayerWithPlayer:self.fullpagePlayer];
        self.playerLayerFullPage.frame = fullPagePlayerView.frame;
        [self.playerLayerFullPage setBackgroundColor:[UIColor clearColor].CGColor];
        self.playerLayerFullPage.videoGravity = AVLayerVideoGravityResizeAspectFill;
        [fullPagePlayerView.layer addSublayer:self.playerLayerFullPage];
        self.fullpagePlayer.muted = true;
        
        [self.playerLayerTop setBackgroundColor:[UIColor whiteColor].CGColor];
        
        self.fullpagePlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd2:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:[self.fullpagePlayer currentItem]];
        
        
        [btnPlay setUserInteractionEnabled:NO];
        //[btnPlay setBackgroundColor:[UIColor clearColor]];
        [btnPlay setHidden:YES];
        [self.fullpagePlayer play];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            fullPagePlayerView.frame=CGRectMake(fullPagePlayerView.frame.origin.x, fullPagePlayerView.frame.origin.y, fullPagePlayerView.frame.size.width, _mainView.frame.size.height);
            self.playerLayerFullPage.frame = fullPagePlayerView.frame;
            btnFullPage.frame=CGRectMake(btnFullPage.frame.origin.x, _mainView.frame.size.height-(btnFullPage.frame.size.height+30), btnFullPage.frame.size.width, btnFullPage.frame.size.height);
        }
                         completion:^(BOOL finished)
         {
             
             self->ifFullPageVisible=YES;
             
         }];
        
    }
    else
    {
        NSLog(@"btnFullPageClicked ifFullPageVisible");
        [UIView animateWithDuration:0.3 animations:^{
            
            fullPagePlayerView.frame=CGRectMake(_topPlayerView.frame.origin.x, _topPlayerView.frame.origin.y, _topPlayerView.frame.size.width, _topPlayerView.frame.size.height);
            self.playerLayerFullPage.frame = fullPagePlayerView.frame;
            
            
            
            btnFullPage.frame=CGRectMake(scrSize.width-60, (_topPlayerView.frame.origin.y+_topPlayerView.frame.size.height)-80, 40, 40);
            
            
        }
                         completion:^(BOOL finished)
         {
             self->ifFullPageVisible=NO;
             
             if(self.fullpagePlayer!=nil)
             {
                 [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:[self.fullpagePlayer currentItem]];
                 [self.fullpagePlayer pause];
                 self.fullpagePlayer=nil;
                 [self.playerLayerFullPage removeFromSuperlayer];
                 self.playerLayerFullPage=nil;
             }
             [self->fullPagePlayerView removeFromSuperview];
             self->fullPagePlayerView=nil;
             [self.halfPlayer play];
         }];
        
        
        
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    //  NSLog(@"hello playerItemDidReachEnd");
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
    [btnPlay setUserInteractionEnabled:YES];
   // [btnPlay setBackgroundColor:[UIColor greenColor]];
    [btnPlay setHidden:NO];
}
- (void)playerItemDidReachEnd2:(NSNotification *)notification {
    NSLog(@"hello playerItemDidReachEnd2");
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
    // [btnPlay setUserInteractionEnabled:YES];
    //[btnPlay setBackgroundColor:[UIColor greenColor]];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
