//
//  AppDelegate.m
//  Add Music
//
//  Created by Puneet Kalra on 06/01/19.
//  Copyright © 2019 Atul. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

-(void)writeCurrentProjectWithFileName:(NSString*)fileName creationDate:(NSDate*)dtOfCreation andProjectName:(NSString*)projectName andThumbName:(NSString*)strThumb{
    NSMutableDictionary *data2=nil;
    data2 = [[NSMutableDictionary alloc] initWithContentsOfFile:_projectMediaInfoFilePath];
    NSMutableArray *ar=nil;
    NSDictionary *dct=[NSDictionary dictionaryWithObjectsAndKeys:fileName,@"fileName",projectName,@"projectName",dtOfCreation,@"creationDate",strThumb,@"thumbName",@YES,@"fromLocal", nil];

    if(data2==nil)
    {

        ar=[@[dct] mutableCopy];
        
    }
    else
    {
        ar=[[data2 objectForKey:@"projectList"] mutableCopy];
        [ar addObject:dct];
    }
    if(data2==nil)
        data2=[[NSMutableDictionary alloc] init];
    data2[@"projectList"] = ar;
    [data2 writeToFile: _projectMediaInfoFilePath atomically:YES];
   
}

-(void)updateProjectInfo:(NSDictionary*)projectDict forIndex:(int)idx
{
    NSMutableDictionary *data2=nil;
    data2 = [[NSMutableDictionary alloc] initWithContentsOfFile:_projectMediaInfoFilePath];
    NSMutableArray *ar=nil;
    ar=[[data2 objectForKey:@"projectList"] mutableCopy];
    [ar replaceObjectAtIndex:idx withObject:projectDict];
    data2[@"projectList"] = ar;
    [data2 writeToFile: _projectMediaInfoFilePath atomically:YES];
    
}

-(NSArray*)getStoredProjectList
{
    NSMutableDictionary *data2=nil;
    data2 = [[NSMutableDictionary alloc] initWithContentsOfFile:_projectMediaInfoFilePath];
    NSMutableArray *ar=nil;

    if(data2==nil)
    {
        return nil;
    }
    else
    {
         ar=[[data2 objectForKey:@"projectList"] mutableCopy];
        return ar;
        
    }
    
}
- (void)listAllLocalFiles
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    _rootFolderPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"AppVideo"];
    NSError *error=nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:_rootFolderPath])    //Does Video directory already exist?
    {
        if (![[NSFileManager defaultManager] createDirectoryAtPath:_rootFolderPath
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                             error:&error])
        {
            NSLog(@"Create video directory error: %@", error);
        }
        //     NSLog(@"pathTo Appvideo filePath=%@",_videoPath);
    }
    else
    {
        NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:_rootFolderPath error:nil];
        NSLog(@"In appdeligate Handler video path already exist with number of files=%@",files);
        files=nil;
        
    }
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    _currentAssestInfo=nil;
    [self listAllLocalFiles];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    _projectMediaInfoFilePath=[[paths objectAtIndex:0] stringByAppendingPathComponent:@"mediaFileInfo.plist"];
    if(![[NSFileManager defaultManager] fileExistsAtPath:_projectMediaInfoFilePath])
    {
        [[NSFileManager defaultManager] createFileAtPath:_projectMediaInfoFilePath contents:nil attributes:nil];
        NSLog(@"The file is not exist sentMediaInfoFilePath=%@",_projectMediaInfoFilePath);
    }
    else
    {
        NSLog(@"sentMediaInfoFilePath File is ready");
    }
    
    
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
} 
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
