//
//  AudioSourceSelector.m
//  RecordAndUpload
//
//  Created by Mac-Mini on 24/01/19.
//  Copyright © 2019 Fortune. All rights reserved.
//

#import "AudioSourceSelector.h"
#import "SourceColletctionHeader.h"
#import "MyMainCollectionViewCell.h"
#import "MyCustomLayoutHorizontal.h"
#import "MyCustomLayoutUGC.h"
#import "VideoEditorRoot.h"
@interface AudioSourceSelector ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    UICollectionView *myCollectionView;
    CGSize scrSize;
    NSMutableArray *sourceTypeList;
    SourceColletctionHeader *headerView ;
    BOOL isFirst;
    UISearchBar *theSearchBar;
}
@property (weak, nonatomic) IBOutlet UIView *mainBottomView;
@property (weak, nonatomic) IBOutlet UIView *topSideView;

@end

@implementation AudioSourceSelector

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"AudioSourceSelector viewDidLoad selectedVideoUrl=%@",_selectedVideoUrl);
    
    isFirst=YES;
    sourceTypeList=[[self getSourceTypeList] mutableCopy];
    headerView=nil;
    scrSize=[UIScreen mainScreen].bounds.size;
    UICollectionViewFlowLayout *myLayout=[[UICollectionViewFlowLayout alloc] init];
    //MyCustomLayoutUGC *myLayout=[[MyCustomLayoutUGC alloc] init];
    
    myLayout.headerReferenceSize = CGSizeMake(scrSize.width, 150.f);
    
    myCollectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, scrSize.width, scrSize.height) collectionViewLayout:myLayout];
    
    
    [myCollectionView registerClass:[MyMainCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [myCollectionView registerClass:[SourceColletctionHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    
    [myCollectionView setBackgroundColor:[UIColor clearColor]];
    [_mainBottomView addSubview:myCollectionView];
    
    
    
    [myCollectionView setDataSource:self];
    [myCollectionView setDelegate:self];
    
    
    [_topSideView setHidden:YES];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"in cellForItemAtIndexPath");
    __block MyMainCollectionViewCell *cell=(MyMainCollectionViewCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    cell.tag=indexPath.row+100;
    
    cell.backView.frame=CGRectMake(0,0, cell.frame.size.width, cell.frame.size.height);
    
    
    cell.mainContainerView.hidden=NO;
    cell.mainContainerView.frame=CGRectMake(3, 2, cell.frame.size.width-6, cell.frame.size.height-6);
    cell.mainContainerView.backgroundColor=[UIColor whiteColor];
    cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return sourceTypeList.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((scrSize.width/2)-5, (scrSize.width/2)-5);
}



/*- (CGSize)collectionView:(UICollectionView *)collectionView layout:(MyCustomLayoutUGC *)collectionViewLayout preferredSizeForItemAtIndexPath:(NSIndexPath *)indexPath
 {
 return CGSizeMake(_mainBottomView.frame.size.width/2, _mainBottomView.frame.size.width/2);
 }*/
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"viewForSupplementaryElementOfKind");
    headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                  UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
    if(isFirst)
    {
        isFirst=NO;
        [headerView setBackgroundColor:[UIColor clearColor]];
        [self updateSectionHeader:headerView forIndexPath:indexPath];
    }
    return headerView;
}


- (void)updateSectionHeader:(UICollectionReusableView *)header forIndexPath:(NSIndexPath *)indexPath
{
    SourceColletctionHeader *headerView1 =(SourceColletctionHeader *)header;
    NSString *text = [NSString stringWithFormat:@"header #%i", indexPath.row];
    headerView1.lbl.text = text;
    
    theSearchBar= [[UISearchBar alloc] init];
    
    theSearchBar.frame=CGRectMake(0,headerView1.frame.size.height/2-20,headerView1.frame.size.width,40);
    
    [headerView1 addSubview:theSearchBar];
    
    
    //   [theSearchBar setBarTintColor:[UIColor colorWithRed:224.0f/255.0f green:224.0f/255.0f blue:224.0f/255.0f alpha:1.0]];
    [theSearchBar setBarTintColor:[UIColor blackColor]];
    
    //theSearchBar.delegate = self;
    [theSearchBar setSearchBarStyle:UISearchBarStyleMinimal];
    theSearchBar.backgroundColor=[UIColor darkGrayColor];
    [self setSearchIconToFavicon];
    
}
//
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self performSegueWithIdentifier:@"audiosourcetoaudioviewer" sender:_selectedVideoUrl];
    
    
    
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"audiosourcetoaudioviewer"]){
        AudioSourceSelector *controller = (AudioSourceSelector *)segue.destinationViewController;
        controller.selectedVideoUrl=sender;
    }
    
}
- (void)setSearchIconToFavicon
{
    // Really a UISearchBarTextField, but the header is private.
    UITextField *searchField = nil;
    for (UIView *subview in theSearchBar.subviews) {
        if ([subview isKindOfClass:[UITextField class]]) {
            searchField = (UITextField *)subview;
            break;
        }
    }
    if (searchField) {
        UIImage *image = [UIImage imageNamed: @"167.png"];
        UIImageView *iView = [[UIImageView alloc] initWithImage:image];
        searchField.leftView = iView;
        
    }
}


-(NSArray*)getSourceTypeList
{
    return @[@"One",@"Two",@"Three",@"Four",@"Five",@"Six"];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_mainBottomView bringSubviewToFront:_topSideView];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
