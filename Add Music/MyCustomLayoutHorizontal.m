//
//  MyCustomLayoutHorizontal.m
//  Messagif
//
//  Created by Puneet Kalra on 17/07/18.
//  Copyright © 2018 Aaplabs. All rights reserved.
//

#import "MyCustomLayoutHorizontal.h"

#define INSET_TOP3 1
#define INSET_LEFT3 1
#define INSET_BOTTOM3 1
#define INSET_RIGHT3 1
@interface MyCustomLayoutHorizontal()
{
    CGRect **_itemFrameSections;
    NSInteger _numberOfItemFrameSections;
    NSMutableArray *leftArray,*rightArray,*finalArray;
    NSString *currentSearchString;
}
@property (nonatomic) CGSize contentSize;

@property (nonatomic, strong) NSArray *headerFrames;
@property (nonatomic, strong) NSArray *footerFrames;
@property (nonatomic) NSDictionary *layoutInformation;
@property (nonatomic) NSInteger maxNumRows;
@property (nonatomic) UIEdgeInsets insets;

@end
@implementation MyCustomLayoutHorizontal
-(id)init {
    if(self = [super init]) {
        self.insets = UIEdgeInsetsMake(INSET_TOP3, INSET_LEFT3, INSET_BOTTOM3, INSET_RIGHT3);
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    // set to NULL so it is not released by accident in dealloc
    
    leftArray=[[NSMutableArray alloc] init];
    rightArray=[[NSMutableArray alloc] init];
    finalArray=[[NSMutableArray alloc] init];
    self.sectionInset = UIEdgeInsetsMake(1, 1,1, 1);
    self.minimumLineSpacing = 1;
    self.minimumInteritemSpacing = 1;
    // self.headerReferenceSize = CGSizeZero;
    
    self.footerReferenceSize = CGSizeZero;
    self.scrollDirection = UICollectionViewScrollDirectionVertical;
}

- (void)prepareLayout
{
    [super prepareLayout];
    [self clearItemFrames];
    [self helloCalculateAndStoreFrame];
    
    NSLog(@"prepareLayout _numberOfItemFrameSections");
    
    
}

-(void)helloCalculateAndStoreFrame
{
    
    
    float lftTop,rtTop,lftxVal;
    lftTop=self.minimumLineSpacing;
    rtTop=self.minimumLineSpacing;
    lftxVal=self.minimumLineSpacing;
    for(int i=0;i<[self.collectionView numberOfItemsInSection:0];i++)
    {
        
        CGSize actualSize = [self.delegate collectionView:self.collectionView layout:self preferredSizeForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
        
        CGRect frame;
        frame = CGRectMake(lftxVal, lftTop, actualSize.width, actualSize.height);
        

        lftxVal+= actualSize.width + self.minimumInteritemSpacing;

    
        [finalArray addObject:[NSValue valueWithCGRect:frame]];
        
    }
    self.contentSize = CGSizeMake( self.minimumLineSpacing+lftxVal,self.collectionView.frame.size.height);
    NSLog(@"helloCalculateAndStoreFrame fianl width contentSize=%f height=%f",self.contentSize.width,self.contentSize.height);

    
}

- (NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
    
    NSMutableArray *layoutAttributes = [NSMutableArray array];
    
    NSLog(@"layoutAttributesForElementsInRect");
    
    for (int i = 0; i < [self.collectionView numberOfItemsInSection:0]; i++) {
        CGRect itemFrame=[[finalArray objectAtIndex:i] CGRectValue];
        if (CGRectIntersectsRect(rect, itemFrame))
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            [layoutAttributes addObject:[self layoutAttributesForItemAtIndexPath:indexPath]];
        }
    }
    
    
    
    
    return layoutAttributes;
    
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    
    attributes.frame =[[finalArray objectAtIndex:indexPath.row] CGRectValue];
    
    return attributes;
}




- (void)clearItemFrames
{
    [finalArray removeAllObjects];
    
    
}
- (void)dealloc
{
    [self clearItemFrames];
}


- (CGSize)collectionViewContentSize {
    return self.contentSize;
    
}
#pragma mark - Delegate

- (id<MyCustomProtocol>)delegate
{
    return (id<MyCustomProtocol>)self.collectionView.delegate;
}

@end
