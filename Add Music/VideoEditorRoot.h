//
//  VideoEditorRoot.h
//  RecordAndUpload
//
//  Created by Mac-Mini on 24/01/19.
//  Copyright © 2019 Fortune. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoEditorRoot : UIViewController
@property (nonatomic)NSURL *selectedVideoUrl;

@end

NS_ASSUME_NONNULL_END
