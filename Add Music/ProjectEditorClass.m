//
//  ProjectEditorClass.m
//  Add Music
//
//  Created by Mac-Mini on 15/01/19.
//  Copyright © 2019 Atul. All rights reserved.
//

#import "ProjectEditorClass.h"
#import "AppDelegate.h"

@import AVKit;
@interface ProjectEditorClass ()<UITextFieldDelegate>
@property(nonatomic)  AVPlayer* mPlayer;
@property (strong, nonatomic) AVPlayerItem *playerItem;
@property (nonatomic)  UIView *videoPlayer;
@property (nonatomic)  UIView *videoLayer,*bottomView,*upperTopView,*topUpperView;
@property (strong, nonatomic) AVPlayerLayer *playerLayer;
@property(nonatomic)AVAsset *asset;
@property(nonatomic)UITextField *prjNameField;
@property(nonatomic)UIButton *btnEditEnable,*btnBack;
@property(nonatomic)BOOL canEdit;
@property (nonatomic)NSString *initailTest,*modifiedTest;
@end

@implementation ProjectEditorClass
@synthesize topUpperView;
- (void)viewDidLoad {
    [super viewDidLoad];
    _canEdit=YES;
   // self.view.backgroundColor=[UIColor whiteColor];
    AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
    
    if([[NSFileManager defaultManager] fileExistsAtPath:[app.rootFolderPath stringByAppendingPathComponent:[_selectedDictionary objectForKey:@"fileName"]]])
    {
        NSLog(@"file exist");
    }
    else
    {
        NSLog(@"file not exist");

    }
    self.asset=[AVAsset assetWithURL:[NSURL fileURLWithPath:[app.rootFolderPath stringByAppendingPathComponent:[_selectedDictionary objectForKey:@"fileName"]]]];
     
    _initailTest=[_selectedDictionary objectForKey:@"projectName"];
    _modifiedTest=[_selectedDictionary objectForKey:@"projectName"];
    
    
    NSLog(@"viewDidLoad ProjectEditorClass selected dict=%@ from index=%d",_selectedDictionary,_selectedIndex);
    // Do any additional setup after loading the view.
    
    _bottomView=[[UIView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:_bottomView];
   // _bottomView.backgroundColor=[UIColor whiteColor];

    _upperTopView=[[UIView alloc] initWithFrame:CGRectMake(0,20, self.view.frame.size.width, 50)];
    [_bottomView addSubview:_upperTopView];
    [self setUIForPlayer];
}

-(void)setUIForPlayer
{
    topUpperView=[[UIView alloc] initWithFrame:CGRectMake(0,_upperTopView.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-(_upperTopView.frame.size.height*2))];
    
    [topUpperView setBackgroundColor:[UIColor clearColor]];
    
    [_bottomView addSubview:topUpperView];
    self.videoPlayer=[[UIView alloc] initWithFrame:CGRectMake( 0, 0,topUpperView.frame.size.width, topUpperView.frame.size.height)];
    
    [topUpperView addSubview:self.videoPlayer];
    
    [self.videoPlayer setBackgroundColor:[UIColor clearColor]];
    
    self.videoLayer=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.videoPlayer.frame.size.width, self.videoPlayer.frame.size.height)];
    [self.videoPlayer addSubview:self.videoLayer];
    [self.videoPlayer setBackgroundColor:[UIColor clearColor]];

    _prjNameField=[[UITextField alloc] initWithFrame:CGRectMake(10, 5, topUpperView.frame.size.width-20, 40)];
    [topUpperView addSubview:_prjNameField];
    //[_prjNameField setBackgroundColor:[UIColor lightGrayColor]];
    [_prjNameField setTextColor:[UIColor whiteColor]];
    
    [_prjNameField setText:[_selectedDictionary objectForKey:@"projectName"]];
    [_prjNameField setUserInteractionEnabled:NO];
    
    
    _btnEditEnable=[[UIButton alloc] initWithFrame:CGRectMake(_upperTopView.frame.size.width-40, 10, 30, 30)];
    [_btnEditEnable setBackgroundImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
    [_upperTopView addSubview:_btnEditEnable];
    [_btnEditEnable addTarget:self action:@selector(editBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    _btnBack=[[UIButton alloc] initWithFrame:CGRectMake(10, 12.5, 25, 25)];
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [_upperTopView addSubview:_btnBack];
    [_btnBack addTarget:self action:@selector(toGoBackFromEditor) forControlEvents:UIControlEventTouchUpInside];
    
    
    
     [self performSelector:@selector(selectAsset) withObject:nil afterDelay:0.05];
}
-(void)toGoBackFromEditor
{
    if([_prjNameField isFirstResponder])
    {
        [_prjNameField resignFirstResponder];
        
    }
    if(_mPlayer!=nil)
    {
        [_mPlayer pause];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)editBtnClicked:(UIButton*)btn
{
   if(_canEdit)
   {
       [_prjNameField setUserInteractionEnabled:YES];
       [_prjNameField becomeFirstResponder];
   }
    else
    {
        _modifiedTest=_prjNameField.text;
        [_prjNameField setUserInteractionEnabled:NO];
        [_prjNameField resignFirstResponder];

        if(![_modifiedTest isEqualToString:_initailTest])
        {
            NSLog(@"update project name here");
            NSMutableDictionary *dct=[_selectedDictionary mutableCopy];
            [dct setObject:_modifiedTest forKey:@"projectName"];
            [self.projectUpdateDelegate projectDetailModifiew:dct forIndex:_selectedIndex];
        }
    }
    _canEdit=!_canEdit;
}
-(void)selectAsset
{
    
    
    NSLog(@"selectAsset assest=%@",self.asset);
    
    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:self.asset];
    
    self.mPlayer = [AVPlayer playerWithPlayerItem:item];
    self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.mPlayer];
    self.playerLayer.contentsGravity = AVLayerVideoGravityResizeAspect;
    self.mPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    self.playerLayer.contentsGravity = AVLayerVideoGravityResizeAspectFill;
    [self.playerLayer setBackgroundColor:[UIColor clearColor].CGColor];
    
   
    
    
    
    
    self.playerLayer.frame = CGRectMake(0, 0,self.videoPlayer.frame.size.width, self.videoPlayer.frame.size.height);
    
    [self.videoLayer.layer insertSublayer:self.playerLayer atIndex:0];
    self.mPlayer.volume=0;
    [self.mPlayer play];
    
    
   /*
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnVideoLayer:)];
    [self.videoLayer addGestureRecognizer:tap];
    
    self.videoPlaybackPosition = 0;
    */
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
