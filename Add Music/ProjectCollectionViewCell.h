//
//  ProjectCollectionViewCell.h
//  Add Music
//
//  Created by Puneet Kalra on 06/01/19.
//  Copyright © 2019 Atul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectCollectionViewCell : UICollectionViewCell
@property(nonatomic) UILabel *nameLabel,*dtLabel;
@property(nonatomic) UIImageView *previewImageView;

@end
