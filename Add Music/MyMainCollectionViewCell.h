//
//  MyMainCollectionViewCell.h
//  bridgingEg
//
//  Created by Mac-Mini on 08/01/19.
//  Copyright © 2019 Fortune. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyMainCollectionViewCell : UICollectionViewCell
@property (nonatomic) UIView * mainContainerView,*backView;
@property (nonatomic)UIImageView *imgViewPreview;
@property (nonatomic)BOOL isLoaded;
@property (nonatomic)UILabel *lblName;
@end

NS_ASSUME_NONNULL_END
