//
//  MyMainCollectionViewCell.m
//  bridgingEg
//
//  Created by Mac-Mini on 08/01/19.
//  Copyright © 2019 Fortune. All rights reserved.
//

#import "MyMainCollectionViewCell.h"

@implementation MyMainCollectionViewCell
@synthesize mainContainerView,imgViewPreview,backView;
-(id)initWithFrame:(CGRect)frame
{
    if (![super initWithFrame:frame])
    {
        return nil;
    }
    _isLoaded=NO;
    
    backView=[[UIView alloc] init];
    [self.contentView addSubview:backView];
    
    mainContainerView=[[UIView alloc] init];
    mainContainerView.hidden=YES;
    [self.contentView addSubview:mainContainerView];

    imgViewPreview=[[UIImageView alloc] init];
    imgViewPreview.hidden=YES;
    [self.contentView addSubview:imgViewPreview];

    
    
    
    _lblName=[[UILabel alloc] init];
    [self.contentView addSubview:_lblName];
    return self;
}
@end
