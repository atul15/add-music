//
//  TopHeaderScroller.m
//  bridgingEg
//
//  Created by Mac-Mini on 08/01/19.
//  Copyright © 2019 Fortune. All rights reserved.
//

#import "TopHeaderScroller.h"
@interface TopHeaderScroller ()<UIScrollViewDelegate>
{
    UICollectionView *headerCollectionView;
    CGSize scrSize;
    
    NSTimer *activeTimer;
}
@property(nonatomic) UIScrollView *sliderMainScroller;

@end
@implementation TopHeaderScroller
@synthesize jsonTopBannerList;
-(id)initWithFrame:(CGRect)frame
{
    if (![super initWithFrame:frame])
    {
        return nil;
    }
    activeTimer = nil;
    jsonTopBannerList=@[@"img1.jpg",@"img2.jpg",@"img3.jpg"];
         [self setBackgroundColor:[UIColor whiteColor]];
       /* UICollectionViewFlowLayout *layout=[UICollectionViewFlowLayout alloc];
        headerCollectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 50, scrSize.width, scrSize.height-50) collectionViewLayout:layout];
        
        
        
        
        
        [headerCollectionView setDataSource:self];
        [headerCollectionView setDelegate:self];
        
        [headerCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        [headerCollectionView setBackgroundColor:[UIColor redColor]];
        
        [self addSubview:headerCollectionView];*/
        
    [self setDataOnScroll];
    return self;
}



-(void)setDataOnScroll
{
    if (activeTimer) {
        [activeTimer invalidate];
        activeTimer = nil;
    }
    _sliderMainScroller.delegate=nil;
    [_sliderMainScroller removeFromSuperview];
    _sliderMainScroller=nil;
    _sliderMainScroller=[[UIScrollView alloc] initWithFrame:CGRectMake(0,0, self.frame.size.width, self.frame.size.height)];
    [self addSubview:_sliderMainScroller];
    [_sliderMainScroller setBackgroundColor:[UIColor clearColor]];
    
   // imagesArray = [NSArray arrayWithObjects:@"prize_cup.png",@"prize_cup.png",@"prize_cup.png",nil];
    
    
    _sliderMainScroller.pagingEnabled = YES;
    _sliderMainScroller.delegate = self;
    _sliderMainScroller.contentSize = CGSizeMake(([UIScreen mainScreen].bounds.size.width * [jsonTopBannerList count] * 3), _sliderMainScroller.frame.size.height);
    
    int mainCount = 0;
    
    for (int x = 0; x < 3; x++) {
        
        for (int i=0; i < [jsonTopBannerList count]; i++)
        {
            
            CGRect frameRect;
            frameRect.origin.y = 0.0f;
            frameRect.size.width = self.frame.size.width;
            frameRect.size.height = _sliderMainScroller.frame.size.height;
            frameRect.origin.x = (frameRect.size.width * mainCount);
            
            
            UIView *v=[self getViewAtPosition:i withView:[[UIView alloc] initWithFrame:CGRectMake(frameRect.origin.x, 0, frameRect.size.width, frameRect.size.height)]];
            
            
            [_sliderMainScroller addSubview:v];
            
            
            
            mainCount++;
        }
        
    }
    
    CGFloat startX = (CGFloat)[jsonTopBannerList count] * [UIScreen mainScreen].bounds.size.width;
    [_sliderMainScroller setContentOffset:CGPointMake(startX, 0) animated:NO];
    
    if (([jsonTopBannerList count] > 1))
    {
        [self startTimerThread];
    }
}
-(UIView*)getViewAtPosition:(int)i withView:(UIView*)v
{
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, v.frame.size.width-10, v.frame.size.height-10)];
    [imageV.layer setCornerRadius:10];
    [imageV setClipsToBounds:YES];
    imageV.contentMode = UIViewContentModeScaleAspectFill;
    imageV.image = [UIImage imageNamed:(NSString *)[jsonTopBannerList objectAtIndex:i]];
    [v addSubview:imageV];
    return v;
}

-(void)toStartStopTimer:(BOOL)toStop
{
    if(toStop)
    {
        if (activeTimer) {
            [activeTimer invalidate];
            activeTimer = nil;
        }
    }
    else if (!toStop)
    {
        [self startTimerThread];
    }
}

-(void)startTimerThread
{
    if (activeTimer) {
        [activeTimer invalidate];
        activeTimer = nil;
    }
    activeTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(slideImage) userInfo:nil repeats:YES];
}
- (void)slideImage
{
    
    CGFloat startX = 0.0f;
    CGFloat width = _sliderMainScroller.frame.size.width;
    NSInteger page = (_sliderMainScroller.contentOffset.x + (0.5f * width)) / width;
    NSInteger nextPage = page + 1;
    startX = (CGFloat)nextPage * width;
    [_sliderMainScroller setContentOffset:CGPointMake(startX, 0) animated:YES];
}

#pragma mark - UIScrollView delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSLog(@"scrollViewDidEndDecelerating");
    
    CGFloat width = scrollView.frame.size.width;
    NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
    
    NSInteger moveToPage = page;
    if (moveToPage == 0) {
        
        moveToPage = [jsonTopBannerList count];
        CGFloat startX = (CGFloat)moveToPage * [UIScreen mainScreen].bounds.size.width;
        [scrollView setContentOffset:CGPointMake(startX, 0) animated:NO];
        
    } else if (moveToPage == (([jsonTopBannerList count] * 3) - 1)) {
        
        moveToPage = [jsonTopBannerList count] - 1;
        CGFloat startX = (CGFloat)moveToPage * [UIScreen mainScreen].bounds.size.width;
        [scrollView setContentOffset:CGPointMake(startX, 0) animated:NO];
        
    }
    
    
    if (([jsonTopBannerList count] > 1))
    {
        [self startTimerThread];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    
    NSLog(@"scrollViewDidEndScrollingAnimation");
    CGFloat width = scrollView.frame.size.width;
    NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
    
    NSInteger moveToPage = page;
    if (moveToPage == 0) {
        
        moveToPage = [jsonTopBannerList count];
        CGFloat startX = (CGFloat)moveToPage * [UIScreen mainScreen].bounds.size.width;
        [scrollView setContentOffset:CGPointMake(startX, 0) animated:NO];
        
    } else if (moveToPage == (([jsonTopBannerList count] * 3) - 1)) {
        
        moveToPage = [jsonTopBannerList count] - 1;
        CGFloat startX = (CGFloat)moveToPage * [UIScreen mainScreen].bounds.size.width;
        [scrollView setContentOffset:CGPointMake(startX, 0) animated:NO];
        
    }
    
    
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    NSLog(@"scrollViewWillBeginDragging");
    
    if (activeTimer) {
        [activeTimer invalidate];
        activeTimer = nil;
    }
}

@end
