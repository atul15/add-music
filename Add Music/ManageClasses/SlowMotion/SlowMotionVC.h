//
//  SlowMotionVC.h
//  VideoMaker
//
//  Created by Sohil on 8/7/17.
//  Copyright © 2017 iAppz Studio. All rights reserved.
//
//shareSegue
//musicSegue
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import <AssetsLibrary/ALAsset.h>
#import "MBProgressHUD.h"
#import "ConstantFile.h"
#import "ShareVideoVC.h"
#import "AudioVC.h"
#import "StepSlider.h"

@interface SlowMotionVC : UIViewController<GADInterstitialDelegate>
{
    NSString *status;
    float videoSpeed;
    NSTimer *timer1;
}
@property (strong, nonatomic) NSURL *videoUrl;

@property(nonatomic, strong) GADInterstitial *interstitial;

@property (strong, nonatomic) IBOutlet UIImageView *previewImage;
@property (nonatomic, strong) IBOutlet StepSlider *sliderView;

- (IBAction)changeValue:(StepSlider *)sender;

- (IBAction)backClick:(id)sender;
- (IBAction)doneClick:(id)sender;
- (IBAction)skipClick:(id)sender;
- (IBAction)preiveiwClick:(id)sender;
- (IBAction)addMusicClick:(id)sender;

@end
