//  SlowMotionVC.m
//  VideoMaker
//  Created by Sohil on 8/7/17.
//  Copyright © 2017 iAppz Studio. All rights reserved.


#import "SlowMotionVC.h"

@interface SlowMotionVC ()

@end

@implementation SlowMotionVC


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"SlowMotionVC viewDidLoad");

    timer1 = [NSTimer scheduledTimerWithTimeInterval:5.0
                                              target:self
                                            selector:@selector(insertialAds)
                                            userInfo:nil
                                             repeats:NO];

    videoSpeed = 1;
    self.sliderView.labels = @[@"-3x",@"-2x", @"-1x", @"0", @"1.5x", @"2x",@"3x"];
    self.sliderView.labelOffset = 0.f;
    self.previewImage.image = [self videoThumbNail:self.videoUrl];
    self.previewImage.frame = CGRectMake(0, 48, ScreenWidth, ScreenHeight-40-40-150);
}

- (UIImage *)videoThumbNail:(NSURL *)sourceURL {
    AVAsset *asset = [AVAsset assetWithURL:sourceURL];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMake(1, 1);
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    
    return thumbnail;
}

#pragma mark - Change Slider Value

- (IBAction)changeValue:(StepSlider *)sender {
    
    if (sender.index == 0)
    {
        videoSpeed = 3.5;
    }
    else if (sender.index == 1)
    {
        videoSpeed = 2.0;
    }
    else if (sender.index == 2)
    {
        videoSpeed = 1.5;
    }
    else if (sender.index == 3)
    {
        videoSpeed = 1.0;
    }
    else if (sender.index == 4)
    {
        videoSpeed = 0.75;
    }
    else if (sender.index == 5)
    {
        videoSpeed = 0.50;
    }
    else if (sender.index == 6)
    {
        videoSpeed = 0.25;
    }
    
//    NSLog(@"Vidio Speed : %f",videoSpeed);
}



- (void)SlowMotion:(NSURL *)URl {
    AVURLAsset* videoAsset = [AVURLAsset URLAssetWithURL:URl options:nil]; //self.inputAsset;
    
    AVAsset *currentAsset = [AVAsset assetWithURL:URl];
    AVAssetTrack *vdoTrack = [[currentAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    //create mutable composition
    AVMutableComposition *mixComposition = [AVMutableComposition composition];
    
    AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    BOOL hasAudio = [currentAsset tracksWithMediaType:AVMediaTypeAudio].count > 0;
    
    AVMutableCompositionTrack *compositionAudioTrack;
    if (hasAudio) {
        compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    }
    
    NSError *videoInsertError = nil;
    BOOL videoInsertResult = [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                                                            ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]
                                                             atTime:kCMTimeZero
                                                              error:&videoInsertError];
    if (!videoInsertResult || nil != videoInsertError) {
        //handle error
        return;
    }
    
    NSError *audioInsertError =nil;
    
    BOOL audioInsertResult  = NO;
    if (hasAudio) {
         audioInsertResult =[compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                                                               ofTrack:[[currentAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
                                                                atTime:kCMTimeZero
                                                                 error:&audioInsertError];
        
    }
    
    CMTime duration =kCMTimeZero;
    duration=CMTimeAdd(duration, currentAsset.duration);
    //slow down whole video by 2.0
    double videoScaleFactor = videoSpeed;
    CMTime videoDuration = videoAsset.duration;
    
    [compositionVideoTrack scaleTimeRange:CMTimeRangeMake(kCMTimeZero, videoDuration)
                               toDuration:CMTimeMake(videoDuration.value*videoScaleFactor, videoDuration.timescale)];
    if (hasAudio) {
        [compositionAudioTrack scaleTimeRange:CMTimeRangeMake(kCMTimeZero, videoDuration)
                                   toDuration:CMTimeMake(videoDuration.value*videoScaleFactor, videoDuration.timescale)];
        
    }
    
    [compositionVideoTrack setPreferredTransform:vdoTrack.preferredTransform];
    //
    AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSString *filevideoNameName =[NSString stringWithFormat:@"%@_slowMotion.mov",app.currentFileStartingName];
    app.currentFileName=filevideoNameName;
    
    NSString *outputFilePath = [app.rootFolderPath stringByAppendingPathComponent:filevideoNameName];
    
    
    /*  NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
     NSString *docsDir = [dirPaths objectAtIndex:0];
     NSString *outputFilePath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"slowMotion.mov"]];
     */
    //
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:outputFilePath])
        [[NSFileManager defaultManager] removeItemAtPath:outputFilePath error:nil];
    NSURL *_filePath = [NSURL fileURLWithPath:outputFilePath];
    
    //export
    AVAssetExportSession* assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition
                                                                         presetName:AVAssetExportPresetHighestQuality];
   
   
    AVAssetWriter *exporter;
    assetExport.outputURL=_filePath;
    assetExport.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.shouldOptimizeForNetworkUse = YES;
    [assetExport exportAsynchronouslyWithCompletionHandler:^
     {
         
         NSLog(@"ERROR : %@",assetExport.error);
         
         switch ([assetExport status]) {
             case AVAssetExportSessionStatusFailed:
             {
                 NSLog(@"Export session faiied with error: %@", [assetExport error]);
                 dispatch_async(dispatch_get_main_queue(), ^{
                     // completion(nil);
                 });
             }
                 break;
             case AVAssetExportSessionStatusCompleted:
             {
                 
                 NSLog(@"Successful");
                 NSURL *outputURL = assetExport.outputURL;
                 
                 NSLog(@"OUTPUT URL : %@",outputURL);
                 
                 ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                 if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
                     //[self writeExportedVideoToAssetsLibrary:outputURL];
                 }
                 
                 if ([status isEqualToString:@"preview"])
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [MBProgressHUD hideHUDForView:self.view animated:YES];
                         MPMoviePlayerViewController *movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:assetExport.outputURL];
                         [self presentMoviePlayerViewControllerAnimated:movieController];
                     });
 
                 }
                 else if ([status isEqualToString:@"addMusic"])
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [MBProgressHUD hideHUDForView:self.view animated:YES];
                         [self performSegueWithIdentifier:@"musicSegue" sender:outputURL];
                     });
                 }
                 else
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [self toGoBackWithVideoUrl:assetExport.outputURL] ;// [self performSegueWithIdentifier:@"shareSegue" sender:assetExport.outputURL];
                     });
                 }

                 
             }
                 break;
             default:
                 
                 break;
         }
         
         
     }];
    
    
}

-(void)toGoBackWithVideoUrl:(NSURL*)outputURL
{
    AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
    app.currentAssestInfo=[NSDictionary dictionaryWithObjectsAndKeys:outputURL,@"assetUrl",[self generateThumbImage:outputURL],@"thumbImage",@YES,@"fromLocal", nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(UIImage *)generateThumbImage : (NSURL *)url
{
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = [asset duration];
    time.value = 0;
    float duration = CMTimeGetSeconds([asset duration]);
    CGImageRef imgRef = [imageGenerator copyCGImageAtTime:CMTimeMake(1.0, duration) actualTime:NULL error:nil];
    UIImage* thumbnail = [[UIImage alloc] initWithCGImage:imgRef scale:UIViewContentModeScaleAspectFit orientation:UIImageOrientationUp];
    return thumbnail;
}
/*
- (IBAction)backClick:(id)sender {
    [self invalidTimer];
    [self.navigationController popViewControllerAnimated:YES];
}*/

- (void)writeExportedVideoToAssetsLibrary :(NSURL *)url {
    NSURL *exportURL = url;
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:exportURL]) {
        [library writeVideoAtPathToSavedPhotosAlbum:exportURL completionBlock:^(NSURL *assetURL, NSError *error){
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error localizedDescription]
                                            message:[error localizedRecoverySuggestion]
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }
                if(!error)
                {
                    
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                    // [activityView setHidden:YES];
                   /* UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sucess"
                                                                        message:@"video added to gallery successfully"
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                    [alertView show];*/
                }
#if !TARGET_IPHONE_SIMULATOR
                [[NSFileManager defaultManager] removeItemAtURL:exportURL error:nil];
#endif
            });
        }];
    } else {
        NSLog(@"Video could not be exported to assets library.");
    }
    
}


- (IBAction)doneClick:(id)sender {
    [self invalidTimer];
    status = @"next";
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self SlowMotion:self.videoUrl];
}

- (IBAction)preiveiwClick:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    status = @"preview";
    [self SlowMotion:self.videoUrl];
}

- (IBAction)addMusicClick:(id)sender {
    [self invalidTimer];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    status = @"addMusic";
    [self SlowMotion:self.videoUrl];
}


- (IBAction)backClick:(id)sender {
    [self invalidTimer];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)skipClick:(id)sender {
    [self invalidTimer];
    [self toGoBackWithVideoUrl:self.videoUrl] ;//[self performSegueWithIdentifier:@"shareSegue" sender:self.videoUrl];
}

#pragma mark - Advertise Method

- (void)invalidTimer {
    [timer1 invalidate];
    timer1 = nil;
}

- (void)insertialAds {
    [self createAndLoadInterstitial];
}

- (void)createAndLoadInterstitial {
    self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:AD_INTERSTITIAL_UNIT_ID];
    self.interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
    //request.testDevices = @[ kGADSimulatorID, @"c8c2515ab174a9ce1943e7d0cfc244e7" ];
    [self.interstitial loadRequest:request];
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    [self.interstitial presentFromRootViewController:self];
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"INSERTIAL ADS CLOSE");
    
    timer1 = [NSTimer scheduledTimerWithTimeInterval:60.0
                                              target:self
                                            selector:@selector(insertialAds)
                                            userInfo:nil
                                             repeats:NO];
    
}


- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"shareSegue"]){
        ShareVideoVC *controller = (ShareVideoVC *)segue.destinationViewController;
        controller.videoUrl=sender;
    }
    else if([segue.identifier isEqualToString:@"musicSegue"]){
        AudioVC *controller = (AudioVC *)segue.destinationViewController;
        controller.videoUrl=sender;
        controller.isFrom=@"Slowmotion";
    }

}


@end
