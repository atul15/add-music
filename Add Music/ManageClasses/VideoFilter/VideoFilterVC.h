//
//  VideoFilterVC.h
//  VideoMaker
//
//  Created by Sohil on 8/11/17.
//  Copyright © 2017 iAppz Studio. All rights reserved.
//

//videoEditingSegue

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import "VideoEditing.h"
#import "MBProgressHUD.h"

@interface VideoFilterVC : UIViewController<GADInterstitialDelegate>
{
    NSTimer *timer1;
}

@property (strong, nonatomic) NSURL *videoUrl;
@property (nonatomic, strong) AVAsset *asset;
@property (nonatomic, strong) AVPlayerLayer *playerLayer;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) NSArray *filters;
@property (nonatomic, strong) AVMutableComposition *mixComposition;
@property (nonatomic, strong) AVMutableComposition *originalMixComposition;
@property (nonatomic, strong) AVVideoComposition *videoComposition;

@property(nonatomic, strong) GADInterstitial *interstitial;

@property (strong, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet UIScrollView *filterScrollView;

- (IBAction)backClick:(id)sender;
- (IBAction)skipClick:(id)sender;
- (IBAction)doneClick:(id)sender;

@end
