//
//  VideoFilterVC.m
//  VideoMaker
//
//  Created by Sohil on 8/11/17.
//  Copyright © 2017 iAppz Studio. All rights reserved.
//

#import "VideoFilterVC.h"
#import "AppDelegate.h"
@interface VideoFilterVC ()

@end

@implementation VideoFilterVC


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"VideoFilterVC viewDidLoad");

    timer1 = [NSTimer scheduledTimerWithTimeInterval:5.0
                                              target:self
                                            selector:@selector(insertialAds)
                                            userInfo:nil
                                             repeats:NO];

    self.view.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0];

    
    self.asset = [AVAsset assetWithURL:self.videoUrl];


    
    self.player = [[AVPlayer alloc] init];
    
    self.originalMixComposition = [LTAVAssetUtils mixCompositionWithAsset:self.asset];
    
    self.mixComposition = [self.originalMixComposition mutableCopy];
    [self resetPlayerItemWithAsset:self.mixComposition];
    
    self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    self.playerLayer.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight-70);
    [self.view.layer addSublayer:self.playerLayer];

    [self.view addSubview:self.topView];
    
    [self FiltersType];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.player play];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.player pause];
}




#pragma mark - Process Video

- (IBAction)doneClick:(id)sender {
    
     NSLog(@"doneClick here");
    [self.player pause];
    
    
    AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
    
     NSString *filevideoNameName =[NSString stringWithFormat:@"%@_EffectVideo.mp4",app.currentFileStartingName];
    app.currentFileName=filevideoNameName;
    
    NSString *path = [app.rootFolderPath stringByAppendingPathComponent:filevideoNameName];

    NSLog(@"doneClick here app.currentFileName=%@ app.rootFolderPath=%@ path=%@",app.currentFileName,app.rootFolderPath,path);

    /*
    NSString *videoName = [NSString stringWithFormat:@"EffectVideo.mp4"];
   NSString *documentsDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *path = [documentsDirPath stringByAppendingPathComponent:videoName];
    */
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:path error: &error];
    
    NSURL *outputUrl = [NSURL fileURLWithPath:path];
    
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:self.mixComposition presetName:AVAssetExportPresetHighestQuality];
    
    exportSession.videoComposition = self.playerItem.videoComposition;
    exportSession.outputURL = outputUrl;
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.shouldOptimizeForNetworkUse = YES;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self exportDidFinish:exportSession];
        });
    }];
}

- (void)exportDidFinish:(AVAssetExportSession*)session {
    
    NSLog(@"ERROR : %@",session.error);
    
    if (session.status == AVAssetExportSessionStatusCompleted) {
        NSURL *outputURL = session.outputURL;
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
            
            [self performSegueWithIdentifier:@"videoEditingSegue" sender:outputURL];
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        }
    }
}

#pragma mark - Video Filter

- (void)FiltersType {
    
    int x = 0;
    
    for (int i=0; i<=16; i++) {
        
        UIImageView *filterImageView = [[UIImageView alloc]init];
        filterImageView.frame = CGRectMake(x+5, 7, 50, 50);
        filterImageView.layer.cornerRadius = 25;
        filterImageView.clipsToBounds = YES;
        [self.filterScrollView addSubview:filterImageView];
        
        UILabel *lblFilterName = [[UILabel alloc] init];
        lblFilterName.frame = CGRectMake(x+5, 60, 50, 10);
        lblFilterName.font = [UIFont systemFontOfSize:9.0];
        lblFilterName.textColor = [UIColor whiteColor];
        lblFilterName.textAlignment = NSTextAlignmentCenter;
        [self.filterScrollView addSubview:lblFilterName];
        
        UIButton *btnFilter = [[UIButton alloc]init];
        btnFilter.frame = CGRectMake(x+5, 0, 50, 70);
        //btnFilter.layer.borderWidth = 1.0;
        btnFilter.tag = i;
        [btnFilter addTarget:self action:@selector(filterClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.filterScrollView addSubview:btnFilter];
        
        x+=55;
        
        //Filter
        CIImage *inputImage = [[CIImage alloc] initWithImage:[UIImage imageNamed:@"filterImg.png"]];
        CIFilter * filter;
        
        if (i == 0)
        {
            filterImageView.image = [UIImage imageNamed:@"filterImg.png"];
            lblFilterName.text = @"Original";
        }
        else if (i==1)
        {
            //Fade
            filter = [CIFilter filterWithName:@"CIPhotoEffectFade"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            
            lblFilterName.text = @"Fade";
        }
        else if (i == 2)
        {
            //Chrome
            filter = [CIFilter filterWithName:@"CIPhotoEffectChrome"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            lblFilterName.text = @"Chrome";
        }
        else if (i == 3)
        {
            //Process
            filter = [CIFilter filterWithName:@"CIPhotoEffectProcess"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            lblFilterName.text = @"Process";
        }
        else if (i == 4)
        {
            //Transfer
            filter = [CIFilter filterWithName:@"CIPhotoEffectTransfer"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            lblFilterName.text = @"Transfer";
        }
        else if (i == 5)
        {
            //Instant
            filter = [CIFilter filterWithName:@"CIPhotoEffectInstant"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            lblFilterName.text = @"Instant";
        }
        else if (i == 6)
        {
            //Sepia
            filter = [CIFilter filterWithName:@"CISepiaTone"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            lblFilterName.text = @"Sepia";
        }
        else if (i == 7)
        {
            //Mono
            filter = [CIFilter filterWithName:@"CIColorMonochrome"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            lblFilterName.text = @"Mono";
        }
        else if (i == 8)
        {
            //Tonal
            filter = [CIFilter filterWithName:@"CIPhotoEffectTonal"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            lblFilterName.text = @"Tonal";
        }
        else if (i == 9)
        {
            //Noir
            filter = [CIFilter filterWithName:@"CIPhotoEffectNoir"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            lblFilterName.text = @"Noir";
        }
        else if (i == 10)
        {
            //Blure
            filter = [CIFilter filterWithName:@"CIBoxBlur"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            lblFilterName.text = @"Blure";
        }
        else if (i == 11)
        {
            //Clamp
            filter = [CIFilter filterWithName:@"CIColorClamp"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            [filter setValue:[CIVector vectorWithX:0.1 Y:0.1 Z:0.3 W:0.0] forKey:@"inputMinComponents"];
            [filter setValue:[CIVector vectorWithX:0.5 Y:0.7 Z:0.7 W:1.0]  forKey:@"inputMaxComponents"];
            lblFilterName.text = @"Clamp";
        }
        else if (i == 12)
        {
            //Hue
            filter = [CIFilter filterWithName:@"CIHueAdjust"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            [filter setValue: [NSNumber numberWithFloat: 0.5] forKey: @"inputAngle"];
            lblFilterName.text = @"Hue";
        }
        else if (i == 13)
        {
            //ToneCurve
            filter = [CIFilter filterWithName:@"CIToneCurve"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            [filter setDefaults];
            [filter setValue:[CIVector vectorWithX:0.00 Y:0.00] forKey:@"inputPoint0"];
            [filter setValue:[CIVector vectorWithX:0.25 Y:0.15] forKey:@"inputPoint1"];
            [filter setValue:[CIVector vectorWithX:0.50 Y:0.50] forKey:@"inputPoint2"];
            [filter setValue:[CIVector vectorWithX:0.75 Y:0.85] forKey:@"inputPoint3"];
            [filter setValue:[CIVector vectorWithX:1.00 Y:1.00] forKey:@"inputPoint4"];
            lblFilterName.text = @"ToneCurve";
        }
        else if (i == 14)
        {
            //Linear
            filter = [CIFilter filterWithName:@"CISRGBToneCurveToLinear"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            lblFilterName.text = @"Linear";
        }
        else if (i == 15)
        {
            //Exposure
            filter = [CIFilter filterWithName:@"CIExposureAdjust"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            [filter setValue:[NSNumber numberWithFloat: 2.0f] forKey:@"inputEV"];
            lblFilterName.text = @"Exposure";
        }
        else if (i == 16)
        {
            //Invert
            filter = [CIFilter filterWithName:@"CIColorInvert"];
            [filter setValue:inputImage forKey:kCIInputImageKey];
            lblFilterName.text = @"Invert";
        }

        
            if (i!=0) {
                CIImage *displayImage = filter.outputImage;
                UIImage *finalImage = [UIImage imageWithCIImage:displayImage];
                //Set Filter Image
                filterImageView.image = finalImage;
            }

       

        
    }
    
    self.filterScrollView.contentSize = CGSizeMake((50*17 + 5*17), 65);

}

- (void)filterClick:(UIButton *)button {

    self.mixComposition = [self.originalMixComposition mutableCopy];
    CIContext *context = [CIContext contextWithOptions:@{kCIContextWorkingColorSpace : [NSNull null], kCIContextOutputColorSpace : [NSNull null]}];
    
    self.videoComposition = [AVVideoComposition videoCompositionWithAsset:self.mixComposition applyingCIFiltersWithHandler:^(AVAsynchronousCIImageFilteringRequest * _Nonnull request) {
        CIImage *image = request.sourceImage.imageByClampingToExtent;
        image = [self applayFilter:image withTag:button.tag withRequest:request];
        [request finishWithImage:image context:context];
    }];
    
    // config player
    self.playerItem = [[AVPlayerItem alloc] initWithAsset:self.mixComposition];
    self.playerItem.videoComposition = self.videoComposition;
    self.playerItem.audioTimePitchAlgorithm = AVAudioTimePitchAlgorithmTimeDomain;
    
    [self.player replaceCurrentItemWithPlayerItem:self.playerItem];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserverForName:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem queue:nil usingBlock:^(NSNotification *note) {
        [self.player seekToTime:kCMTimeZero];
        [self.player play];
    }];
}

- (void)resetPlayerItemWithAsset:(AVAsset *)asset {
    
    // video composition
    CIContext *context = [CIContext contextWithOptions:@{kCIContextWorkingColorSpace : [NSNull null], kCIContextOutputColorSpace : [NSNull null]}];
    self.videoComposition = [AVVideoComposition videoCompositionWithAsset:asset applyingCIFiltersWithHandler:^(AVAsynchronousCIImageFilteringRequest * _Nonnull request) {
        CIImage *image = request.sourceImage.imageByClampingToExtent;
        [request finishWithImage:image context:context];
    }];
    
    // config player
    self.playerItem = [[AVPlayerItem alloc] initWithAsset:asset];
    self.playerItem.videoComposition = self.videoComposition;
    self.playerItem.audioTimePitchAlgorithm = AVAudioTimePitchAlgorithmTimeDomain;
    
    [self.player replaceCurrentItemWithPlayerItem:self.playerItem];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserverForName:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem queue:nil usingBlock:^(NSNotification *note) {
        [self.player seekToTime:kCMTimeZero];
        [self.player play];
    }];
}

- (CIImage *)applayFilter:(CIImage *)image withTag:(int)tag withRequest:(AVAsynchronousCIImageFilteringRequest * _Nonnull)request {
    if (tag == 0) {
        return image;
    }
    else if (tag == 1)
    {
        image =[image imageByApplyingFilter:@"CIPhotoEffectFade" withInputParameters:nil];
    }
    else if (tag == 2)
    {
        image =[image imageByApplyingFilter:@"CIPhotoEffectChrome" withInputParameters:nil];
    }
    else if (tag == 3)
    {
        image =[image imageByApplyingFilter:@"CIPhotoEffectProcess" withInputParameters:nil];;
    }
    else if (tag == 4)
    {
        image =[image imageByApplyingFilter:@"CIPhotoEffectTransfer" withInputParameters:nil];;
    }
    else if (tag == 5)
    {
        image =[image imageByApplyingFilter:@"CIPhotoEffectInstant" withInputParameters:nil];;
    }
    else if (tag == 6)
    {
        image =[image imageByApplyingFilter:@"CISepiaTone" withInputParameters:nil];;
    }
    else if (tag == 7)
    {
        image =[image imageByApplyingFilter:@"CIColorMonochrome" withInputParameters:nil];;
    }
    else if (tag == 8)
    {
        image =[image imageByApplyingFilter:@"CIPhotoEffectTonal" withInputParameters:nil];;
    }
    else if (tag == 9)
    {
        image =[image imageByApplyingFilter:@"CIPhotoEffectNoir" withInputParameters:nil];;
    }
    else if (tag == 10)
    {
        image =[image imageByApplyingFilter:@"CIBoxBlur" withInputParameters:nil];;
    }
    else if (tag == 11)
    {
        CIFilter *filter = [CIFilter filterWithName:@"CIColorClamp"];
        [filter setValue:image forKey:kCIInputImageKey];
        
        // Vary filter parameters based on video timing
        Float64 seconds = CMTimeGetSeconds(request.compositionTime);
        NSNumber *tempNumber = [[NSNumber alloc] initWithDouble:seconds*10.0];
        [filter setValue:[CIVector vectorWithX:0.1 Y:0.1 Z:0.3 W:0.0] forKey:@"inputMinComponents"];
        [filter setValue:[CIVector vectorWithX:0.5 Y:0.7 Z:0.7 W:1.0]  forKey:@"inputMaxComponents"];
        
        // Crop the blurred output to the bounds of the original image
        CIImage *output = [filter.outputImage imageByCroppingToRect:request.sourceImage.extent];

        image = output;
    }
    else if (tag == 12)
    {
       // image =[image imageByApplyingFilter:@"CIHueAdjust" withInputParameters:nil];
        
        CIFilter *filter = [CIFilter filterWithName:@"CIHueAdjust"];
        [filter setValue:image forKey:kCIInputImageKey];
        
        // Vary filter parameters based on video timing
        [filter setValue: [NSNumber numberWithFloat: 0.5] forKey: @"inputAngle"];
        
        // Crop the blurred output to the bounds of the original image
        CIImage *output = [filter.outputImage imageByCroppingToRect:request.sourceImage.extent];
       
        image = output;
    }
    else if (tag == 13)
    {
//        image =[image imageByApplyingFilter:@"CIToneCurve" withInputParameters:nil];;
        
        CIFilter *filter = [CIFilter filterWithName:@"CIToneCurve"];
        [filter setValue:image forKey:kCIInputImageKey];
        
        // Vary filter parameters based on video timing
        [filter setDefaults];
        [filter setValue:[CIVector vectorWithX:0.00 Y:0.00] forKey:@"inputPoint0"];
        [filter setValue:[CIVector vectorWithX:0.25 Y:0.15] forKey:@"inputPoint1"];
        [filter setValue:[CIVector vectorWithX:0.50 Y:0.50] forKey:@"inputPoint2"];
        [filter setValue:[CIVector vectorWithX:0.75 Y:0.85] forKey:@"inputPoint3"];
        [filter setValue:[CIVector vectorWithX:1.00 Y:1.00] forKey:@"inputPoint4"];
        
        // Crop the blurred output to the bounds of the original image
        CIImage *output = [filter.outputImage imageByCroppingToRect:request.sourceImage.extent];
        
        image = output;
    }
    else if (tag == 14)
    {
        image =[image imageByApplyingFilter:@"CISRGBToneCurveToLinear" withInputParameters:nil];;
    }
    else if (tag == 15)
    {
//        image =[image imageByApplyingFilter:@"CIExposureAdjust" withInputParameters:nil];
        
        CIFilter *filter = [CIFilter filterWithName:@"CIExposureAdjust"];
        [filter setValue:image forKey:kCIInputImageKey];
        
        // Vary filter parameters based on video timing
        [filter setValue:[NSNumber numberWithFloat: 2.0f] forKey:@"inputEV"];
        
        // Crop the blurred output to the bounds of the original image
        CIImage *output = [filter.outputImage imageByCroppingToRect:request.sourceImage.extent];
        
        image = output;
    }
    else if (tag == 16)
    {
        image =[image imageByApplyingFilter:@"CIColorInvert" withInputParameters:nil];;
    }
    else
    {
        image =[image imageByApplyingFilter:@"CIColorMonochrome" withInputParameters:nil];;
    }
        
    return image;
}




- (IBAction)backClick:(id)sender {
    [self invalidTimer];
    [self.player pause];
     NSLog(@"backClick here");
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)skipClick:(id)sender {
    NSLog(@"skipClick here");
    [self performSegueWithIdentifier:@"videoEditingSegue" sender:self.videoUrl];
}


#pragma mark - Advertise Method

- (void)invalidTimer {
    [timer1 invalidate];
    timer1 = nil;
}

- (void)insertialAds {
    [self createAndLoadInterstitial];
}

- (void)createAndLoadInterstitial {
    self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:AD_INTERSTITIAL_UNIT_ID];
    self.interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
    //request.testDevices = @[ kGADSimulatorID, @"c8c2515ab174a9ce1943e7d0cfc244e7" ];
    [self.interstitial loadRequest:request];
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    [self.interstitial presentFromRootViewController:self];
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"INSERTIAL ADS CLOSE");
    
    timer1 = [NSTimer scheduledTimerWithTimeInterval:60.0
                                              target:self
                                            selector:@selector(insertialAds)
                                            userInfo:nil
                                             repeats:NO];
    
}






#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    [self invalidTimer];
    
    if([segue.identifier isEqualToString:@"videoEditingSegue"]){
        VideoEditing *controller = (VideoEditing *)segue.destinationViewController;
        controller.videoUrl=sender;
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
