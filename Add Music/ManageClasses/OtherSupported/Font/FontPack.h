//
//  FontPack.h
//  pb2free
//
//  Created by Chow Shung Chee on 26/09/13.
//  Copyright (c) 2013 Aurora Mind. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FontPack : NSObject

+ (NSString *)findPaidFont:(NSInteger)tag;
+ (NSString *)findFont:(NSInteger)tag;
+ (int)totalColour;
+ (UIColor *)findColour:(int)tag;

@end
