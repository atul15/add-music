//
//  FontPack.m
//  pb2free
//
//  Created by Chow Shung Chee on 26/09/13.
//  Copyright (c) 2013 Aurora Mind. All rights reserved.
//

#import "FontPack.h"

@implementation FontPack

+ (NSString *)findFont:(NSInteger)tag
{
    NSString *name;
    
    switch (tag)
    {
        case 1:
            name = @"AmericanTypewriter-Bold";
            break;
            
        case 2:
            name = @"Courier-BoldOblique";
            break;
            
        case 3:
            name = @"AvenirNext-BoldItalic";
            break;
            
        case 4:
            name = @"Courier-Oblique";
            break;
            
        case 5:
            name = @"HiraMinProN-W6";
            break;
            
        case 6:
            name = @"OriyaSangamMN";
            break;
            
        case 7:
            name = @"Devanagari Sangam MN";
            break;
            
        case 8:
            name = @"SnellRoundhand";
            break;
            
        case 9:
            name = @"ZapfDingbatsITC";
            break;
            
        case 10:
            name = @"Verdana-Bold";
            break;
            
        case 11:
            name = @"Baskerville";
            break;
            
        case 12:
            name = @"Khmer Sangam MN";
            break;
            
        case 13:
            name = @"Didot";
            break;
            
        case 14:
            name = @"SavoyeLetPlain";
            break;
            
        case 15:
            name = @"BodoniOrnamentsITCTT";
            break;
            
        case 16:
            name = @"Courier New";
            break;
            
        case 17:
            name = @"Gill Sans";
            break;
            
        case 18:
            name = @"Apple SD Gothic Neo";
            break;
            
        case 19:
            name = @"MarkerFelt-Wide";
            break;
            
        case 20:
            name = @"GurmukhiMN";
            break;
            
        case 21:
            name = @"Times New Roman";
            break;
            
        case 22:
            name = @"Apple Color Emoji";
            break;
            
        case 23:
            name = @"Kailasa";
            break;
            
        case 24:
            name = @"KohinoorDevanagari-Semibold";
            break;
            
        case 25:
            name = @"ChalkboardSE-Regular";
            break;
        
        case 26:
            name = @"PingFang TC";
            break;
        
        case 27:
            name = @"Damascus";
            break;
            
        case 28:
            name = @"MarkerFelt-Thin";
            break;
            
        default:
            name = @"Verdana-BoldItalic";
            break;
    }
    return name;
}

+ (NSString *)findPaidFont:(NSInteger)tag
{
    NSString *name1;
    
    switch (tag)
    {
        case 1:
            name1 = @"aaaiight! fat";
            break;
            
        case 2:
            name1 = @"aaaiight!";
            break;
            
        case 3:
            name1 = @"Arcade Interlaced";
            break;
            
        case 4:
            name1 = @"Arcade Normal";
            break;
            
        case 5:
            name1 = @"Arcade Rounded";
            break;
            
        case 6:
            name1 = @"Beatbox";
            break;
            
        case 7:
            name1 = @"Chintzy CPU BRK";
            break;
            
        case 8:
            name1 = @"Chintzy CPU Shadow BRK";
            break;
            
        case 9:
            name1 = @"Husky Stash";
            break;
            
        case 10:
            name1 = @"IntellectaCrowns";
            break;
            
        case 11:
            name1 = @"Pincoyablack";
            break;
            
        case 12:
            name1 = @"Retro Gamer";
            break;
            
        case 13:
            name1 = @"row";
            break;
            
        case 14:
            name1 = @"Scriptina Pro";
            break;
            
        default:
            name1 = @"Verdana-BoldItalic";
            break;
    }
    return name1;
}

+ (int)totalColour
{
    int total = 177;
    return total-1;
}

+ (UIColor *)findColour:(int)tag
{
   /* NSArray *array = [NSArray arrayWithObjects:
                      [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:64/255.0 green:64/255.0 blue:64/255.0 alpha:1.0],
                      [UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0],
                      [UIColor colorWithRed:160/255.0 green:160/255.0 blue:160/255.0 alpha:1.0],
                      [UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:51/255.0 blue:102/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:102/255.0 blue:102/255.0 alpha:1.0],
                      [UIColor colorWithRed:204/255.0 green:255/255.0 blue:255/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:128/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:204/255.0 blue:153/255.0 alpha:1.0],
                      [UIColor colorWithRed:228/255.0 green:176/255.0 blue:74/255.0 alpha:1.0],
                      [UIColor colorWithRed:100/255.0 green:59/255.0 blue:15/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:51/255.0 blue:51/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:102/255.0 blue:102/255.0 alpha:1.0],
                      [UIColor colorWithRed:153/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:102/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:204/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:157/255.0 green:213/255.0 blue:42/255.0 alpha:1.0],
                      [UIColor colorWithRed:153/255.0 green:255/255.0 blue:153/255.0 alpha:1.0],
                      [UIColor colorWithRed:51/255.0 green:0/255.0 blue:102/255.0 alpha:1.0],
                      [UIColor colorWithRed:115/255.0 green:44/255.0 blue:123/255.0 alpha:1.0],
                      [UIColor colorWithRed:204/255.0 green:153/255.0 blue:255/255.0 alpha:1.0],
                      [UIColor colorWithRed:238/255.0 green:219/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:190/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:255/255.0 blue:0/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:255/255.0 blue:153/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:76/255.0 blue:153/255.0 alpha:1.0],
                      [UIColor colorWithRed:0/255.0 green:102/255.0 blue:204/255.0 alpha:1.0],
                      [UIColor colorWithRed:51/255.0 green:153/255.0 blue:255/255.0 alpha:1.0],
                      [UIColor colorWithRed:120/255.0 green:213/255.0 blue:227/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:0/255.0 blue:128/255.0 alpha:1.0],
                      [UIColor colorWithRed:242/255.0 green:0/255.0 blue:86/255.0 alpha:1.0],
                      [UIColor colorWithRed:255/255.0 green:174/255.0 blue:174/255.0 alpha:1.0], // 35
                      nil];
    */
    
    NSArray *array1 = [NSArray arrayWithObjects:
                       [UIColor colorWithRed:51/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:51/255.0 green:25/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:51/255.0 green:51/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:25/255.0 green:51/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:51/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:51/255.0 blue:25/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:51/255.0 blue:51/255.0 alpha:1.0],
                       [UIColor colorWithRed:128/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:139/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:165/255.0 green:42/255.0 blue:42/255.0 alpha:1.0],
                       [UIColor colorWithRed:178/255.0 green:34/255.0 blue:34/255.0 alpha:1.0],
                       [UIColor colorWithRed:220/255.0 green:20/255.0 blue:60/255.0 alpha:1.0],
                       
                       [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:64/255.0 green:64/255.0 blue:64/255.0 alpha:1.0],
                       [UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0],
                       [UIColor colorWithRed:160/255.0 green:160/255.0 blue:160/255.0 alpha:1.0],
                       [UIColor colorWithRed:224/255.0 green:224/255.0 blue:224/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:51/255.0 blue:102/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:102/255.0 blue:102/255.0 alpha:1.0],
                       [UIColor colorWithRed:204/255.0 green:255/255.0 blue:255/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:128/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:204/255.0 blue:153/255.0 alpha:1.0],
                       [UIColor colorWithRed:228/255.0 green:176/255.0 blue:74/255.0 alpha:1.0],
                       [UIColor colorWithRed:100/255.0 green:59/255.0 blue:15/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:51/255.0 blue:51/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:102/255.0 blue:102/255.0 alpha:1.0],
                       [UIColor colorWithRed:153/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:102/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:204/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:157/255.0 green:213/255.0 blue:42/255.0 alpha:1.0],
                       [UIColor colorWithRed:153/255.0 green:255/255.0 blue:153/255.0 alpha:1.0],
                       [UIColor colorWithRed:51/255.0 green:0/255.0 blue:102/255.0 alpha:1.0],
                       [UIColor colorWithRed:115/255.0 green:44/255.0 blue:123/255.0 alpha:1.0],
                       [UIColor colorWithRed:204/255.0 green:153/255.0 blue:255/255.0 alpha:1.0],
                       [UIColor colorWithRed:238/255.0 green:219/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:190/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:255/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:255/255.0 blue:153/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:76/255.0 blue:153/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:102/255.0 blue:204/255.0 alpha:1.0],
                       [UIColor colorWithRed:51/255.0 green:153/255.0 blue:255/255.0 alpha:1.0],
                       [UIColor colorWithRed:120/255.0 green:213/255.0 blue:227/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:0/255.0 blue:128/255.0 alpha:1.0],
                       [UIColor colorWithRed:242/255.0 green:0/255.0 blue:86/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:174/255.0 blue:174/255.0 alpha:1.0],
                       
                       [UIColor colorWithRed:255/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:99/255.0 blue:71/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:127/255.0 blue:80/255.0 alpha:1.0],
                       [UIColor colorWithRed:205/255.0 green:92/255.0 blue:92/255.0 alpha:1.0],
                       [UIColor colorWithRed:240/255.0 green:128/255.0 blue:128/255.0 alpha:1.0],
                       [UIColor colorWithRed:233/255.0 green:150/255.0 blue:122/255.0 alpha:1.0],
                       [UIColor colorWithRed:250/255.0 green:128/255.0 blue:114/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:160/255.0 blue:122/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:69/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:140/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:165/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:215/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:184/255.0 green:134/255.0 blue:11/255.0 alpha:1.0],
                       [UIColor colorWithRed:218/255.0 green:165/255.0 blue:32/255.0 alpha:1.0],
                       [UIColor colorWithRed:238/255.0 green:232/255.0 blue:170/255.0 alpha:1.0],
                       [UIColor colorWithRed:189/255.0 green:183/255.0 blue:107/255.0 alpha:1.0],
                       [UIColor colorWithRed:240/255.0 green:230/255.0 blue:140/255.0 alpha:1.0],
                       [UIColor colorWithRed:128/255.0 green:128/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:154/255.0 green:205/255.0 blue:50/255.0 alpha:1.0],
                       [UIColor colorWithRed:85/255.0 green:107/255.0 blue:47/255.0 alpha:1.0],
                       [UIColor colorWithRed:107/255.0 green:142/255.0 blue:35/255.0 alpha:1.0],
                       [UIColor colorWithRed:124/255.0 green:252/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:127/255.0 green:255/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:173/255.0 green:255/255.0 blue:47/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:100/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:128/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:34/255.0 green:139/255.0 blue:34/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:255/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:50/255.0 green:205/255.0 blue:50/255.0 alpha:1.0],
                       [UIColor colorWithRed:144/255.0 green:238/255.0 blue:144/255.0 alpha:1.0],
                       [UIColor colorWithRed:152/255.0 green:251/255.0 blue:151/255.0 alpha:1.0],
                       [UIColor colorWithRed:143/255.0 green:188/255.0 blue:143/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:250/255.0 blue:154/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:255/255.0 blue:127/255.0 alpha:1.0],
                       [UIColor colorWithRed:46/255.0 green:139/255.0 blue:87/255.0 alpha:1.0],
                       [UIColor colorWithRed:102/255.0 green:205/255.0 blue:170/255.0 alpha:1.0],
                       [UIColor colorWithRed:60/255.0 green:179/255.0 blue:113/255.0 alpha:1.0],
                       [UIColor colorWithRed:32/255.0 green:178/255.0 blue:170/255.0 alpha:1.0],
                       [UIColor colorWithRed:47/255.0 green:79/255.0 blue:79/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:128/255.0 blue:128/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:139/255.0 blue:139/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:255/255.0 blue:255/255.0 alpha:1.0],
                       [UIColor colorWithRed:224/255.0 green:255/255.0 blue:255/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:206/255.0 blue:209/255.0 alpha:1.0],
                       [UIColor colorWithRed:64/255.0 green:224/255.0 blue:208/255.0 alpha:1.0],
                       [UIColor colorWithRed:72/255.0 green:209/255.0 blue:204/255.0 alpha:1.0],
                       [UIColor colorWithRed:175/255.0 green:238/255.0 blue:238/255.0 alpha:1.0],
                       [UIColor colorWithRed:127/255.0 green:255/255.0 blue:212/255.0 alpha:1.0],
                       [UIColor colorWithRed:176/255.0 green:224/255.0 blue:230/255.0 alpha:1.0],
                       [UIColor colorWithRed:95/255.0 green:158/255.0 blue:160/255.0 alpha:1.0],
                       [UIColor colorWithRed:70/255.0 green:130/255.0 blue:180/255.0 alpha:1.0],
                       [UIColor colorWithRed:100/255.0 green:149/255.0 blue:237/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:191/255.0 blue:255/255.0 alpha:1.0],
                       [UIColor colorWithRed:30/255.0 green:144/255.0 blue:255/255.0 alpha:1.0],
                       [UIColor colorWithRed:173/255.0 green:216/255.0 blue:230/255.0 alpha:1.0],
                       [UIColor colorWithRed:135/255.0 green:206/255.0 blue:235/255.0 alpha:1.0],
                       [UIColor colorWithRed:135/255.0 green:206/255.0 blue:250/255.0 alpha:1.0],
                       [UIColor colorWithRed:25/255.0 green:25/255.0 blue:112/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:0/255.0 blue:128/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:0/255.0 blue:139/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:0/255.0 blue:205/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:0/255.0 blue:255/255.0 alpha:1.0],
                       [UIColor colorWithRed:65/255.0 green:105/255.0 blue:225/255.0 alpha:1.0],
                       [UIColor colorWithRed:138/255.0 green:43/255.0 blue:226/255.0 alpha:1.0],
                       [UIColor colorWithRed:75/255.0 green:0/255.0 blue:130/255.0 alpha:1.0],
                       [UIColor colorWithRed:72/255.0 green:61/255.0 blue:139/255.0 alpha:1.0],
                       [UIColor colorWithRed:106/255.0 green:90/255.0 blue:205/255.0 alpha:1.0],
                       [UIColor colorWithRed:123/255.0 green:104/255.0 blue:238/255.0 alpha:1.0],
                       [UIColor colorWithRed:147/255.0 green:112/255.0 blue:219/255.0 alpha:1.0],
                       [UIColor colorWithRed:139/255.0 green:0/255.0 blue:139/255.0 alpha:1.0],
                       [UIColor colorWithRed:148/255.0 green:0/255.0 blue:211/255.0 alpha:1.0],
                       [UIColor colorWithRed:153/255.0 green:50/255.0 blue:204/255.0 alpha:1.0],
                       [UIColor colorWithRed:186/255.0 green:85/255.0 blue:211/255.0 alpha:1.0],
                       [UIColor colorWithRed:128/255.0 green:0/255.0 blue:128/255.0 alpha:1.0],
                       [UIColor colorWithRed:216/255.0 green:191/255.0 blue:216/255.0 alpha:1.0],
                       [UIColor colorWithRed:221/255.0 green:160/255.0 blue:221/255.0 alpha:1.0],
                       [UIColor colorWithRed:238/255.0 green:130/255.0 blue:238/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:0/255.0 blue:255/255.0 alpha:1.0],
                       [UIColor colorWithRed:218/255.0 green:112/255.0 blue:214/255.0 alpha:1.0],
                       [UIColor colorWithRed:199/255.0 green:21/255.0 blue:133/255.0 alpha:1.0],
                       [UIColor colorWithRed:219/255.0 green:112/255.0 blue:147/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:20/255.0 blue:147/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:182/255.0 blue:193/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:192/255.0 blue:203/255.0 alpha:1.0],
                       [UIColor colorWithRed:250/255.0 green:235/255.0 blue:215/255.0 alpha:1.0],
                       [UIColor colorWithRed:245/255.0 green:245/255.0 blue:220/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:228/255.0 blue:196/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:235/255.0 blue:205/255.0 alpha:1.0],
                       [UIColor colorWithRed:245/255.0 green:222/255.0 blue:179/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:248/255.0 blue:220/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:250/255.0 blue:205/255.0 alpha:1.0],
                       [UIColor colorWithRed:250/255.0 green:250/255.0 blue:210/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:255/255.0 blue:224/255.0 alpha:1.0],
                       [UIColor colorWithRed:139/255.0 green:69/255.0 blue:19/255.0 alpha:1.0],
                       [UIColor colorWithRed:160/255.0 green:82/255.0 blue:45/255.0 alpha:1.0],
                       [UIColor colorWithRed:210/255.0 green:105/255.0 blue:30/255.0 alpha:1.0],
                       [UIColor colorWithRed:205/255.0 green:133/255.0 blue:63/255.0 alpha:1.0],
                       [UIColor colorWithRed:244/255.0 green:164/255.0 blue:96/255.0 alpha:1.0],
                       [UIColor colorWithRed:222/255.0 green:184/255.0 blue:135/255.0 alpha:1.0],
                       [UIColor colorWithRed:210/255.0 green:180/255.0 blue:140/255.0 alpha:1.0],
                       [UIColor colorWithRed:188/255.0 green:143/255.0 blue:143/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:228/255.0 blue:181/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:222/255.0 blue:173/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:218/255.0 blue:185/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:228/255.0 blue:225/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:240/255.0 blue:245/255.0 alpha:1.0],
                       [UIColor colorWithRed:250/255.0 green:240/255.0 blue:230/255.0 alpha:1.0],
                       [UIColor colorWithRed:253/255.0 green:245/255.0 blue:230/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:239/255.0 blue:213/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:245/255.0 blue:238/255.0 alpha:1.0],
                       [UIColor colorWithRed:245/255.0 green:255/255.0 blue:250/255.0 alpha:1.0],
                       [UIColor colorWithRed:112/255.0 green:255/255.0 blue:250/255.0 alpha:1.0],
                       [UIColor colorWithRed:112/255.0 green:128/255.0 blue:144/255.0 alpha:1.0],
                       [UIColor colorWithRed:119/255.0 green:136/255.0 blue:153/255.0 alpha:1.0],
                       [UIColor colorWithRed:176/255.0 green:196/255.0 blue:222/255.0 alpha:1.0],
                       [UIColor colorWithRed:230/255.0 green:230/255.0 blue:250/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:250/255.0 blue:240/255.0 alpha:1.0],
                       [UIColor colorWithRed:240/255.0 green:248/255.0 blue:255/255.0 alpha:1.0],
                       [UIColor colorWithRed:248/255.0 green:255/255.0 blue:240/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:255/255.0 blue:240/255.0 alpha:1.0],
                       [UIColor colorWithRed:240/255.0 green:255/255.0 blue:255/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:250/255.0 blue:250/255.0 alpha:1.0],
                       [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0],
                       [UIColor colorWithRed:105/255.0 green:105/255.0 blue:105/255.0 alpha:1.0],
                       [UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0],
                       [UIColor colorWithRed:169/255.0 green:169/255.0 blue:169/255.0 alpha:1.0],
                       [UIColor colorWithRed:192/255.0 green:192/255.0 blue:192/255.0 alpha:1.0],
                       [UIColor colorWithRed:211/255.0 green:211/255.0 blue:211/255.0 alpha:1.0],
                       [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0],
                       [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1.0],
                       [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0],
                      nil];
    return array1[tag];
}

@end
