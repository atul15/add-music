//  VideoEditing.m
//  VideoMaker
//  Created by Sohil on 4/23/17.
//  Copyright © 2017 iAppz Studio. All rights reserved.


#import "VideoEditing.h"
#import "AppDelegate.h"
@interface VideoEditing ()

@end

@implementation VideoEditing



- (instancetype)initWithVideoUrl:(NSURL *)url {
    self = [super init];
    if(self) {
        _videoUrl = url;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"VideoEditing viewDidLoad");

    
    timer1 = [NSTimer scheduledTimerWithTimeInterval:5.0
                                     target:self
                                   selector:@selector(insertialAds)
                                   userInfo:nil
                                    repeats:NO];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    stickerArray = [[NSMutableArray alloc]init];
    for (int i=1; i<=32; i++) {
        [stickerArray addObject:[NSString stringWithFormat:@"E%d.png",i]];
    }
    
    borderArray = [[NSMutableArray alloc]init];
    [borderArray addObject:@"nonBorder.png"];

    
    for (int i=1; i<=21; i++) {
        [borderArray addObject:[NSString stringWithFormat:@"Border%d.png",i]];
    }
    
    APPDELEGATE_SHARE.drawLineColor = @"1";
    self.drawingView.userInteractionEnabled = NO;
    
    selectedType = @"TextColor";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];

    stickerTag =0;

    [self playVideo];
    [self setFrame];
}

- (void)setFrame {
    
    [self editorViewFrameSet];
    
    //Button Up And Down
    isClose = YES;
    
    imgUpDown = [[UIImageView alloc]init];
    imgUpDown.frame = CGRectMake((ScreenWidth/2)-25, ScreenHeight-65-28, 50, 30);
    imgUpDown.image = [UIImage imageNamed:@"downArrow.png"];
    [self.view addSubview:imgUpDown];
    
    self.btnUpDown = [[UIButton alloc]init];
    self.btnUpDown.frame = CGRectMake((ScreenWidth/2)-25, ScreenHeight-65-25, 50, 50);
    self.btnUpDown.layer.cornerRadius = self.btnUpDown.frame.size.width/2;
    self.btnUpDown.clipsToBounds = YES;
    [self.btnUpDown addTarget:self action:@selector(upDownClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.btnUpDown];
    
    //Bottom Main View For Selection
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:0.40];
    self.bottomView.frame = CGRectMake(0, ScreenHeight-65, ScreenWidth, 65);
    [self.view addSubview:self.bottomView];
    
    //Scrollview Add For Buttons in Mainview
    self.bottomScrollView = [[UIScrollView alloc]init];
    self.bottomScrollView.frame = CGRectMake(0, 0, ScreenWidth, self.bottomView.frame.size.height);
    [self.bottomView addSubview:self.bottomScrollView];
    
    lblLine = [[UILabel alloc]init];
    lblLine.frame = CGRectMake(0, self.bottomScrollView.frame.size.height-3, ScreenWidth/4, 2);
    lblLine.backgroundColor = [UIColor yellowColor];
    [self.bottomScrollView addSubview:lblLine];
    
    // Button sticker For Add New Sticker in editor view
    UIImageView *imgSticker = [[UIImageView alloc]init];
    imgSticker.frame = CGRectMake(((ScreenWidth/4)-35)/2, 5, 35, 35 );
    imgSticker.image = [UIImage imageNamed:@"sticker"];
    [self.bottomScrollView addSubview:imgSticker];
    
    UILabel *lblSticker = [[UILabel alloc]init];
    lblSticker.frame = CGRectMake(0, imgSticker.frame.origin.y+ imgSticker.frame.size.height+6, ScreenWidth/4, 15);
    lblSticker.text = @"STICKER";
    lblSticker.font = [UIFont fontWithName:ORATORSTD_FONT size:12.0];
    lblSticker.textColor = [UIColor whiteColor];
    lblSticker.textAlignment = NSTextAlignmentCenter;
    [self.bottomScrollView addSubview:lblSticker];
    
    UIButton *btnSticker = [[UIButton alloc]init];
    btnSticker.frame = CGRectMake(0,0, ScreenWidth/4, self.bottomScrollView.frame.size.height);
    [btnSticker addTarget:self action:@selector(stickerClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomScrollView addSubview:btnSticker];
    
    [self imageStickerViewAdd];
    imageStickerView.hidden = YES;
    
    // Button Text For Add Text Sticker in editor view
    UIImageView *imgText = [[UIImageView alloc]init];
    imgText.frame = CGRectMake((((ScreenWidth/4)-35)/2)+ScreenWidth/4, 5, 35, 35 );
    imgText.image = [UIImage imageNamed:@"text"];
    [self.bottomScrollView addSubview:imgText];
    
    UILabel *lblText = [[UILabel alloc]init];
    lblText.frame = CGRectMake(lblSticker.frame.origin.x + lblSticker.frame.size.width, imgSticker.frame.origin.y+ imgSticker.frame.size.height+6, ScreenWidth/4, 15);
    lblText.text = @"TEXT";
    lblText.font = [UIFont fontWithName:ORATORSTD_FONT size:12.0];
    lblText.textColor = [UIColor whiteColor];
    lblText.textAlignment = NSTextAlignmentCenter;
    [self.bottomScrollView addSubview:lblText];
    
    UIButton *btnText = [[UIButton alloc]init];
    btnText.frame = CGRectMake((ScreenWidth/4), 0, (ScreenWidth/4), self.bottomScrollView.frame.size.height);
    [btnText addTarget:self action:@selector(textClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomScrollView addSubview:btnText];
    
    [self textStickerViewAdd];
    textStickerView.hidden=YES;
    
    
    //Button Border
    UIImageView *imgBorder = [[UIImageView alloc]init];
    imgBorder.frame = CGRectMake((((ScreenWidth/4)-35)/2)+((ScreenWidth/4)*2), 5, 35, 35 );
    imgBorder.image = [UIImage imageNamed:@"border"];
    [self.bottomScrollView addSubview:imgBorder];
    
    UILabel *lblBorder = [[UILabel alloc]init];
    lblBorder.frame = CGRectMake(lblText.frame.origin.x + lblText.frame.size.width, imgBorder.frame.origin.y+ imgBorder.frame.size.height+6, ScreenWidth/4, 15);
    lblBorder.text = @"FRAME";
    lblBorder.font = [UIFont fontWithName:ORATORSTD_FONT size:12.0];
    lblBorder.textColor = [UIColor whiteColor];
    lblBorder.textAlignment = NSTextAlignmentCenter;
    [self.bottomScrollView addSubview:lblBorder];
    
    UIButton *btnBorder = [[UIButton alloc]init];
    btnBorder.frame = CGRectMake(((ScreenWidth/4)*2), 0, ScreenWidth/4, self.bottomScrollView.frame.size.height);
    [btnBorder addTarget:self action:@selector(borderClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomScrollView addSubview:btnBorder];
    
    [self borderViewAdd];
    borderStickerView.hidden = YES;
    
    
    //Button Line
    UIImageView *imgBrush = [[UIImageView alloc]init];
    imgBrush.frame = CGRectMake((((ScreenWidth/4)-35)/2)+((ScreenWidth/4)*3), 5, 35, 35 );
    imgBrush.image = [UIImage imageNamed:@"brush"];
    [self.bottomScrollView addSubview:imgBrush];
    
    UILabel *lblBrush = [[UILabel alloc]init];
    lblBrush.frame = CGRectMake(lblBorder.frame.origin.x + lblBorder.frame.size.width, imgBrush.frame.origin.y+ imgBrush.frame.size.height+6, ScreenWidth/4, 15);
    lblBrush.text = @"BRUSH";
    lblBrush.font = [UIFont fontWithName:ORATORSTD_FONT size:12.0];
    lblBrush.textColor = [UIColor whiteColor];
    lblBrush.textAlignment = NSTextAlignmentCenter;
    [self.bottomScrollView addSubview:lblBrush];
    
    UIButton *btnLines = [[UIButton alloc]init];
    btnLines.frame = CGRectMake(((ScreenWidth/4)*3), 0, ScreenWidth/4, self.bottomScrollView.frame.size.height);
    [btnLines addTarget:self action:@selector(lineClick) forControlEvents:UIControlEventTouchUpInside];
    [self.bottomScrollView addSubview:btnLines];
    
    [self lineViewAdd];
    lineView.hidden = YES;
    
    // Top View
    UIView *topView = [[UIView alloc]init];
    topView.frame = CGRectMake(0, 0, ScreenWidth, 44);
    topView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [self.view addSubview:topView];
    
    // cancel button
    UIButton *cancelButton = [[UIButton alloc] init];
    cancelButton.frame = CGRectMake(10, 7, 30, 30);
    [cancelButton setImage:[UIImage imageNamed:@"cancel_icon.png"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:cancelButton];
    
    //Done For Make Video
    UIButton *btnDone = [[UIButton alloc]init];
    btnDone.frame = CGRectMake(ScreenWidth-40, 7, 30, 30);
    [btnDone setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneClick:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:btnDone];
    
    
    UIButton *btnSkip = [[UIButton alloc]init];
    btnSkip.frame = CGRectMake(ScreenWidth-85, 8, 30, 27);
    [btnSkip setImage:[UIImage imageNamed:@"skip_icon.png"] forState:UIControlStateNormal];
    [btnSkip addTarget:self action:@selector(skipClick:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:btnSkip];
    
    itemCollectionView.frame = CGRectMake(0, ScreenHeight-216, ScreenWidth, 216);
    [self.view addSubview:itemCollectionView];
    itemCollectionView.hidden = YES;
    
    fontPickerView.frame = CGRectMake(0, ScreenHeight-216, ScreenWidth, 216);
    [self.view addSubview:fontPickerView];
    fontPickerView.hidden = YES;
}


- (UIInterfaceOrientation)orientationForTrack:(AVAsset *)asset {
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    
    if (size.width == txf.tx && size.height == txf.ty)
        return UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == 0)
        return UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == size.width)
        return UIInterfaceOrientationPortraitUpsideDown;
    else
        return UIInterfaceOrientationPortrait;
}

- (void)editorViewFrameSet {
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:self.videoUrl options:nil];
    NSArray *tracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    AVAssetTrack *track = [tracks objectAtIndex:0];
    CGSize mediaSize = track.naturalSize;

    
    self.editorView = [[UIView alloc]init];
    
    UIInterfaceOrientation orintation = [self orientationForTrack:asset];
    
    if (orintation == UIInterfaceOrientationLandscapeRight) {
        float videoWidth  = mediaSize.width;
        float videoHeight = mediaSize.height;
        float calVideoHeight =  (videoHeight * ScreenWidth)/videoWidth;
        
        self.editorView.frame = CGRectMake(0, (ScreenHeight/2)-(calVideoHeight/2), ScreenWidth, calVideoHeight);
        self.drawingView.frame = CGRectMake(0, 0, ScreenWidth, calVideoHeight);
    }
    else if (orintation == UIInterfaceOrientationLandscapeLeft) {
        float videoWidth  = mediaSize.width;
        float videoHeight = mediaSize.height;
        float calVideoHeight =  (videoHeight * ScreenWidth)/videoWidth;
        self.editorView.frame = CGRectMake(0, (ScreenHeight/2)-(calVideoHeight/2), ScreenWidth, calVideoHeight);
        self.drawingView.frame = CGRectMake(0, 0, ScreenWidth, calVideoHeight);

    }
    else if (orintation == UIInterfaceOrientationPortraitUpsideDown) {
        self.editorView.frame=CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        self.drawingView.frame = CGRectMake(0, 0,ScreenWidth, ScreenHeight);
    }
    else if (orintation == UIInterfaceOrientationPortrait) {
        self.editorView.frame=CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        self.drawingView.frame = CGRectMake(0, 0,ScreenWidth, ScreenHeight);
    }
    
    self.drawingView.layer.borderWidth = 1.0;
    self.editorView.layer.borderWidth = 1.0;


    self.editorView.clipsToBounds = YES;
    self.drawingView.delegate = self;
    self.drawingView.draggableTextFontName = @"MarkerFelt-Thin";
    [self.view addSubview:self.editorView];
    [self.editorView addSubview:self.drawingView];
}



#pragma mark - Bottom Button Click

- (void)upDownClick {
    isClose = !isClose;
    
    if (isClose) {
        
        [UIView animateWithDuration:0.5f animations:^
         {
             imgUpDown.image = [UIImage imageNamed:@"downArrow.png"];

             imgUpDown.frame = CGRectMake((ScreenWidth/2)-25, ScreenHeight-65-28, 50, 30);
             self.btnUpDown.frame = CGRectMake((ScreenWidth/2)-25, ScreenHeight-65-25, 50, 50);
             self.bottomView.frame = CGRectMake(0, ScreenHeight-65, ScreenWidth, 65);
         }
         completion:^(BOOL finished)
         {
         }];

    }
    else
    {
        imgUpDown.image = [UIImage imageNamed:@"upArrow.png"];


         [UIView animateWithDuration:0.5f animations:^
         {
             imgUpDown.frame = CGRectMake((ScreenWidth/2)-25, ScreenHeight-28, 50, 30);
             self.btnUpDown.frame = CGRectMake((ScreenWidth/2)-25, ScreenHeight-25, 50, 50);
             self.bottomView.frame = CGRectMake(0, ScreenHeight, ScreenWidth, 65);
         }
         completion:^(BOOL finished)
         {
         }];
    }
}



#pragma mark - Sticker

- (void)imageStickerViewAdd {
    
    imageStickerView = [[UIView alloc]init];
    imageStickerView.frame = CGRectMake(0, ScreenHeight-216-50, ScreenWidth, 50);
    imageStickerView.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0];
    [self.view addSubview:imageStickerView];
    
    
    
    //Close Button
    UIImageView *imgClose = [[UIImageView alloc]init];
    imgClose.frame = CGRectMake(((ScreenWidth/5)-30)/2, 10, 30, 30);
    imgClose.image = [UIImage imageNamed:@"close"];
    [imageStickerView addSubview:imgClose];
    
    UIButton *closeButton = [[UIButton alloc]init];
    closeButton.frame = CGRectMake(0, 0, ScreenWidth/5, imageStickerView.frame.size.height);
    [closeButton addTarget:self action:@selector(closeStickerClick) forControlEvents:UIControlEventTouchUpInside];
    [imageStickerView addSubview:closeButton];

    
    UIScrollView *backScroll = [[UIScrollView alloc]init];
    backScroll.frame=CGRectMake(closeButton.frame.size.width, 0, ScreenWidth-closeButton.frame.size.width, 50);
    [imageStickerView addSubview:backScroll];
    
    lblLineSticker = [[UILabel alloc]init];
    lblLineSticker.frame = CGRectMake(0, 50-3, ScreenWidth/5, 2);
    lblLineSticker.backgroundColor = [UIColor yellowColor];
    [backScroll addSubview:lblLineSticker];

    
    //Color Image Button
    btnEmotional = [[UIButton alloc]init];
    btnEmotional.frame = CGRectMake(0, 0, ScreenWidth/5, backScroll.frame.size.height);
    [btnEmotional setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnEmotional addTarget:self action:@selector(emotionClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgEmotional = [[UIImageView alloc]init];
    imgEmotional.frame = CGRectMake((btnEmotional.frame.size.width/2)-15, 10, 30, 30);
    imgEmotional.image = [UIImage imageNamed:@"Emoji"];
    [backScroll addSubview:imgEmotional];
    [backScroll addSubview:btnEmotional];
    
    
    //Line Image Button
    btnBirthday = [[UIButton alloc]init];
    btnBirthday.frame = CGRectMake(btnEmotional.frame.origin.x + btnEmotional.frame.size.width, 0, ScreenWidth/5, backScroll.frame.size.height);
    [btnBirthday setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnBirthday addTarget:self action:@selector(birhdayClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgLine = [[UIImageView alloc]init];
    imgLine.frame = CGRectMake(btnBirthday.frame.origin.x +(btnBirthday.frame.size.width/2)-15, 10, 30, 30);
    imgLine.image = [UIImage imageNamed:@"Birthday"];
    [backScroll addSubview:imgLine];
    [backScroll addSubview:btnBirthday];

    /*
    //Line Image Button
    btnGoldenBirthday = [[UIButton alloc]init];
    btnGoldenBirthday.frame = CGRectMake(btnBirthday.frame.origin.x + btnBirthday.frame.size.width, 0, ScreenWidth/5, backScroll.frame.size.height);
    [btnGoldenBirthday setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnGoldenBirthday addTarget:self action:@selector(birhdayGoldenClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgGolden = [[UIImageView alloc]init];
    imgGolden.frame = CGRectMake(btnGoldenBirthday.frame.origin.x +(btnGoldenBirthday.frame.size.width/2)-15, 10, 30, 30);
    imgGolden.image = [UIImage imageNamed:@"GoldBirthday"];
    [backScroll addSubview:imgGolden];
    [backScroll addSubview:btnGoldenBirthday];
*/
    //Line Image Button
    btnBoom = [[UIButton alloc]init];
    btnBoom.frame = CGRectMake(btnBirthday.frame.origin.x + btnBirthday.frame.size.width, 0, ScreenWidth/5, backScroll.frame.size.height);

    [btnBoom setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnBoom addTarget:self action:@selector(boomClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgBoom = [[UIImageView alloc]init];
    imgBoom.frame = CGRectMake(btnBoom.frame.origin.x +(btnBoom.frame.size.width/2)-15, 10, 30, 30);
    imgBoom.image = [UIImage imageNamed:@"boom"];
    [backScroll addSubview:imgBoom];
    [backScroll addSubview:btnBoom];

    
    //Line Image Button
    btnLove = [[UIButton alloc]init];
    btnLove.frame = CGRectMake(btnBoom.frame.origin.x + btnBoom.frame.size.width, 0, ScreenWidth/5, backScroll.frame.size.height);
    [btnLove setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnLove addTarget:self action:@selector(loveClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgLove = [[UIImageView alloc]init];
    imgLove.frame = CGRectMake(btnLove.frame.origin.x +(btnLove.frame.size.width/2)-15, 10, 30, 30);
    imgLove.image = [UIImage imageNamed:@"Romantic"];
    [backScroll addSubview:imgLove];
    [backScroll addSubview:btnLove];

    //Line Image Button
   /* btnFace = [[UIButton alloc]init];
    btnFace.frame = CGRectMake(btnLove.frame.origin.x + btnLove.frame.size.width, 0, ScreenWidth/5, backScroll.frame.size.height);
    [btnFace setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnFace addTarget:self action:@selector(faceClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgFace = [[UIImageView alloc]init];
    imgFace.frame = CGRectMake(btnFace.frame.origin.x +(btnFace.frame.size.width/2)-15, 10, 30, 30);
    imgFace.image = [UIImage imageNamed:@"Face1.png"];
    [backScroll addSubview:imgFace];
    [backScroll addSubview:btnFace];
*/
    
    btnHLove = [[UIButton alloc]init];
    btnHLove.frame = CGRectMake(btnLove.frame.origin.x + btnLove.frame.size.width, 0, ScreenWidth/5, backScroll.frame.size.height);
    [btnHLove setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnHLove addTarget:self action:@selector(loveHClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgHLove = [[UIImageView alloc]init];
    imgHLove.frame = CGRectMake(btnHLove.frame.origin.x +(btnHLove.frame.size.width/2)-15, 10, 30, 30);
    imgHLove.image = [UIImage imageNamed:@"Love"];
    [backScroll addSubview:imgHLove];
    [backScroll addSubview:btnHLove];
    

    backScroll.contentSize = CGSizeMake(((ScreenWidth/5)*5), 50);
}

- (void)stickerClick {

     [UIView animateWithDuration:0.5f animations:^
     {
         lblLine.frame = CGRectMake(0, self.bottomScrollView.frame.size.height-3, ScreenWidth/4, 2);
     }
     completion:^(BOOL finished)
     {
     }];
    
    for (UIView *j in self.editorView.subviews){
        if([j isKindOfClass:[ZDStickerView class]]){
            ZDStickerView *selectSticker = (ZDStickerView *)j;
            [selectSticker hideCustomHandle];
            [selectSticker hideDelHandle];
            [selectSticker hideEditingHandles];
        }
    }

    selectedType = @"image";
    itemCollectionView.hidden = NO;
    imageStickerView.hidden = NO;
    self.drawingView.userInteractionEnabled = NO;
    [itemCollectionView reloadData];
}

- (void)closeStickerClick {
    selectedType=@"image";
    stickerArray = [[NSMutableArray alloc]init];
    for (int i=1; i<=32; i++) {
        [stickerArray addObject:[NSString stringWithFormat:@"E%d.png",i]];
    }
    lblLineSticker.frame = CGRectMake(btnEmotional.frame.origin.x, 50-3, ScreenWidth/5, 2);

    itemCollectionView.hidden = YES;
    imageStickerView.hidden = YES;
}

- (void)emotionClick {
    
     selectedType=@"image";
    
     [UIView animateWithDuration:0.5f animations:^
     {
         lblLineSticker.frame = CGRectMake(btnEmotional.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
     completion:^(BOOL finished)
     {
     }];
    
    stickerArray = [[NSMutableArray alloc]init];
    for (int i=1; i<=32; i++) {
        [stickerArray addObject:[NSString stringWithFormat:@"E%d.png",i]];
    }
    [itemCollectionView reloadData];
}

- (void)birhdayClick {
    
    selectedType=@"birthday";
    
     [UIView animateWithDuration:0.5f animations:^
     {
         lblLineSticker.frame = CGRectMake(btnBirthday.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
     completion:^(BOOL finished)
     {
     }];
    
    stickerArray = [[NSMutableArray alloc]init];
    for (int i=1; i<=13; i++) {
        [stickerArray addObject:[NSString stringWithFormat:@"H%d.png",i]];
    }
    [itemCollectionView reloadData];
}

- (void)birhdayGoldenClick {
    
    selectedType = @"GoldenBirthday";
    
    [UIView animateWithDuration:0.5f animations:^
     {
         lblLineSticker.frame = CGRectMake(btnGoldenBirthday.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
     completion:^(BOOL finished)
     {
     }];
    
    stickerArray = [[NSMutableArray alloc]init];
    for (int i=1; i<=10; i++) {
        [stickerArray addObject:[NSString stringWithFormat:@"HG%d.png",i]];
    }
    [itemCollectionView reloadData];

}

- (void)boomClick {
    
    selectedType=@"BOOM";
    
    [UIView animateWithDuration:0.5f animations:^
     {
         lblLineSticker.frame = CGRectMake(btnBoom.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
     completion:^(BOOL finished)
     {
     }];
    
    stickerArray = [[NSMutableArray alloc]init];
    for (int i=1; i<=12; i++) {
        [stickerArray addObject:[NSString stringWithFormat:@"BM%d.png",i]];
    }
    [itemCollectionView reloadData];
}

- (void)loveClick {
    selectedType=@"image";
    
    [UIView animateWithDuration:0.5f animations:^
     {
         lblLineSticker.frame = CGRectMake(btnLove.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
                     completion:^(BOOL finished)
     {
     }];
    
    stickerArray = [[NSMutableArray alloc]init];
    for (int i=1; i<=20; i++) {
        [stickerArray addObject:[NSString stringWithFormat:@"Love%d.png",i]];
    }
    [itemCollectionView reloadData];
}

- (void)faceClick {
    selectedType=@"image";
    
    [UIView animateWithDuration:0.5f animations:^
     {
         lblLineSticker.frame = CGRectMake(btnFace.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
                     completion:^(BOOL finished)
     {
     }];
    
    stickerArray = [[NSMutableArray alloc]init];
    for (int i=1; i<=16; i++) {
        [stickerArray addObject:[NSString stringWithFormat:@"Face%d.png",i]];
    }
    [itemCollectionView reloadData];
}

- (void)loveHClick{
    selectedType=@"LoveH";
    
     [UIView animateWithDuration:0.5f animations:^
     {
         lblLineSticker.frame = CGRectMake(btnHLove.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
                     completion:^(BOOL finished)
     {
     }];
    
    stickerArray = [[NSMutableArray alloc]init];
    for (int i=1; i<=10; i++) {
        [stickerArray addObject:[NSString stringWithFormat:@"LoveH%d.png",i]];
    }
    [itemCollectionView reloadData];
}

- (void)selectSticker:(int)sender {

    for (UIView *j in self.editorView.subviews){
        if([j isKindOfClass:[ZDStickerView class]]){
            ZDStickerView *selectSticker = (ZDStickerView *)j;
            [selectSticker hideCustomHandle];
            [selectSticker hideDelHandle];
            [selectSticker hideEditingHandles];
        }
    }

    
    CGRect gripFrame1 = CGRectMake(50, 50, 140, 140);

    if ([selectedType isEqualToString:@"LoveH"])
    {
        gripFrame1 = CGRectMake(0, 50, ScreenWidth, 150);
    }
    else if ([selectedType isEqualToString:@"BOOM"])
    {
        if (sender<6) {
            gripFrame1 = CGRectMake(50, 50, 120, 120);
        }
        else
        {
            gripFrame1 = CGRectMake(50, 50, 200, 130);
        }
        
    }
    else if ([selectedType isEqualToString:@"GoldenBirthday"])
    {
        if (sender > 3)
        {
            gripFrame1 = CGRectMake(50, 50, 100, 100);
        }
        else
        {
            gripFrame1 = CGRectMake(50, 50, 100, 140);
        }
        
    }
    
    UIImageView *stickerImageView = [[UIImageView alloc]
                                     initWithImage:[UIImage imageNamed:[stickerArray objectAtIndex:sender]]];

    UIView* contentView = [[UIView alloc] initWithFrame:gripFrame1];
    [contentView addSubview:stickerImageView];

    ZDStickerView *userResizableView1 = [[ZDStickerView alloc] initWithFrame:gripFrame1];
    userResizableView1.tag = stickerTag;
    stickerTag++;
    userResizableView1.stickerViewDelegate = self;
    userResizableView1.contentView = contentView;
    userResizableView1.preventsPositionOutsideSuperview = NO;
    userResizableView1.translucencySticker = NO;
    [userResizableView1 showEditingHandles];
    [self.editorView addSubview:userResizableView1];
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
    tapGesture1.numberOfTapsRequired = 1;
    [tapGesture1 setDelegate:self];
    [userResizableView1 addGestureRecognizer:tapGesture1];

}

- (void)tapGesture:(UITapGestureRecognizer*)sender {
    UIView *view = sender.view;
    
    for (UIView *j in self.editorView.subviews){
        if([j isKindOfClass:[ZDStickerView class]]){
            ZDStickerView *selectSticker = (ZDStickerView *)j;
            
            if(selectSticker.tag == view.tag)
            {
                [selectSticker showCustomHandle];
                [selectSticker showDelHandle];
                [selectSticker showEditingHandles];
            }
            else
            {
                [selectSticker hideCustomHandle];
                [selectSticker hideDelHandle];
                [selectSticker hideEditingHandles];
            }
            
        }
    }
    
    
}






#pragma mark - Text

- (void)keyboardDidShow:(NSNotification*)notification {
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    textStickerView.frame = CGRectMake(0, ScreenHeight-keyboardFrameBeginRect.size.height-50, ScreenWidth, 50);
    
    isHide = YES;
}

- (void)keyboardDidHide:(NSNotification*)notification {
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    textStickerView.frame = CGRectMake(0, ScreenHeight-keyboardFrameBeginRect.size.height-50, ScreenWidth, 50);
    
    isHide = NO;
}

- (void)textStickerViewAdd {
    textStickerView = [[UIView alloc]init];
    textStickerView.frame = CGRectMake(0, ScreenHeight-216-50, ScreenWidth, 50);
    textStickerView.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0];
    [self.view addSubview:textStickerView];
    
    lblLineText = [[UILabel alloc]init];
    lblLineText.frame = CGRectMake((ScreenWidth/5)*2, textStickerView.frame.size.height-3, ScreenWidth/5, 2);

    lblLineText.backgroundColor = [UIColor yellowColor];
    [textStickerView addSubview:lblLineText];
    
    //Close Button
    UIImageView *imgClose = [[UIImageView alloc]init];
    imgClose.frame = CGRectMake(((ScreenWidth/5)-30)/2, 10, 30, 30);
    imgClose.image = [UIImage imageNamed:@"close"];
    [textStickerView addSubview:imgClose];

    UIButton *closeButton = [[UIButton alloc]init];
    closeButton.frame = CGRectMake(0, 0, ScreenWidth/5, textStickerView.frame.size.height);
    [closeButton addTarget:self action:@selector(closeClick) forControlEvents:UIControlEventTouchUpInside];
    [textStickerView addSubview:closeButton];
    
    //New Sticker Button
    UIImageView *imgSticker = [[UIImageView alloc]init];
    imgSticker.frame = CGRectMake((((ScreenWidth/5)-30)/2)+ScreenWidth/5, 10, 30, 30);
    imgSticker.image = [UIImage imageNamed:@"newText"];
    [textStickerView addSubview:imgSticker];

    UIButton *addSticker = [[UIButton alloc]init];
    addSticker.frame = CGRectMake(ScreenWidth/5, 0, ScreenWidth/5, textStickerView.frame.size.height);
    [addSticker addTarget:self action:@selector(addSticker) forControlEvents:UIControlEventTouchUpInside];
    [textStickerView addSubview:addSticker];
    
    //Text Color
    UIImageView *imgColor = [[UIImageView alloc]init];
    imgColor.frame = CGRectMake((((ScreenWidth/5)-30)/2)+((ScreenWidth/5)*2), 10, 30, 30);
    imgColor.image = [UIImage imageNamed:@"fontColor"];
    [textStickerView addSubview:imgColor];

    UIButton *textColor = [[UIButton alloc]init];
    textColor.frame = CGRectMake((ScreenWidth/5)*2, 0, ScreenWidth/5, textStickerView.frame.size.height);
    [textColor addTarget:self action:@selector(changeTextColor) forControlEvents:UIControlEventTouchUpInside];
    [textStickerView addSubview:textColor];
    
    //Change Font
    UIImageView *imgFont = [[UIImageView alloc]init];
    imgFont.frame = CGRectMake((((ScreenWidth/5)-30)/2)+((ScreenWidth/5)*3), 10, 30, 30);
    imgFont.image = [UIImage imageNamed:@"font"];
    [textStickerView addSubview:imgFont];

    UIButton *changeFont = [[UIButton alloc]init];
    changeFont.frame = CGRectMake((ScreenWidth/5)*3, 0, ScreenWidth/5, textStickerView.frame.size.height);
    [changeFont addTarget:self action:@selector(changeFont) forControlEvents:UIControlEventTouchUpInside];
    [textStickerView addSubview:changeFont];
    
    //Font Size
    UIImageView *imgFontSize = [[UIImageView alloc]init];
    imgFontSize.frame = CGRectMake((((ScreenWidth/5)-30)/2)+((ScreenWidth/5)*4), 10, 30, 30);
    imgFontSize.image = [UIImage imageNamed:@"fontSize"];
    [textStickerView addSubview:imgFontSize];

    UIButton *fontSize = [[UIButton alloc]init];
    fontSize.frame = CGRectMake((ScreenWidth/5)*4, 0, ScreenWidth/5, textStickerView.frame.size.height);
    [fontSize addTarget:self action:@selector(fontSize) forControlEvents:UIControlEventTouchUpInside];
    [textStickerView addSubview:fontSize];

    self.fontAttributeView.frame = CGRectMake(0, ScreenHeight-216, ScreenWidth, 216);
    self.fontAttributeView.hidden = YES;
    [self.view addSubview:self.fontAttributeView];
}

- (void)changeFont {
    
     [UIView animateWithDuration:0.5f animations:^
     {
         lblLineText.frame = CGRectMake((ScreenWidth/5)*3, textStickerView.frame.size.height-3, ScreenWidth/5, 2);
     }
     completion:^(BOOL finished)
     {
     }];

    fontPickerView.hidden = NO;
    itemCollectionView.hidden = YES;
    self.fontAttributeView.hidden = YES;
    [self.view endEditing:YES];
}

- (void)changeTextColor {
    
    [UIView animateWithDuration:0.5f animations:^
     {
         lblLineText.frame = CGRectMake((ScreenWidth/5)*2, textStickerView.frame.size.height-3, ScreenWidth/5, 2);
     }
     completion:^(BOOL finished)
     {
     }];

    selectedType = @"TextColor";
    fontPickerView.hidden = YES;
    itemCollectionView.hidden = NO;
    self.fontAttributeView.hidden = YES;
    [self.view endEditing:YES];
    [itemCollectionView reloadData];
}

- (void)closeClick {
    
    isHide = NO;
    
     [UIView animateWithDuration:0.5f animations:^
     {
         lblLineText.frame = CGRectMake(ScreenWidth/5, textStickerView.frame.size.height-3, ScreenWidth/5, 2);
     }
     completion:^(BOOL finished)
     {
     }];

    fontPickerView.hidden = YES;
    itemCollectionView.hidden = YES;
    textStickerView.hidden = YES;
    borderStickerView.hidden = YES;
    self.fontAttributeView.hidden = YES;
    [self.view endEditing:YES];
}

- (void)textClick {
    
     isHide = YES;
    
     [UIView animateWithDuration:0.5f animations:^
     {
         
         lblLine.frame = CGRectMake(ScreenWidth/4, self.bottomScrollView.frame.size.height-3, ScreenWidth/4, 2);

     }
     completion:^(BOOL finished)
     {
     }];

    for (UIView *j in self.editorView.subviews){
        if([j isKindOfClass:[ZDStickerView class]]){
            ZDStickerView *selectSticker = (ZDStickerView *)j;
            [selectSticker hideCustomHandle];
            [selectSticker hideDelHandle];
            [selectSticker hideEditingHandles];
        }
    }

    selectedType = @"TextColor";
    fontPickerView.hidden = YES;
    itemCollectionView.hidden = NO;
    textStickerView.hidden = NO;
    self.fontAttributeView.hidden = YES;
    self.drawingView.userInteractionEnabled = NO;
    [itemCollectionView reloadData];
}

- (void)fontSize {
    
     [UIView animateWithDuration:0.5f animations:^
     {
         lblLineText.frame = CGRectMake((ScreenWidth/5)*4, textStickerView.frame.size.height-3, ScreenWidth/5, 2);
     }
     completion:^(BOOL finished)
     {
     }];
    
    self.fontAttributeView.hidden = NO;
    fontPickerView.hidden = YES;
    itemCollectionView.hidden = YES;
    textStickerView.hidden = NO;
    [self.view endEditing:YES];
}

- (void)addSticker {
    
     [UIView animateWithDuration:0.5f animations:^
     {
         lblLineText.frame = CGRectMake(ScreenWidth/5, textStickerView.frame.size.height-3, ScreenWidth/5, 2);
     }
     completion:^(BOOL finished)
     {
     }];

    
    for (UIView *j in self.editorView.subviews){
        if([j isKindOfClass:[ZDStickerView class]]){
            ZDStickerView *selectSticker = (ZDStickerView *)j;
            [selectSticker hideCustomHandle];
            [selectSticker hideDelHandle];
            [selectSticker hideEditingHandles];
        }
    }
    

    CGRect gripFrame2 = CGRectMake(self.editorView.frame.size.width/2-50, self.editorView.frame.size.height/2-40, 130, 70);
    textView = [[UITextView alloc] initWithFrame:gripFrame2];
    textView.tag=stickerTag;
    textView.delegate=self;
    textView.text = @"Add Text";
    selectFontItem = 28;
    textView.font = [UIFont fontWithName:[FontPack findFont:selectFontItem] size:20];
    textView.textAlignment = NSTextAlignmentCenter;
    [textView becomeFirstResponder];

    textView.autocorrectionType = UITextAutocorrectionTypeYes;
    textView.autocorrectionType = UITextAutocorrectionTypeNo;
    
    [textView setTextColor:[UIColor redColor]];
//    [textView setFont:[UIFont systemFontOfSize:16]];
    userResizableView = [[ZDStickerView alloc] initWithFrame:gripFrame2];
    userResizableView.tag = stickerTag;
    stickerTag++;
    
    [textView setBackgroundColor:[UIColor clearColor]];
    userResizableView.stickerViewDelegate = self;
    userResizableView.contentView = textView;
    userResizableView.preventsPositionOutsideSuperview = YES;
    userResizableView.preventsCustomButton = NO;
    [userResizableView setButton:ZDSTICKERVIEW_BUTTON_CUSTOM image:[UIImage imageNamed:@"Write.png"]];
    userResizableView.preventsResizing = NO;
    [userResizableView showEditingHandles];
    [self.editorView addSubview:userResizableView];
}

- (IBAction)textTranparancyChange:(UISlider *)sender {
    float sliderValue = [sender value];
    tempTextView.alpha=sliderValue;
}

- (IBAction)sizeTransparancyChange:(UISlider *)sender {
    float sliderValue = [sender value];
    [tempTextView setFont:[UIFont fontWithName:[FontPack findFont:selectFontItem] size:sliderValue]];
}

- (IBAction)leftAlignmentClick:(id)sender {
    tempTextView.textAlignment=NSTextAlignmentLeft;
}

- (IBAction)centerAlignmentClick:(id)sender {
    tempTextView.textAlignment=NSTextAlignmentCenter;
}

- (IBAction)rightAlignmentClick:(id)sender {
    tempTextView.textAlignment=NSTextAlignmentRight;
}

- (IBAction)canvasClick:(UIButton *)sender {
    NSUInteger buttonIndex = sender.tag;

    switch (buttonIndex) {
        case 0:
            self.drawingView.drawTool = ACEDrawingToolTypePen;
            break;
            
        case 1:
            self.drawingView.drawTool = ACEDrawingToolTypeLine;
            break;
            
        case 2:
            self.drawingView.drawTool = ACEDrawingToolTypeArrow;
            break;
            
        case 3:
            self.drawingView.drawTool = ACEDrawingToolTypeRectagleStroke;
            break;
            
        case 4:
            self.drawingView.drawTool = ACEDrawingToolTypeRectagleFill;
            break;
            
        case 5:
            self.drawingView.drawTool = ACEDrawingToolTypeEllipseStroke;
            break;
            
        case 6:
            self.drawingView.drawTool = ACEDrawingToolTypeEllipseFill;
            break;
            
        case 7:
            self.drawingView.drawTool = ACEDrawingToolTypeEraser;
            break;
            
        case 8:
            [self.drawingView undoLatestStep];
            [self updateButtonStatus];
            break;
        case 9:
            [self.drawingView redoLatestStep];
            [self updateButtonStatus];
            break;
        case 10:
            [self.drawingView clear];
            [self updateButtonStatus];
            break;
    }
}

- (void)updateButtonStatus {
     [self.drawingView canUndo];
     [self.drawingView canRedo];
}



#pragma mark - ZDSticker delegate functions

- (void)stickerViewDidLongPressed:(ZDStickerView *)sticker {
    NSLog(@"%s [%zd]",__func__, sticker.tag);
}

- (void)stickerViewDidClose:(ZDStickerView *)sticker {
    if (textStickerView.hidden == NO) {
         [UIView animateWithDuration:0.5f animations:^
         {
             lblLineText.frame = CGRectMake(ScreenWidth/5, textStickerView.frame.size.height-3, ScreenWidth/5, 2);
         }
         completion:^(BOOL finished)
         {
         }];
        
        fontPickerView.hidden = YES;
        itemCollectionView.hidden = YES;
        textStickerView.hidden = YES;
        borderStickerView.hidden = YES;
        self.fontAttributeView.hidden = YES;
        
        imageStickerView.hidden = YES;
    }
}

- (void)stickerViewDidCustomButtonTap:(ZDStickerView *)sticker {
    NSLog(@"%s [%zd]",__func__, sticker.tag);
    ((UITextView*)sticker.contentView).editable=YES;
    
     ((UITextView*)sticker.contentView).selectedRange = NSMakeRange([((UITextView*)sticker.contentView).text length], 0);

    [((UITextView*)sticker.contentView) becomeFirstResponder];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UIView *j in self.editorView.subviews){
        if([j isKindOfClass:[ZDStickerView class]]){
            ZDStickerView *selectSticker = (ZDStickerView *)j;
            [selectSticker hideCustomHandle];
            [selectSticker hideDelHandle];
            [selectSticker hideEditingHandles];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}




#pragma mark - Border

- (void)borderViewAdd {
    borderStickerView = [[UIView alloc]init];
    borderStickerView.frame = CGRectMake(0, ScreenHeight-216-50, ScreenWidth, 50);
    borderStickerView.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0];
    [self.view addSubview:borderStickerView];
    
    //Close Button
    UIImageView *imgClose = [[UIImageView alloc]init];
    imgClose.frame = CGRectMake(((ScreenWidth/5)-30)/2, 10, 30, 30);
    imgClose.image = [UIImage imageNamed:@"close"];
    [borderStickerView addSubview:imgClose];
    
    UIButton *closeButton = [[UIButton alloc]init];
    closeButton.frame = CGRectMake(0, 0, ScreenWidth/5, borderStickerView.frame.size.height);
    [closeButton addTarget:self action:@selector(closeClick) forControlEvents:UIControlEventTouchUpInside];
    [borderStickerView addSubview:closeButton];

    
    UIScrollView *backScroll = [[UIScrollView alloc]init];
    backScroll.frame=CGRectMake(closeButton.frame.size.width, 0, ScreenWidth-closeButton.frame.size.width, 50);
    [borderStickerView addSubview:backScroll];
    
    lblLineBorder = [[UILabel alloc]init];
    lblLineBorder.frame = CGRectMake(0, 50-3, ScreenWidth/5, 2);
    lblLineBorder.backgroundColor = [UIColor yellowColor];
    [backScroll addSubview:lblLineBorder];
    
    
    //Color Image Button
    borderType1 = [[UIButton alloc]init];
    borderType1.frame = CGRectMake(0, 0, ScreenWidth/5, backScroll.frame.size.height);
    [borderType1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [borderType1 addTarget:self action:@selector(type1Click) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgType1 = [[UIImageView alloc]init];
    imgType1.frame = CGRectMake((borderType1.frame.size.width/2)-15, 10, 30, 30);
    imgType1.image = [UIImage imageNamed:@"BorderIcon.png"];
    [backScroll addSubview:imgType1];
    [backScroll addSubview:borderType1];
    
    backScroll.contentSize = CGSizeMake((60*1)+(10*1), 44);
}

- (void)borderClick {
    
     [UIView animateWithDuration:0.5f animations:^
     {
         lblLine.frame = CGRectMake(ScreenWidth/4*2, self.bottomScrollView.frame.size.height-3, ScreenWidth/4, 2);
     }
     completion:^(BOOL finished)
     {
     }];

    for (UIView *j in self.editorView.subviews){
        if([j isKindOfClass:[ZDStickerView class]]){
            ZDStickerView *selectSticker = (ZDStickerView *)j;
            [selectSticker hideCustomHandle];
            [selectSticker hideDelHandle];
            [selectSticker hideEditingHandles];
        }
    }
    
    selectedType = @"border";
    itemCollectionView.hidden = NO;
    borderStickerView.hidden = NO;
    self.drawingView.userInteractionEnabled = NO;
    [itemCollectionView reloadData];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    textStickerView.hidden = NO;
    tempTextView = textView;
    
    for (UIView *j in self.editorView.subviews) {
        if([j isKindOfClass:[ZDStickerView class]]){
            ZDStickerView *selectSticker = (ZDStickerView *)j;
            
            if(selectSticker.tag == textView.tag)
            {
                [selectSticker showCustomHandle];
                [selectSticker showDelHandle];
                [selectSticker showEditingHandles];
            }
            else
            {
                [selectSticker hideCustomHandle];
                [selectSticker hideDelHandle];
                [selectSticker hideEditingHandles];
            }
        }
    }
    
    return YES;
}

- (void)type1Click {
     [UIView animateWithDuration:0.5f animations:^
     {
         lblLineBorder.frame = CGRectMake(borderType1.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
     completion:^(BOOL finished)
     {
         
     }];
}

- (void)type2Click {
    [UIView animateWithDuration:0.5f animations:^
     {
         lblLineBorder.frame = CGRectMake(borderType2.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
                     completion:^(BOOL finished)
     {
         
     }];

}




#pragma mark - Drawing View

- (void)lineViewAdd {
    
    self.lineWidthView.frame = CGRectMake(0, ScreenHeight-216, ScreenWidth, 216);
    self.lineWidthView.hidden = YES;
    [self.view addSubview:self.lineWidthView];
    
    self.lineTypeView.frame= CGRectMake(0, ScreenHeight-216, ScreenWidth, 216);
    self.lineTypeView.hidden = YES;
    [self.view addSubview:self.lineTypeView];
    
    lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(0, ScreenHeight-216-50, ScreenWidth, 50);
    lineView.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0];
    [self.view addSubview:lineView];
    
    //Close Button
    UIImageView *imgClose = [[UIImageView alloc]init];
    imgClose.frame = CGRectMake(((ScreenWidth/5)-30)/2, 10, 30, 30);
    imgClose.image = [UIImage imageNamed:@"close"];
    [lineView addSubview:imgClose];
    
    UIButton *closeButton = [[UIButton alloc]init];
    closeButton.frame = CGRectMake(0, 0, ScreenWidth/5, lineView.frame.size.height);
    [closeButton addTarget:self action:@selector(closeLineClick) forControlEvents:UIControlEventTouchUpInside];
    [lineView addSubview:closeButton];
    
    UIScrollView *backScroll = [[UIScrollView alloc]init];
    backScroll.frame=CGRectMake(closeButton.frame.size.width, 0, ScreenWidth-closeButton.frame.size.width, 50);
    backScroll.showsVerticalScrollIndicator=NO;
    backScroll.showsHorizontalScrollIndicator=NO;
    [lineView addSubview:backScroll];
    
    lblCanvasLine = [[UILabel alloc]init];
    lblCanvasLine.frame = CGRectMake(0, 50-3, ScreenWidth/5, 2);
    lblCanvasLine.backgroundColor = [UIColor yellowColor];
    [backScroll addSubview:lblCanvasLine];

    //Color Image Button
    btnColor = [[UIButton alloc]init];
    btnColor.frame = CGRectMake(0, 0, ScreenWidth/5, backScroll.frame.size.height);
    [btnColor setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnColor addTarget:self action:@selector(lineColor) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgColor = [[UIImageView alloc]init];
    imgColor.frame = CGRectMake((btnColor.frame.size.width/2)-15, 10, 30, 30);
    imgColor.image = [UIImage imageNamed:@"fontColor"];
    [backScroll addSubview:imgColor];
    [backScroll addSubview:btnColor];
    
    //Line Image Button
    btnLine = [[UIButton alloc]init];
    btnLine.frame = CGRectMake(btnColor.frame.origin.x + btnColor.frame.size.width, 0, ScreenWidth/5, backScroll.frame.size.height);
    [btnLine setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnLine addTarget:self action:@selector(drawLineClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgLine = [[UIImageView alloc]init];
    imgLine.frame = CGRectMake(btnLine.frame.origin.x +(btnLine.frame.size.width/2)-15, 10, 30, 30);
    imgLine.image = [UIImage imageNamed:@"Line_Icon"];
    [backScroll addSubview:imgLine];

    [backScroll addSubview:btnLine];
    
    
    //Eraser Image Button
    btnEraser = [[UIButton alloc]init];
    btnEraser.frame = CGRectMake(btnLine.frame.origin.x + btnLine.frame.size.width, 0, ScreenWidth/5, backScroll.frame.size.height);
    [btnEraser setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnEraser addTarget:self action:@selector(eraserClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgEraser = [[UIImageView alloc]init];
    imgEraser.frame = CGRectMake(btnEraser.frame.origin.x +(btnEraser.frame.size.width/2)-15, 10, 30, 30);
    imgEraser.image = [UIImage imageNamed:@"Line_width_height"];
    [backScroll addSubview:imgEraser];

    [backScroll addSubview:btnEraser];
    
    backScroll.contentSize = CGSizeMake(((ScreenWidth/5)*3), 50);
}

- (void)lineClick {
    
     [UIView animateWithDuration:0.5f animations:^
     {
         lblLine.frame = CGRectMake(ScreenWidth/4*3, self.bottomScrollView.frame.size.height-3, ScreenWidth/4, 2);
     }
     completion:^(BOOL finished)
     {
     }];
    
    
    for (UIView *j in self.editorView.subviews){
        if([j isKindOfClass:[ZDStickerView class]]){
            ZDStickerView *selectSticker = (ZDStickerView *)j;
            [selectSticker hideCustomHandle];
            [selectSticker hideDelHandle];
            [selectSticker hideEditingHandles];
        }
    }

    self.drawingView.userInteractionEnabled = YES;
    selectedType = @"LineColor";
    lineView.hidden = NO;
    itemCollectionView.hidden = NO;
    [itemCollectionView reloadData];
}

- (void)closeLineClick {
    
     [UIView animateWithDuration:0.5f animations:^
     {
         lblCanvasLine.frame = CGRectMake(btnColor.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
     completion:^(BOOL finished)
     {
     }];

    lineView.hidden = YES;
    self.lineTypeView.hidden = YES;
    itemCollectionView.hidden = YES;
    self.lineWidthView.hidden = YES;
}

- (void)drawLineClick {
     [UIView animateWithDuration:0.5f animations:^
     {
         lblCanvasLine.frame = CGRectMake(btnLine.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
     completion:^(BOOL finished)
     {
     }];

    self.lineTypeView.hidden = NO;
    self.lineWidthView.hidden = YES;
    itemCollectionView.hidden = YES;

    APPDELEGATE_SHARE.drawLineColor=@"1";
}

- (void)eraserClick {
    
    [UIView animateWithDuration:0.5f animations:^
     {
         lblCanvasLine.frame = CGRectMake(btnEraser.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
                     completion:^(BOOL finished)
     {
     }];

    self.lineWidthView.hidden = NO;
    self.lineTypeView.hidden = YES;
    itemCollectionView.hidden = YES;

    APPDELEGATE_SHARE.drawLineColor=@"0";
}

- (void)lineWidthViewClick {
    
    [UIView animateWithDuration:0.5f animations:^
     {
         lblCanvasLine.frame = CGRectMake(btnWidth.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
                     completion:^(BOOL finished)
     {
     }];

    self.lineTypeView.hidden = YES;
    self.lineWidthView.hidden = NO;
    itemCollectionView.hidden = YES;
}

- (void)lineColor {
    
    [UIView animateWithDuration:0.5f animations:^
     {
         lblCanvasLine.frame = CGRectMake(btnColor.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
                     completion:^(BOOL finished)
     {
     }];

    self.lineTypeView.hidden = YES;
    itemCollectionView.hidden = NO;
    self.lineWidthView.hidden = YES;
    selectedType = @"LineColor";
    [itemCollectionView reloadData];
}

- (void)resetLineViewClick {
    
     [UIView animateWithDuration:0.5f animations:^
     {
         lblCanvasLine.frame = CGRectMake(btnReset.frame.origin.x, 50-3, ScreenWidth/5, 2);
     }
     completion:^(BOOL finished)
     {
     }];
    self.lineTypeView.hidden = YES;
}

- (IBAction)lineSliderChange:(UISlider *)sender {
    self.drawingView.lineWidth = sender.value;
    
}

- (IBAction)alphaChange:(UISlider *)sender {
    self.drawingView.lineAlpha = sender.value;
}

#pragma mark - ACEDrawing View Delegate

- (void)drawingView:(ACEDrawingView *)view didEndDrawUsingTool:(id<ACEDrawingTool>)tool {
    [self updateButtonStatus];
}






#pragma mark - Color CollactionView

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    if ([selectedType isEqualToString:@"image"]) {
        return stickerArray.count;
    }
    else if ([selectedType isEqualToString:@"birthday"])
    {
        return stickerArray.count;
    }
    else if ([selectedType isEqualToString:@"BOOM"])
    {
        return stickerArray.count;
    }
    else if ([selectedType isEqualToString:@"GoldenBirthday"])
    {
        return stickerArray.count;
    }
    else if ([selectedType isEqualToString:@"LoveH"])
    {
        return stickerArray.count;
    }
    else if ([selectedType isEqualToString:@"border"])
    {
        return borderArray.count;
    }
    else
    {
        return [FontPack totalColour];
    }
    
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(<#CGFloat top#>, <#CGFloat left#>, <#CGFloat bottom#>, <#CGFloat right#>)
    
    if ([selectedType isEqualToString:@"image"]) {
        return UIEdgeInsetsMake(5, 5, 5, 5);
    }
    else if ([selectedType isEqualToString:@"birthday"])
    {
        return UIEdgeInsetsMake(5, 5, 5, 5);
    }
    else if ([selectedType isEqualToString:@"BOOM"])
    {
        return UIEdgeInsetsMake(5, 5, 5, 5);
    }
    else if ([selectedType isEqualToString:@"GoldenBirthday"])
    {
        return UIEdgeInsetsMake(5, 5, 5, 5);
    }
    else if ([selectedType isEqualToString:@"LoveH"])
    {
        return UIEdgeInsetsMake(5, 5, 5, 5);
    }
    else if ([selectedType isEqualToString:@"border"])
    {
        return UIEdgeInsetsMake(5, 5, 3, 5);
    }
    else
    {
        return UIEdgeInsetsMake(0,0,0,0);
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([selectedType isEqualToString:@"image"])
    {
        float newCellSize = (ScreenWidth/4)-5;
        return CGSizeMake(newCellSize,newCellSize);
    }
    else if ([selectedType isEqualToString:@"birthday"])
    {
        float newCellSize = (ScreenWidth/3)-5;
        return CGSizeMake(newCellSize,newCellSize);
    }
    else if ([selectedType isEqualToString:@"BOOM"])
    {
        float newCellSize = (ScreenWidth/3)-5;
        return CGSizeMake(newCellSize,newCellSize);
    }
    else if ([selectedType isEqualToString:@"GoldenBirthday"])
    {
        float newCellSize = (ScreenWidth/3)-5;
        return CGSizeMake(newCellSize,newCellSize+40);
    }
    else if ([selectedType isEqualToString:@"LoveH"])
    {
        return CGSizeMake(ScreenWidth,120);
    }
    else if ([selectedType isEqualToString:@"border"])
    {
        float newCellSize = (ScreenWidth/3)-5;
        return CGSizeMake(newCellSize,newCellSize);
    }
    else
    {
        return CGSizeMake(40,40);
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cell";
    StickerCell *cell = [cv dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if(cell == nil)
    {
        cell=[[StickerCell alloc]init];
    }

    if ([selectedType isEqualToString:@"image"])
    {
        cell.imgSticker.hidden = NO;
        cell.backgroundColor = [UIColor clearColor];
        cell.imgSticker.frame = CGRectMake(0, 0, (ScreenWidth/4)-5, (ScreenWidth/4)-5);
        cell.imgSticker.image = [UIImage imageNamed:[stickerArray objectAtIndex:indexPath.row]];
    }
    else if ([selectedType isEqualToString:@"birthday"])
    {
        cell.imgSticker.hidden = NO;
        cell.backgroundColor = [UIColor clearColor];
        cell.imgSticker.frame = CGRectMake(0, 0, (ScreenWidth/3)-5, (ScreenWidth/3)-5);
        cell.imgSticker.image = [UIImage imageNamed:[stickerArray objectAtIndex:indexPath.row]];
    }
    else if ([selectedType isEqualToString:@"BOOM"])
    {
        cell.imgSticker.hidden = NO;
        cell.backgroundColor = [UIColor clearColor];
        cell.imgSticker.frame = CGRectMake(0, 0, (ScreenWidth/3)-5, (ScreenWidth/3)-5);
        cell.imgSticker.image = [UIImage imageNamed:[stickerArray objectAtIndex:indexPath.row]];

    }
    else if ([selectedType isEqualToString:@"GoldenBirthday"])
    {
        cell.imgSticker.hidden = NO;
        cell.backgroundColor = [UIColor clearColor];
        
        if (indexPath.row>2) {
            cell.imgSticker.frame = CGRectMake(0, 0, 100, 100);
        }
        else
        {
            cell.imgSticker.frame = CGRectMake(0, 0, 100, 140);
        }
        
        cell.imgSticker.image = [UIImage imageNamed:[stickerArray objectAtIndex:indexPath.row]];
        
    }
    else if ([selectedType isEqualToString:@"LoveH"])
    {
        cell.imgSticker.hidden = NO;
        cell.backgroundColor = [UIColor clearColor];
        cell.imgSticker.frame = CGRectMake(5, 0, ScreenWidth-10, 117);
        cell.imgSticker.image = [UIImage imageNamed:[stickerArray objectAtIndex:indexPath.row]];

        UILabel *lblLinek = [[UILabel alloc]init];
        lblLinek.frame = CGRectMake(0, 119, ScreenWidth, 1);
        lblLinek.backgroundColor = [UIColor lightGrayColor];
        [cell addSubview:lblLinek];

    }
    else if ([selectedType isEqualToString:@"border"])
    {
        cell.imgSticker.hidden = NO;
        cell.backgroundColor = [UIColor clearColor];
        cell.imgSticker.frame = CGRectMake(0, 0, (ScreenWidth/3)-5, (ScreenWidth/3)-5);
        cell.imgSticker.image = [UIImage imageNamed:[borderArray objectAtIndex:indexPath.row]];
    }
    else if([selectedType isEqualToString:@"TextColor"])
    {
        cell.imgSticker.hidden = YES;
        cell.backgroundColor=[FontPack findColour:(int)indexPath.row];
    }
    else if([selectedType isEqualToString:@"LineColor"])
    {
        cell.imgSticker.hidden = YES;
        cell.backgroundColor=[FontPack findColour:(int)indexPath.row];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([selectedType isEqualToString:@"image"])
    {
        [self selectSticker:(int)indexPath.row];
    }
    else if ([selectedType isEqualToString:@"birthday"])
    {
        [self selectSticker:(int)indexPath.row];
    }
    else if ([selectedType isEqualToString:@"BOOM"])
    {
        [self selectSticker:(int)indexPath.row];
    }
    else if ([selectedType isEqualToString:@"GoldenBirthday"])
    {
        [self selectSticker:(int)indexPath.row];
    }
    else if ([selectedType isEqualToString:@"LoveH"])
    {
        [self selectSticker:(int)indexPath.row];
    }
    else if ([selectedType isEqualToString:@"border"])
    {
        [borderImageView removeFromSuperview];
        borderImageView = [[UIImageView alloc]init];

        if (indexPath.row == 0) {
            [borderImageView removeFromSuperview];
        }
        else
        {
            borderImageView.frame = CGRectMake(0, 0, self.editorView.frame.size.width, self.editorView.frame.size.height);
            borderImageView.image = [UIImage imageNamed:[borderArray objectAtIndex:indexPath.row]];
            [self.editorView addSubview:borderImageView];
        }
    }
    else if([selectedType isEqualToString:@"TextColor"])
    {
        [tempTextView setTextColor:[FontPack findColour:(int)indexPath.row]];
    }
    else if([selectedType isEqualToString:@"LineColor"])
    {
        UIColor *color = [FontPack findColour:(int)indexPath.row];
        self.drawingView.lineColor = color;
    }
}






#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 27;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    UILabel *lblFontType=[[UILabel alloc]init];
    lblFontType.text=@"Video Editor";
    [lblFontType setFont:[UIFont fontWithName:[FontPack findFont:row] size:36]];
    return lblFontType.text;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    selectFontItem = row;
    [tempTextView setFont:[UIFont fontWithName:[FontPack findFont:row] size:16]];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel* tView = (UILabel*)view;
    tView = [[UILabel alloc] init];
    [tView setFont:[UIFont fontWithName:[FontPack findFont:row] size:36]];
    tView.numberOfLines=1;
    [tView setTextAlignment:NSTextAlignmentCenter];
    tView.text=@"Video Editor";
    return tView;
}






#pragma mark - Play Video

- (void)playVideo {
    
    self.view.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0];
    
    // the video player
    self.avPlayer = [AVPlayer playerWithURL:self.videoUrl];
    self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    self.avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
    //self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avPlayer currentItem]];
    
    self.avPlayerLayer.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    [self.view.layer addSublayer:self.avPlayerLayer];
    [self.avPlayer play];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.avPlayer play];
}

- (void)viewDidDisappear:(BOOL)animated {
    //    [self.avPlayer seekToTime:CMTimeMake(0, 1)];
    [self.avPlayer pause];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

#pragma mark - Cancel OR Back

- (void)cancelButtonPressed:(UIButton *)button {
    [self invalidTimer];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - VIDEO EDITING

- (void)doneClick:(id)sender {
    
    [self invalidTimer];
    
    textStickerView.hidden = YES;
    imageStickerView.hidden = YES;
    itemCollectionView.hidden = YES;
    fontPickerView.hidden = YES;
    self.fontAttributeView.hidden = YES;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    for (UIView *j in self.editorView.subviews) {
        if([j isKindOfClass:[ZDStickerView class]]){
            ZDStickerView *selectSticker = (ZDStickerView *)j;
            [selectSticker hideCustomHandle];
            [selectSticker hideDelHandle];
            [selectSticker hideEditingHandles];
        }
    }

    for (UIView *j in self.editorView.subviews){
        if([j isKindOfClass:[ACEDrawingLabelView class]]){
            ACEDrawingLabelView *selectSticker = (ACEDrawingLabelView *)j;
            [selectSticker hideEditingHandles];
        }
    }

    [self.view endEditing:YES];
    [self captureCamera_VideoSave_Method];
}

- (void)captureCamera_VideoSave_Method {
    
    UIGraphicsBeginImageContext(CGSizeMake(self.editorView.frame.size.width,self.editorView.frame.size.height));
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.editorView.layer renderInContext:context];
    img_screenShot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    
    self.videoAsset = [[AVURLAsset alloc]initWithURL:_videoUrl options:nil];
    AVAssetTrack *videoAssetTrack = [[self.videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];

    if(ScreenHeight == 736 || ScreenHeight == 667)
    {
        img_screenShot = [self image:img_screenShot scaledToSize:CGSizeMake(videoAssetTrack.naturalSize.width, videoAssetTrack.naturalSize.height)];
    }
    
    [self videoOutput];
}

- (UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size {
    //avoid redundant drawing
    if (CGSizeEqualToSize(originalImage.size, size))
    {
        return originalImage;
    }
    
    //create drawing context
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
    
    //draw
    [originalImage drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];
    
    //capture resultant image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return image
    return image;
}

- (void)videoOutput {
    NSURL *urlx = _videoUrl;
    self.videoAsset = [[AVURLAsset alloc]initWithURL:urlx options:nil];

    // 1 - Early exit if there's no video file selected
    if (!self.videoAsset) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Load a Video Asset First" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    // 2 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
    
    // 3 - Video track
    AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, self.videoAsset.duration)
                        ofTrack:[[self.videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]
                         atTime:kCMTimeZero error:nil];
    
    // 3.1 - Create AVMutableVideoCompositionInstruction
    AVMutableVideoCompositionInstruction *mainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, self.videoAsset.duration);
    
    BOOL hasAudio = [self.videoAsset tracksWithMediaType:AVMediaTypeAudio].count > 0;
    
    if (hasAudio) {
        AVMutableCompositionTrack *compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        AVAssetTrack *clipAudioTrack = [[self.videoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
        [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, self.videoAsset.duration) ofTrack:clipAudioTrack atTime:kCMTimeZero error:nil];
    }
    
    // 3.2 - Create an AVMutableVideoCompositionLayerInstruction for the video track and fix the orientation.
    
    AVMutableVideoCompositionLayerInstruction *videolayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    AVAssetTrack *videoAssetTrack = [[self.videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    UIImageOrientation videoAssetOrientation_  = UIImageOrientationUp;
    BOOL isVideoAssetPortrait_  = NO;
    CGAffineTransform videoTransform = videoAssetTrack.preferredTransform;
    if (videoTransform.a == 0 && videoTransform.b == 1.0 && videoTransform.c == -1.0 && videoTransform.d == 0) {
        videoAssetOrientation_ = UIImageOrientationRight;
        isVideoAssetPortrait_ = YES;
    }
    if (videoTransform.a == 0 && videoTransform.b == -1.0 && videoTransform.c == 1.0 && videoTransform.d == 0) {
        videoAssetOrientation_ =  UIImageOrientationLeft;
        isVideoAssetPortrait_ = YES;
    }
    if (videoTransform.a == 1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == 1.0) {
        videoAssetOrientation_ =  UIImageOrientationUp;
    }
    if (videoTransform.a == -1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == -1.0) {
        videoAssetOrientation_ = UIImageOrientationDown;
    }
    [videolayerInstruction setTransform:videoAssetTrack.preferredTransform atTime:kCMTimeZero];
    [videolayerInstruction setOpacity:0.0 atTime:self.videoAsset.duration];
    
    // 3.3 - Add instructions
    mainInstruction.layerInstructions = [NSArray arrayWithObjects:videolayerInstruction,nil];
    
    AVMutableVideoComposition *mainCompositionInst = [AVMutableVideoComposition videoComposition];
    
    CGSize naturalSize;
    if(isVideoAssetPortrait_){
        naturalSize = CGSizeMake(videoAssetTrack.naturalSize.height, videoAssetTrack.naturalSize.width);
    } else {
        naturalSize = videoAssetTrack.naturalSize;
    }
    
    float renderWidth, renderHeight;
    renderWidth = naturalSize.width;
    renderHeight = naturalSize.height;
    mainCompositionInst.renderSize = CGSizeMake(renderWidth, renderHeight);
    mainCompositionInst.instructions = [NSArray arrayWithObject:mainInstruction];
    mainCompositionInst.frameDuration = CMTimeMake(1, 30);
    
    [self applyVideoEffectsToComposition:mainCompositionInst size:naturalSize];
    
    // 4 - Get path
   
    /*NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"StickerVideo.mov"]];
    
    */
    AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSString *filevideoNameName =[NSString stringWithFormat:@"%@_StickerVideo.mov",app.currentFileStartingName];
    app.currentFileName=filevideoNameName;
    
    NSString *myPathDocs = [app.rootFolderPath stringByAppendingPathComponent:filevideoNameName];
    
    NSLog(@"Sticker abd border videoOutput here app.currentFileName=%@ app.rootFolderPath=%@ path=%@",app.currentFileName,app.rootFolderPath,myPathDocs);

    
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error: &error];

    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
    
    // 5 - Create exporter
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition
                                                                      presetName:AVAssetExportPresetHighestQuality];
    exporter.outputURL=url;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.shouldOptimizeForNetworkUse = YES;
    exporter.videoComposition = mainCompositionInst;
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self exportDidFinish:exporter];
        });
    }];
}

- (void)applyVideoEffectsToComposition:(AVMutableVideoComposition *)composition size:(CGSize)size {
    
    // 1 - set up the overlay
    CALayer *overlayLayer = [CALayer layer];
    UIImage *overlayImage = nil;
    overlayImage = img_screenShot;
    
    NSLog(@"video width  : %f",size.width);  //video width  : 568.000000
    NSLog(@"video Height : %f",size.height); //video Height : 320.000000
    
    [overlayLayer setContents:(id)[overlayImage CGImage]];
    overlayLayer.frame = CGRectMake(0, 0, size.width, size.height);
    [overlayLayer setMasksToBounds:YES];
    
    // 2 - set up the parent layer
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
    videoLayer.frame  = CGRectMake(0, 0, size.width, size.height);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:overlayLayer];
    
    // 3 - apply magic
    composition.animationTool = [AVVideoCompositionCoreAnimationTool
                                 videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    
}

- (void)exportDidFinish:(AVAssetExportSession*)session {
    if (session.status == AVAssetExportSessionStatusCompleted) {
        NSURL *outputURL = session.outputURL;
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
           
            [self performSegueWithIdentifier:@"audioSegue" sender:outputURL];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            /*
            AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
            app.currentAssestInfo=[NSDictionary dictionaryWithObjectsAndKeys:outputURL,@"assetUrl",[self generateThumbImage:outputURL],@"thumbImage",@YES,@"fromLocal", nil];
            [self.navigationController popToRootViewControllerAnimated:YES];
            */
            
        }
    }
}


-(UIImage *)generateThumbImage : (NSURL *)url
{
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = [asset duration];
    time.value = 0;
    float duration = CMTimeGetSeconds([asset duration]);
    CGImageRef imgRef = [imageGenerator copyCGImageAtTime:CMTimeMake(1.0, duration) actualTime:NULL error:nil];
    UIImage* thumbnail = [[UIImage alloc] initWithCGImage:imgRef scale:UIViewContentModeScaleAspectFit orientation:UIImageOrientationUp];
    return thumbnail;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"audioSegue"]){
        AudioVC *controller = (AudioVC *)segue.destinationViewController;
        controller.videoUrl=sender;
        controller.isFrom=@"editingVideo";
    }
}



#pragma mark - Skip video Editing

- (void)skipClick:(id)sender {
    [self invalidTimer];
    [self performSegueWithIdentifier:@"audioSegue" sender:self.videoUrl];
}

#pragma mark - Advertise Method

- (void)invalidTimer {
    [timer1 invalidate];
    timer1 = nil;
}

- (void)insertialAds {
    [self createAndLoadInterstitial];
}

- (void)createAndLoadInterstitial {
    self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:AD_INTERSTITIAL_UNIT_ID];
    self.interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
    //request.testDevices = @[kGADSimulatorID,@"c8c2515ab174a9ce1943e7d0cfc244e7"];
    [self.interstitial loadRequest:request];
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    [self.interstitial presentFromRootViewController:self];
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"INSERTIAL ADS CLOSE");
    
    if (!isHide) {
        [self closeAllView];
    }
    
    timer1 = [NSTimer scheduledTimerWithTimeInterval:80.0
                                              target:self
                                            selector:@selector(insertialAds)
                                            userInfo:nil
                                             repeats:NO];
    
}

- (void)closeAllView {
    imageStickerView.hidden = YES;
    borderStickerView.hidden = YES;
    lineView.hidden = YES;
    fontPickerView.hidden = YES;
    
    itemCollectionView.hidden = YES;
    textStickerView.hidden = YES;
    borderStickerView.hidden = YES;
    self.fontAttributeView.hidden = YES;
    self.lineTypeView.hidden = YES;
    self.lineWidthView.hidden = YES;
    [self.view endEditing:YES];
    
}




#pragma mark - Other Method

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
   [super didReceiveMemoryWarning];
}

@end
