//
//  StickerCell.h
//  VideoMaker
//
//  Created by Sohil on 7/6/17.
//  Copyright © 2017 iAppz Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StickerCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgSticker;

@end
