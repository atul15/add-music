//  VideoEditing.h
//  VideoMaker
//  Created by Sohil on 4/23/17.
//  Copyright © 2017 iAppz Studio. All rights reserved.

#import <UIKit/UIKit.h>
#import "ZDStickerView.h"
#import "FontPack.h"
#import "StickerCell.h"
#import "ConstantFile.h"
#import "AudioVC.h"
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import "LTAVAssetUtils.h"

#import "ACEDrawingView.h"

//audioSegue
@import GoogleMobileAds;

@interface VideoEditing : UIViewController<ZDStickerViewDelegate,UIGestureRecognizerDelegate,MPMediaPickerControllerDelegate,AVAudioRecorderDelegate, AVAudioPlayerDelegate,AVAudioSessionDelegate,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,GADInterstitialDelegate,ACEDrawingViewDelegate>
{
    NSTimer *timer1;
    
    NSString *exportPath;
    UIImage *img_screenShot;
    BOOL isClose;
    
    UIView *stickerView;
    NSMutableArray *stickerArray,*borderArray,*emoticsArray;
    
    UIView *textStickerView,*imageStickerView,*borderStickerView;
    UIImageView *borderImageView;
    
    ZDStickerView *userResizableView;
    UITextView *textView,*tempTextView;

    int stickerTag;
    NSString *selectedType;
    
    UIImageView *imgUpDown;
    
    //Font , Color And Sticker CollectionView and PickerView
    IBOutlet UIPickerView *fontPickerView;
    IBOutlet UICollectionView *itemCollectionView;
    
    float videoFactor;
    UIView *lineView;
    
    //Text
    UILabel *lblLine,*lblLineText,*lblCanvasLine,*lblLineSticker,*lblLineBorder;
    
    //Sticker
    UIButton *btnEmotional,*btnBirthday,*btnGoldenBirthday,*btnBoom,*btnLove,*btnFace,*btnHLove;
    
    //Canvas
    UIButton *btnColor,*btnLine,*btnEraser,*btnReset,*btnWidth;
    
    //Border
    UIButton *borderType1,*borderType2;

    //For Set Edit View And Border Image in Video
    int videoWidth,videoHeight;
    int selectFontItem;
    
    BOOL isHide;
}

- (instancetype)initWithVideoUrl:(NSURL *)url;

@property(nonatomic, strong) GADInterstitial *interstitial;

@property (strong, nonatomic) UIView *editorView;
@property (strong, nonatomic) IBOutlet UISlider *lineSlider;

- (IBAction)lineSliderChange:(id)sender;
- (IBAction)sizeTransparancyChange:(UISlider *)sender;

//Line Color ETC
@property (strong, nonatomic) IBOutlet UIView *lineWidthView;
@property (strong, nonatomic) IBOutlet UILabel *lblLineTitle;


//ACEDrawingView
@property (nonatomic, unsafe_unretained) IBOutlet ACEDrawingView *drawingView;
@property (strong, nonatomic) IBOutlet UIView *lineTypeView;


//Bottom View
@property (strong, nonatomic)  UIView *bottomView;
@property (strong, nonatomic)  UIScrollView *bottomScrollView;
@property (strong, nonatomic)  UIButton *btnUpDown;
@property (strong, nonatomic) IBOutlet UIView *fontAttributeView;

//Playing Video
@property(nonatomic, strong) AVAsset *videoAsset;
@property (strong, nonatomic) NSURL *videoUrl;
@property (strong, nonatomic) AVPlayer *avPlayer;
@property (strong, nonatomic) AVPlayerLayer *avPlayerLayer;

@property (strong, nonatomic) IBOutlet UISlider *sliderTransperancy;
@property (strong, nonatomic) IBOutlet UISlider *sliderFontSize;
- (IBAction)textTranparancyChange:(id)sender;

- (IBAction)leftAlignmentClick:(id)sender;
- (IBAction)centerAlignmentClick:(id)sender;
- (IBAction)rightAlignmentClick:(id)sender;

- (IBAction)canvasClick:(UIButton *)sender;
- (IBAction)alphaChange:(UISlider *)sender;





@end
