//
//  ShareAndFeedbackVC.m
//  iIslamic
//
//  Created by amit  on 10/04/17.
//  Copyright © 2017 Sohil. All rights reserved.
//

#import "ShareAndFeedbackVC.h"

@interface ShareAndFeedbackVC ()

@end

@implementation ShareAndFeedbackVC


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"ShareAndFeedbackVC viewDidLoad");

    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    self.navigationController.navigationBar.translucent=NO;

    feedBackView.layer.cornerRadius=5.0;
    facebookView.layer.cornerRadius=5.0;
    twitterView.layer.cornerRadius=5.0;
    moreView.layer.cornerRadius=5.0;
    moreAppsView.layer.cornerRadius=5.0;
}


#pragma mark - Send Mail

- (IBAction)feedbackClick:(id)sender {
    [self sendFeedback];
}

- (void)sendFeedback {
    // Email Subject
    NSString *emailTitle = @"Video Editor - Feedback";
    // Email Content
    NSString *messageBody = @"";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"iappzstudio@gmail.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - Sharing

- (IBAction)faceBookClick:(id)sender {
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *controllerSLC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        NSString *msg= @"I found a great app for editing video.";
        [controllerSLC setInitialText:msg];
        [controllerSLC addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/video-editor-add-music-stickers-slow-motion/id1269223951?ls=1&mt=8"]];

        [self presentViewController:controllerSLC animated:YES completion:Nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't connected to Facebook right now, make sure your device has an internet connection and you have at least one Facebook account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (IBAction)twitterClick:(id)sender {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        NSString *msg  = @"I found a great app for editing video.";
        
        [tweetSheet setInitialText:msg];
        [tweetSheet addURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/video-editor-add-music-stickers-slow-motion/id1269223951?ls=1&mt=8"]];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }

}

- (IBAction)moreClcik:(id)sender {
    
    NSString *url=@"I found a great app for editing video. - \n https://itunes.apple.com/us/app/video-editor-add-music-stickers-slow-motion/id1269223951?ls=1&mt=8";
    
    UIActivityViewController* activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[url] applicationActivities:nil];
    
    [activityViewController setCompletionWithItemsHandler:
     ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError)
     {
         if (!completed) return;
         // do whatever you want to do after the activity view controller is finished
         if (completed) {
             NSLog(@"finish");
         }
     }];
    [self presentViewController:activityViewController animated:YES completion:nil];

}

- (IBAction)moreAppsClick:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/developer/jayesh-hirapara/id1203862850"]];
}

- (IBAction)linkClick:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.iappzstudio.com"]];
}


- (IBAction)backClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
