//
//  ShareAndFeedbackVC.h
//  iIslamic
//
//  Created by amit  on 10/04/17.
//  Copyright © 2017 Sohil. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>

@interface ShareAndFeedbackVC : UIViewController<MFMailComposeViewControllerDelegate>
{
    IBOutlet UIView *moreView;
    IBOutlet UIView *twitterView;
    IBOutlet UIView *facebookView;
    IBOutlet UIView *feedBackView;
    IBOutlet UIView *moreAppsView;
}

- (IBAction)feedbackClick:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *revealButtonItem;

- (IBAction)faceBookClick:(id)sender;
- (IBAction)twitterClick:(id)sender;
- (IBAction)moreClcik:(id)sender;
- (IBAction)moreAppsClick:(id)sender;
- (IBAction)linkClick:(id)sender;
- (IBAction)backClick:(id)sender;

@end
