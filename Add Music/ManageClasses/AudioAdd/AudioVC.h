//
//  AudioVC.h
//  VideoMaker
//
//  Created by Sohil on 7/8/17.
//  Copyright © 2017 iAppz Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstantFile.h"
#import "ShareVideoVC.h"
#import "SlowMotionVC.h"

//slowMotionSegue
//shareSegue
@import GoogleMobileAds;

@interface AudioVC : UIViewController<AVAudioPlayerDelegate,AVAudioRecorderDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,MPMediaPickerControllerDelegate,AVAudioRecorderDelegate, AVAudioPlayerDelegate,AVAudioSessionDelegate,GADInterstitialDelegate>

{
    NSTimer *timer1;
    
    NSMutableArray *dataArray,*audioFilePathArray,*playNumberArray;
    
    MPMediaItem *song;
    NSURL *exportURL;
    AVAudioPlayer *player;
    BOOL isSelectSong;
    
    // Recording Audio
    AVAudioRecorder *recorder;
    int sec,min;
    NSTimer *timeDuration;
    BOOL isRecording;
    
    NSString *strAudio;
}

@property(nonatomic, strong) GADInterstitial *interstitial;

@property (nonatomic, strong) NSString *isFrom;
@property (nonatomic, retain) NSData *audioData;
@property (nonatomic, strong) MPMusicPlayerController *musicPlayer;

@property (strong, nonatomic) IBOutlet UIView *topview;
@property (strong, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet UITableView *audioTableView;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;

//Audio recording View
@property (strong, nonatomic) IBOutlet UIView *recordingBackView;
@property (strong, nonatomic) IBOutlet UIView *audioView;
@property (strong, nonatomic) IBOutlet UIButton *btnStopRecording;
@property (strong, nonatomic) IBOutlet UILabel *timerLabel;
- (IBAction)stopRecording:(id)sender;


//Playing Video
@property (strong, nonatomic) NSURL *videoUrl;
@property (nonatomic, strong) AVAsset *videoAsset;
@property (nonatomic, strong) AVAsset *audeoAsset;
@property (strong, nonatomic) AVPlayer *avPlayer;
@property (strong, nonatomic) AVPlayerLayer *avPlayerLayer;


- (IBAction)skipClick:(id)sender;
- (IBAction)backClick:(id)sender;
- (IBAction)nextClick:(id)sender;

@end
