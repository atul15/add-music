//  AudioVC.m
//  VideoMaker
//  Created by Sohil on 7/8/17.
//  Copyright © 2017 iAppz Studio. All rights reserved.

#import "AudioVC.h"

@interface AudioVC ()

@end

@implementation AudioVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"AudioVC viewDidLoad");

    timer1 = [NSTimer scheduledTimerWithTimeInterval:70.0
                                              target:self
                                            selector:@selector(insertialAds)
                                            userInfo:nil
                                             repeats:NO];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self.audioTableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    dataArray = [[NSMutableArray alloc] init];
    [dataArray addObject:@"ORIGINAL"];
    [dataArray addObject:@"NONE"];
    
    playNumberArray = [[NSMutableArray alloc] init];
    [playNumberArray addObject:@"YES"];
    [playNumberArray addObject:@"NO"];
    
    audioFilePathArray = [[NSMutableArray alloc]init];

    self.audioTableView.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0];
    
    [self playVideo];
    [self setFrame];
    [self prepareToRecordAudio];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.avPlayer play];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self.avPlayer pause];
}

- (void)setFrame {
    self.videoView.frame = CGRectMake(0, 0, ScreenWidth, ScreenWidth);
    [self.view addSubview:self.topview];
    
    self.audioTableView.frame = CGRectMake(0, ScreenWidth, ScreenWidth, ScreenHeight-ScreenWidth-self.btnNext.frame.size.height);
    
    [self.view addSubview:self.recordingBackView];
    
    self.recordingBackView.hidden = YES;
    self.audioView.layer.cornerRadius=8.0;
    self.btnStopRecording.layer.cornerRadius=35;
}

- (void)prepareToRecordAudio {
    NSArray *pathComponents = [NSArray arrayWithObjects: [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject], @"MyAudioMemo.m4a",nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
    
    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute,
                             sizeof (audioRouteOverride),&audioRouteOverride);

}


#pragma mark - Play Video

- (void)playVideo {
    
    self.view.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0];
    // the video player
    self.avPlayer = [AVPlayer playerWithURL:self.videoUrl];
    self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    self.avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
    //self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avPlayer currentItem]];
    
    self.avPlayerLayer.frame = CGRectMake(0, 0, ScreenWidth, ScreenWidth);
    [self.view.layer addSublayer:self.avPlayerLayer];
    
    [self.avPlayer play];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}


#pragma mark TableView DataSource Method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    }
    else
    {
        return dataArray.count;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 35;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView* header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 35)];
    [header setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:175.0/255.0 blue:152.0/255.0 alpha:1.0]];
    
    UILabel *headerName=[[UILabel alloc]initWithFrame:CGRectMake(10, 3, ScreenWidth-10, 35-3)];
    [headerName setTextAlignment:NSTextAlignmentLeft];
    headerName.font = [UIFont fontWithName:ORATORSTD_FONT size:16.0];
    [header addSubview:headerName];
    
    
    if (section == 0) {
        headerName.text=@"SELECT ANY ONE";
    }
    else
    {
        headerName.text=@"CHOOSE AUDIO";
    }
    
    return header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = nil;
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor blackColor];
    
    
    
    if (indexPath.section == 0) {
        
        UIImageView *imgIcon = [[UIImageView alloc]init];
        imgIcon.frame = CGRectMake(10, 9, 28, 28);
        [cell addSubview:imgIcon];

        UILabel *lblType = [[UILabel alloc]init];
        lblType.frame = CGRectMake(55, 3, ScreenWidth-10, 44-3);
        lblType.textColor=[UIColor whiteColor];
        lblType.font = [UIFont fontWithName:ORATORSTD_FONT size:16.0];
        [cell addSubview:lblType];
        
        if (indexPath.row == 0) {
            lblType.text = @"CHOOSE FROM LIBRARY";
            imgIcon.image = [UIImage imageNamed:@"Library.png"];
        }
        else {
            lblType.text = @"RECORDING AUDIO";
            imgIcon.image = [UIImage imageNamed:@"Recorder.png"];
        }
    }
    else
    {
        UILabel *lblType = [[UILabel alloc]init];
        lblType.frame = CGRectMake(10, 3, ScreenWidth-10, 44-3);
        lblType.text = [dataArray objectAtIndex:indexPath.row];
        lblType.textColor=[UIColor whiteColor];
        lblType.font = [UIFont fontWithName:ORATORSTD_FONT size:16.0];
        [cell addSubview:lblType];
        
        if ([[playNumberArray objectAtIndex:indexPath.row]isEqualToString:@"YES"]) {
            UIImageView *imgCheckmark = [[UIImageView alloc]init];
            imgCheckmark.frame = CGRectMake(ScreenWidth-30, 15, 15, 13);
            imgCheckmark.image = [UIImage imageNamed:@"check-mark.png"];
            [cell addSubview:imgCheckmark];
        }
    }
    
    return cell;
}



#pragma mark TableView Delegate Method

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 
    if(indexPath.section == 0)
    {
        self.avPlayer.volume = 0;
        [player stop];
        
        if (indexPath.row == 0) {
            [self getAudioFromLibarary];
        }
        else
        {
            [self invalidTimer];
            self.recordingBackView.hidden = NO;
            [self microPhoneStart];
        }
    }
    else
    {
        for (int i =0; i<playNumberArray.count; i++) {
            [playNumberArray replaceObjectAtIndex:i withObject:@"NO"];
        }
        
        [playNumberArray replaceObjectAtIndex:indexPath.row withObject:@"YES"];

        [player stop];
        self.avPlayer.volume = 0;
        
        if (indexPath.row == 0) {
            [player stop];
            [self.avPlayer play];
            self.avPlayer.volume = 1;

        }
        else if (indexPath.row == 1) {
            [player stop];
            self.avPlayer.volume = 0;
        }
        
        if (indexPath.row > 1)
        {

            if([[dataArray objectAtIndex:indexPath.row] isEqualToString:@"SELECT SONG"])
            {
                strAudio = @"Select Song";

                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *createUrl = [NSString stringWithFormat:@"%@/%@",documentsDirectory,@"exported.m4a"];
                // NSLog(@"URL :%@",createUrl);
                
                player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL URLWithString:createUrl] error:nil];
                [player setDelegate:self];
                [player play];
            }
            else
            {
                strAudio = @"My Audio Recording";

                player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
                [player setDelegate:self];
                [player play];
                
            }
            
            [self.avPlayer play];
        }
    }
    
    [self.audioTableView reloadData];
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}




#pragma mark - Browse Audio from Device
- (void)getAudioFromLibarary {
    
    if ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone Simulator"]) {
        //device is simulator
        UIAlertView *alert1;
        alert1 = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"There is no Audio file in the Device" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
        alert1.tag=2;
        [alert1 show];
        //[alert1 release],alert1=nil;
    }else{
        
        MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes:MPMediaTypeMusic];
        mediaPicker.delegate = self;
        mediaPicker.allowsPickingMultipleItems = NO; // this is the default
        [self presentViewController:mediaPicker animated:YES completion:nil];
        
    }
    
}

#pragma mark - Media picker delegate methods

-(void)mediaPicker:(MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // Assign the selected item(s) to the music player and start playback.
    if ([mediaItemCollection count] < 1) {
        return;
    }
    song = [[mediaItemCollection items] objectAtIndex:0];
    [self handleExportTapped];
    
}

-(void)mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker {
    [self dismissViewControllerAnimated:YES completion:nil ];
}

-(void)handleExportTapped {
    
    // get the special URL
    if (! song) {
        return;
    }
    
    //[self startLoaderWithLabel:@"Preparing for upload..."];
    
    NSURL *assetURL = [song valueForProperty:MPMediaItemPropertyAssetURL];
    AVURLAsset *songAsset = [AVURLAsset URLAssetWithURL:assetURL options:nil];
    
    NSLog (@"Core Audio %@ directly open library URL %@",
           coreAudioCanOpenURL (assetURL) ? @"can" : @"cannot",
           assetURL);
    
    NSLog (@"compatible presets for songAsset: %@",
           [AVAssetExportSession exportPresetsCompatibleWithAsset:songAsset]);
    
    
    /* approach 1: export just the song itself
     */
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc]
                                      initWithAsset: songAsset
                                      presetName: AVAssetExportPresetAppleM4A];
    NSLog (@"created exporter. supportedFileTypes: %@", exporter.supportedFileTypes);
    exporter.outputFileType = @"com.apple.m4a-audio";
    NSString *exportFile = [myDocumentsDirectory() stringByAppendingPathComponent: @"exported.m4a"];
    // end of approach 1
    
    // set up export (hang on to exportURL so convert to PCM can find it)
    myDeleteFile(exportFile);
    //[exportURL release];
    exportURL = [NSURL fileURLWithPath:exportFile];
    exporter.outputURL = exportURL;
    
    // do the export
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        int exportStatus = exporter.status;
        switch (exportStatus) {
            case AVAssetExportSessionStatusFailed: {
                // log error to text view
                NSError *exportError = exporter.error;
                NSLog (@"AVAssetExportSessionStatusFailed: %@", exportError);
                //errorView.text = exportError ? [exportError description] : @"Unknown failure";
                //errorView.hidden = NO;
                //[self stopLoader];
                //[self showAlertWithMessage:@"There ia an error!"];
                break;
            }
            case AVAssetExportSessionStatusCompleted: {
                NSLog (@"AVAssetExportSessionStatusCompleted");
                
                NSURL *audioUrl = exportURL;
                self.audioData = [NSData dataWithContentsOfURL:audioUrl];
                
                if (!isSelectSong) {
                    isSelectSong = YES;
                    [audioFilePathArray addObject:audioUrl];
                    [dataArray addObject:@"SELECT SONG"];
                    [playNumberArray addObject:@"NO"];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [self.audioTableView reloadData];
                });
                
                break;
            }
            case AVAssetExportSessionStatusUnknown: {
                NSLog (@"AVAssetExportSessionStatusUnknown");
                //[self stopLoader];
                //[self showAlertWithMessage:@"There ia an error!"];
                break;
            }
            case AVAssetExportSessionStatusExporting: {
                NSLog (@"AVAssetExportSessionStatusExporting");
                //[self stopLoader];
                //[self showAlertWithMessage:@"There ia an error!"];
                break;
            }
            case AVAssetExportSessionStatusCancelled: {
                NSLog (@"AVAssetExportSessionStatusCancelled");
                //[self stopLoader];
                //[self showAlertWithMessage:@"There ia an error!"];
                break;
            }
            case AVAssetExportSessionStatusWaiting: {
                NSLog (@"AVAssetExportSessionStatusWaiting");
                //[self stopLoader];
                //[self showAlertWithMessage:@"There ia an error!"];
                break;
            }
            default: {
                NSLog (@"didn't get export status");
                //[self stopLoader];
                //[self showAlertWithMessage:@"There ia an error!"];
                break;
            }
        }
    }];
}

#pragma mark - conveniences

NSString* myDocumentsDirectory(){
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];;
    
}

void myDeleteFile (NSString* path){
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSError *deleteErr = nil;
        [[NSFileManager defaultManager] removeItemAtPath:path error:&deleteErr];
        if (deleteErr) {
            NSLog (@"Can't delete %@: %@", path, deleteErr);
        }
    }
}

static void CheckResult(OSStatus result, const char *operation) {
    
    if (result == noErr) return;
    
    char errorString[20];
    // see if it appears to be a 4-char-code
    *(UInt32 *)(errorString + 1) = CFSwapInt32HostToBig(result);
    if (isprint(errorString[1]) && isprint(errorString[2]) && isprint(errorString[3]) && isprint(errorString[4])) {
        errorString[0] = errorString[5] = '\'';
        errorString[6] = '\0';
    } else
        // no, format it as an integer
        sprintf(errorString, "%d", (int)result);
    
    fprintf(stderr, "Error: %s (%s)\n", operation, errorString);
    
    exit(1);
}





#pragma mark - core audio test

BOOL coreAudioCanOpenURL (NSURL* url) {
    
    OSStatus openErr = noErr;
    AudioFileID audioFile = NULL;
    openErr = AudioFileOpenURL((__bridge CFURLRef) url,
                               kAudioFileReadPermission ,
                               0,
                               &audioFile);
    if (audioFile) {
        AudioFileClose (audioFile);
    }
    return openErr ? NO : YES;
}

#pragma mark - Recording Audio

- (IBAction)stopRecording:(id)sender {
    
    timer1 = [NSTimer scheduledTimerWithTimeInterval:60.0
                                              target:self
                                            selector:@selector(insertialAds)
                                            userInfo:nil
                                             repeats:NO];

    self.recordingBackView.hidden = YES;
    [self microPhoneStop];
    [self.avPlayer play];
}

- (void)microPhoneStart {
    
    [self.avPlayer pause];

    if (player.playing) {
        [player stop];
    }
    
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        // Start recording
        [recorder record];
    }
    
    self.timerLabel.text=@"00:00";
    sec=min=0;

    timeDuration=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeChange) userInfo:nil repeats:YES];
}

- (void)microPhoneStop {
    
    [recorder stop];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    [timeDuration invalidate];
    timeDuration=nil;
    //isRecordingComplete=YES;
}

- (void)timeChange {
    sec++;
    if(sec>=60)
    {
        sec=0;
        min++;
        if (min>=60)
        {
            min=0;
        }
    }
    self.timerLabel.text=[NSString stringWithFormat:@"%.2i:%.2i",min,sec];
}

#pragma mark - AVAudioRecorderDelegate

- (void)itemDidFinishPlaying:(NSNotification *) notification {
    NSLog(@"Finish Audio");
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *createUrl = [NSString stringWithFormat:@"%@/%@",documentsDirectory,@"MyAudioMemo.m4a"];
    
    [audioFilePathArray addObject:createUrl];
    self.avPlayer.volume = 1;
    
    if (!isRecording) {
        isRecording=YES;
        [playNumberArray addObject:@"NO"];
        [dataArray addObject:@"MY AUDIO RECORDING"];
        [self.audioTableView reloadData];
    }
    
}





#pragma mark - Make Video

- (IBAction)nextClick:(id)sender {
    
    [self invalidTimer];
    
    [recorder stop];
    [player stop];
    [player prepareToPlay];
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if ([[playNumberArray objectAtIndex:0] isEqualToString:@"YES"])
    {
        NSURL *urlAudioPath = self.videoUrl;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([self.isFrom isEqualToString:@"Slowmotion"]) {
            [self toGoBackWithVideoUrl:urlAudioPath] ;// [self performSegueWithIdentifier:@"shareSegue" sender:urlAudioPath];
        }
        else {
            [self performSegueWithIdentifier:@"slowMotionSegue" sender:urlAudioPath];
        }

    }
    else if ([[playNumberArray objectAtIndex:1] isEqualToString:@"YES"])
    {
        [self videoOutput:self.videoUrl isSilent:YES];
    }
    else if ([[playNumberArray objectAtIndex:2] isEqualToString:@"YES"])
    {
        if ([strAudio isEqualToString:@"Select Song"]) {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory1 = [paths objectAtIndex:0];
            NSString *fullpath;
            fullpath = [documentsDirectory1 stringByAppendingPathComponent:@"exported.m4a"];
            [self videoOutput:[NSURL fileURLWithPath:fullpath] isSilent:NO];
        }
        else
        {
             [self videoOutput:[NSURL fileURLWithPath:[audioFilePathArray objectAtIndex:0]] isSilent:NO];
        }
    }
    else if ([[playNumberArray objectAtIndex:2] isEqualToString:@"YES"])
    {
        if ([strAudio isEqualToString:@"Select Song"]) {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory1 = [paths objectAtIndex:1];
            NSString *fullpath;
            fullpath = [documentsDirectory1 stringByAppendingPathComponent:@"exported.m4a"];
            [self videoOutput:[NSURL fileURLWithPath:fullpath] isSilent:NO];
        }
        else
        {
            [self videoOutput:[NSURL fileURLWithPath:[audioFilePathArray objectAtIndex:1]] isSilent:NO];
        }
 
    }
}

- (void)videoOutput:(NSURL *)fileURL isSilent:(BOOL)type {
    NSURL *urlx = _videoUrl;
    
    self.videoAsset = [[AVURLAsset alloc]initWithURL:urlx options:nil];
    
    // 1 - Early exit if there's no video file selected
    if (!self.videoAsset) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please Load a Video Asset First"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    // 2 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
    
    // 3 - Video track
    AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, self.videoAsset.duration)
                        ofTrack:[[self.videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]
                         atTime:kCMTimeZero error:nil];
    
    

    // If Audio Not Add on video TYPE = NO
    
    if (!type) {
        
        self.audeoAsset = [[AVURLAsset alloc]initWithURL:fileURL options:nil];

        BOOL hasAudio = [self.audeoAsset tracksWithMediaType:AVMediaTypeAudio].count > 0;
        
        if (hasAudio) {
            AVMutableCompositionTrack *compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
            
            AVAssetTrack *clipAudioTrack = [[self.audeoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
            [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, self.videoAsset.duration) ofTrack:clipAudioTrack atTime:kCMTimeZero error:nil];
            
        }
        
    }
    
    
    // 3.2 - Create an AVMutableVideoCompositionLayerInstruction for the video track and fix the orientation.
    AVMutableVideoCompositionLayerInstruction *videolayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    AVAssetTrack *videoAssetTrack = [[self.videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    UIImageOrientation videoAssetOrientation_  = UIImageOrientationUp;
    BOOL isVideoAssetPortrait_  = NO;
    CGAffineTransform videoTransform = videoAssetTrack.preferredTransform;
    if (videoTransform.a == 0 && videoTransform.b == 1.0 && videoTransform.c == -1.0 && videoTransform.d == 0) {
        videoAssetOrientation_ = UIImageOrientationRight;
        isVideoAssetPortrait_ = YES;
    }
    if (videoTransform.a == 0 && videoTransform.b == -1.0 && videoTransform.c == 1.0 && videoTransform.d == 0) {
        videoAssetOrientation_ =  UIImageOrientationLeft;
        isVideoAssetPortrait_ = YES;
    }
    if (videoTransform.a == 1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == 1.0) {
        videoAssetOrientation_ =  UIImageOrientationUp;
    }
    if (videoTransform.a == -1.0 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == -1.0) {
        videoAssetOrientation_ = UIImageOrientationDown;
    }
    [videolayerInstruction setTransform:videoAssetTrack.preferredTransform atTime:kCMTimeZero];
    [videolayerInstruction setOpacity:0.0 atTime:self.videoAsset.duration];
    
    // 3.1 - Create AVMutableVideoCompositionInstruction
    AVMutableVideoCompositionInstruction *mainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    mainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, self.videoAsset.duration);

    // 3.3 - Add instructions
    mainInstruction.layerInstructions = [NSArray arrayWithObjects:videolayerInstruction,nil];
    
    AVMutableVideoComposition *mainCompositionInst = [AVMutableVideoComposition videoComposition];
    
    CGSize naturalSize;
    if(isVideoAssetPortrait_){
        naturalSize = CGSizeMake(videoAssetTrack.naturalSize.height, videoAssetTrack.naturalSize.width);
    } else {
        naturalSize = videoAssetTrack.naturalSize;
    }
    
    float renderWidth, renderHeight;
    renderWidth = naturalSize.width;
    renderHeight = naturalSize.height;
    mainCompositionInst.renderSize = CGSizeMake(renderWidth, renderHeight);
    mainCompositionInst.instructions = [NSArray arrayWithObject:mainInstruction];
    mainCompositionInst.frameDuration = CMTimeMake(1, 30);
    
    //[self applyVideoEffectsToComposition:mainCompositionInst size:naturalSize];
    
    // 4 - Get path
  
    /*
     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:
                             [NSString stringWithFormat:@"FinalVideo.mov"]];
    
    */
    
    AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSString *filevideoNameName =[NSString stringWithFormat:@"%@_FinalVideo.mov",app.currentFileStartingName];
    app.currentFileName=filevideoNameName;
    
    NSString *myPathDocs = [app.rootFolderPath stringByAppendingPathComponent:filevideoNameName];
    
    
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:myPathDocs error: &error];
    
    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
    
    // 5 - Create exporter
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    
    exporter.outputURL=url;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.shouldOptimizeForNetworkUse = YES;
    exporter.videoComposition = mainCompositionInst;
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self exportDidFinish:exporter];
        });
    }];
}

- (void)exportDidFinish:(AVAssetExportSession*)session {
    
    NSLog(@"Sesstion Error : %@",session.error);
    
    if (session.status == AVAssetExportSessionStatusCompleted) {
        NSURL *outputURL = session.outputURL;
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
            
            if ([self.isFrom isEqualToString:@"Slowmotion"]) {
                [self toGoBackWithVideoUrl:outputURL] ;//[self performSegueWithIdentifier:@"shareSegue" sender:outputURL];
            }
            else {
                [self performSegueWithIdentifier:@"slowMotionSegue" sender:outputURL];
            }
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        else
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    }
}



- (IBAction)skipClick:(id)sender {
    [self invalidTimer];
    NSURL *urlAudioPath = self.videoUrl;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if ([self.isFrom isEqualToString:@"Slowmotion"])
    {
        [self toGoBackWithVideoUrl:urlAudioPath] ;// [self performSegueWithIdentifier:@"shareSegue" sender:urlAudioPath];
    }
    else {
        [self performSegueWithIdentifier:@"slowMotionSegue" sender:urlAudioPath];
    }
}

-(void)toGoBackWithVideoUrl:(NSURL*)outputURL
{
AppDelegate *app=(AppDelegate*)[UIApplication sharedApplication].delegate;
app.currentAssestInfo=[NSDictionary dictionaryWithObjectsAndKeys:outputURL,@"assetUrl",[self generateThumbImage:outputURL],@"thumbImage",@YES,@"fromLocal", nil];
[self.navigationController popToRootViewControllerAnimated:YES];
}
-(UIImage *)generateThumbImage : (NSURL *)url
{
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = [asset duration];
    time.value = 0;
    float duration = CMTimeGetSeconds([asset duration]);
    CGImageRef imgRef = [imageGenerator copyCGImageAtTime:CMTimeMake(1.0, duration) actualTime:NULL error:nil];
    UIImage* thumbnail = [[UIImage alloc] initWithCGImage:imgRef scale:UIViewContentModeScaleAspectFit orientation:UIImageOrientationUp];
    return thumbnail;
}

- (IBAction)backClick:(id)sender {
    [self invalidTimer];
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - Advertise Method

- (void)invalidTimer {
    [timer1 invalidate];
    timer1 = nil;
}

- (void)insertialAds {
    [self createAndLoadInterstitial];
}

- (void)createAndLoadInterstitial {
    self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:AD_INTERSTITIAL_UNIT_ID];
    self.interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
    //    request.testDevices = @[ kGADSimulatorID, @"c8c2515ab174a9ce1943e7d0cfc244e7" ];
    [self.interstitial loadRequest:request];
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    [self.interstitial presentFromRootViewController:self];
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"INSERTIAL ADS CLOSE");
    
    timer1 = [NSTimer scheduledTimerWithTimeInterval:60.0
                                              target:self
                                            selector:@selector(insertialAds)
                                            userInfo:nil
                                             repeats:NO];
    
}




- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"shareSegue"]){
        ShareVideoVC *controller = (ShareVideoVC *)segue.destinationViewController;
        controller.videoUrl=sender;
    }
    else if ([segue.identifier isEqualToString:@"slowMotionSegue"])
    {
        SlowMotionVC *controller = (SlowMotionVC *)segue.destinationViewController;
        controller.videoUrl=sender;

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
