//
//  VideoRecorderVC.h
//  VideoMaker
//
//  Created by Sohil on 6/11/17.
//  Copyright © 2017 iAppz Studio. All rights reserved.
//

//videoEditingSegue
//filterSegue
#import <UIKit/UIKit.h>
#import "LLSimpleCamera.h"
#import "VideoFilterVC.h"
#import "ConstantFile.h"


@interface VideoRecorderVC : UIViewController {
    int second,hour,minuite;
    NSTimer *timerVideo;
}

@property (strong, nonatomic) LLSimpleCamera *camera;
@property (strong, nonatomic) UILabel  *errorLabel;
@property (strong, nonatomic) UIButton *snapButton;
@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) UIButton *switchButton;
@property (strong, nonatomic) UIButton *flashButton;
@property (strong, nonatomic) UILabel  *timeLabel;


@end
