//  ShareVideoVC.m
//  VideoMaker
//  Created by Sohil on 7/9/17.
//  Copyright © 2017 iAppz Studio. All rights reserved.

#import "ShareVideoVC.h"

@interface ShareVideoVC ()

@end

@implementation ShareVideoVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    NSLog(@"ShareVideoVC viewDidLoad");

    timer1 = [NSTimer scheduledTimerWithTimeInterval:5.0
                                              target:self
                                            selector:@selector(insertialAds)
                                            userInfo:nil
                                             repeats:NO];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self playVideo];
    
    self.lblCoins.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"coins"];
}


#pragma mark - Play Video

- (void)playVideo {
    
    self.view.backgroundColor = [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0];
    // the video player
    self.avPlayer = [AVPlayer playerWithURL:self.videoUrl];
    self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    self.avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
    //self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avPlayer currentItem]];
    
    self.avPlayerLayer.frame = CGRectMake(0, 40, ScreenWidth, ScreenHeight-90-40);
    [self.view.layer addSublayer:self.avPlayerLayer];
    
    [self.avPlayer play];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}


#pragma mark - Save Video

- (IBAction)saveClick:(id)sender {
    [self saveVideo];
}

- (void)saveVideo{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSURL *outputURL = self.videoUrl;
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
        [library writeVideoAtPathToSavedPhotosAlbum:outputURL completionBlock:^(NSURL *assetURL, NSError *error){
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"
                                                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
                }
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        }];
    }
}

#pragma mark - Share Video

- (IBAction)shareClick:(id)sender {
    
        NSArray *activityItems = [NSArray arrayWithObjects: self.videoUrl, nil];
        UIActivityViewController* activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        
        [activityViewController setCompletionWithItemsHandler:
         ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError)
         {
             if (!completed) return;
             // do whatever you want to do after the activity view controller is finished
             if (completed)
             {
             }
         }];
        [self presentViewController:activityViewController animated:YES completion:nil];
}

#pragma mark - Send Mail

- (IBAction)mailClick:(id)sender {
    [self shareVideoViaMail];
}

- (void)shareVideoViaMail {
    
    NSString *extention = [NSString stringWithFormat:@"%@",self.videoUrl];
    NSString *theFileName = [extention lastPathComponent];
    NSData *fileData = [NSData dataWithContentsOfURL:self.videoUrl];

    
    // Email Subject
    NSString *emailTitle = @"Download this Video!";
    // Email Content
    NSString *messageBody = @"";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@""];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc addAttachmentData:fileData mimeType:@"video/quicktime" fileName:theFileName];

    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Message 

- (IBAction)messageClick:(id)sender {
    
    NSString *message = [NSString stringWithFormat:@"Download this Video!"];
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setBody:message];
    
    if ([MFMessageComposeViewController canSendAttachments]) {
        NSLog(@"Attachments Can Be Sent.");
        NSString *filePath=[self.videoUrl absoluteString];
        NSData *videoData = [NSData dataWithContentsOfURL:[NSURL URLWithString:filePath]];
        BOOL didAttachVideo = [messageController addAttachmentData:videoData typeIdentifier:@"public.movie" filename:filePath];
        
        if (didAttachVideo) {
            NSLog(@"Video Attached.");
            
        } else {
            NSLog(@"Video Could Not Be Attached.");
        }
    }
    
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if (result == MessageComposeResultCancelled)
        NSLog(@"Message cancelled");
    else if (result == MessageComposeResultSent)
        NSLog(@"Message sent");
    else
        NSLog(@"Message failed");
}

#pragma mark - Twiiter

- (IBAction)twitterClick:(id)sender {
    
    
        NSData *fileData = [NSData dataWithContentsOfURL:self.videoUrl];
        
        // Request access to the Twitter accounts
        
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        
        [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error){
            if (granted) {
                
                NSArray *accounts = [accountStore accountsWithAccountType:accountType];
                
                NSLog(@"Info : %@",accounts);
                
                if (accounts.count>0) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    });
                    
                    
                    // Check if the users has setup at least one Twitter account
                    ACAccount *twitterAccount = [accounts objectAtIndex:0];
                    
                    [SocialVideoHelper uploadTwitterVideo:fileData comment:@"" account:twitterAccount withCompletion:^(BOOL success, NSString *errorMessage) {
                        
                        if (success) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [MBProgressHUD hideHUDForView:self.view animated:YES];
                            });
                            
                            NSLog(@"TWitter Video Share Success");
                        }
                        else
                        {
                            NSLog(@"TWitter Video Share Fail");
                        }
                    }];
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIAlertView *alertView = [[UIAlertView alloc]
                                                  initWithTitle:@"Sign in Required"
                                                  message:@"Please sign in to your twitter account."
                                                  delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
                        [alertView show];
                    });
                }
            } else {
                NSLog(@"No access granted");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertView *alertView = [[UIAlertView alloc]
                                              initWithTitle:@"Twitter"
                                              message:@"Twitter app requesting permission to access granted."
                                              delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
                    [alertView show];
                });
                
            }
        }];
}


#pragma mark - Facebook Apps

- (IBAction)facebookClick:(id)sender {
    [self saveToPhotoAlbum:_videoUrl];
    
}

- (void)saveToPhotoAlbum:(NSURL*)url {
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    ALAssetsLibraryWriteVideoCompletionBlock videoWriteCompletionBlock =
    ^(NSURL *newURL, NSError *error) {
        if (error) {
            NSLog( @"Error writing image with metadata to Photo Library: %@", error );
        } else {
            NSLog( @"Wrote image with metadata to Photo Library %@", newURL.absoluteString);
            url_new  = newURL;
            NSLog(@"Save Video URL : %@",url_new);
        }
    };
    
    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:url]) {
        [library writeVideoAtPathToSavedPhotosAlbum:url completionBlock:^(NSURL *assetURL, NSError *error){
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"
                                                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                }
                else
                {
                    NSLog(@"-----Save Video------");
                    FBSDKShareDialog *shareDialog = [[FBSDKShareDialog alloc] init];
                    NSURL *videoURL = assetURL;
                    FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
                    video.videoURL = videoURL;
                    FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
                    content.video = video;
                    shareDialog.shareContent = content;
                    shareDialog.delegate = self;
                    [shareDialog show];
                }
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        }];
    }
}

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    NSLog(@"Result : %@",results);
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    NSLog(@"Sharing Cancel");
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    NSLog(@"Sharing Error : %@",error);
}

- (IBAction)backClick:(id)sender {
    [self invalidTimer];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark - Advertise Method

- (void)invalidTimer {
    [timer1 invalidate];
    timer1 = nil;
}

- (void)insertialAds {
    [self createAndLoadInterstitial];
}

- (void)createAndLoadInterstitial {
    self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:AD_INTERSTITIAL_UNIT_ID];
    self.interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
    //    request.testDevices = @[ kGADSimulatorID, @"c8c2515ab174a9ce1943e7d0cfc244e7" ];
    [self.interstitial loadRequest:request];
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    [self.interstitial presentFromRootViewController:self];
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"INSERTIAL ADS CLOSE");
    
    timer1 = [NSTimer scheduledTimerWithTimeInterval:60.0
                                              target:self
                                            selector:@selector(insertialAds)
                                            userInfo:nil
                                             repeats:NO];
    
}


- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
