//  ShareVideoVC.h
//  VideoMaker
//  Created by Sohil on 7/9/17.
//  Copyright © 2017 iAppz Studio. All rights reserved.

#import <UIKit/UIKit.h>
#import "ConstantFile.h"
#import "SocialVideoHelper.h"
#import "Reachability.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <MessageUI/MFMessageComposeViewController.h>


@import GoogleMobileAds;

@interface ShareVideoVC : UIViewController<GADRewardBasedVideoAdDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,FBSDKSharingDelegate,GADInterstitialDelegate>
{
    NSTimer *timer1;
    NSURL *url_new;
}

@property(nonatomic, strong) GADInterstitial *interstitial;

@property (strong, nonatomic) IBOutlet UIView *upperView;
@property (strong, nonatomic) IBOutlet UIView *videoView;

//Playing Video
@property (strong, nonatomic) NSURL *videoUrl;
@property (nonatomic, strong) AVAsset *videoAsset;
@property (nonatomic, strong) AVAsset *audeoAsset;
@property (strong, nonatomic) AVPlayer *avPlayer;
@property (strong, nonatomic) AVPlayerLayer *avPlayerLayer;
@property (strong, nonatomic) IBOutlet UILabel *lblCoins;


- (IBAction)saveClick:(id)sender;
- (IBAction)mailClick:(id)sender;
- (IBAction)messageClick:(id)sender;
- (IBAction)twitterClick:(id)sender;
- (IBAction)facebookClick:(id)sender;

- (IBAction)shareClick:(id)sender;
- (IBAction)backClick:(id)sender;


@end
