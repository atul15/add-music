//
//  ProjectCollectionViewCell.m
//  Add Music
//
//  Created by Puneet Kalra on 06/01/19.
//  Copyright © 2019 Atul. All rights reserved.
//

#import "ProjectCollectionViewCell.h"

@implementation ProjectCollectionViewCell
@synthesize previewImageView,nameLabel,dtLabel;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        previewImageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self.contentView addSubview:previewImageView];
        previewImageView.image=nil;
        [previewImageView setContentMode:UIViewContentModeScaleAspectFill];
        [previewImageView setClipsToBounds:YES];
        previewImageView.tag=1;
        
        
        nameLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 20)];
        [self addSubview:nameLabel];
        nameLabel.tag=2;
        [nameLabel setTextColor:[UIColor whiteColor]];
        
        
        dtLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 20)];
        [self addSubview:dtLabel];
        dtLabel.tag=3;
        [dtLabel setTextColor:[UIColor whiteColor]];
        

        
        
    }
    return self;
}
@end
