//
//  ViewController.m
//  Add Music
//
//  Created by Puneet Kalra on 06/01/19.
//  Copyright © 2019 Atul. All rights reserved.
//

#import "ViewController.h"
#import "MyCustomLayoutHorizontal.h"
#import "ProjectCollectionViewCell.h"

#import "TopHeaderScroller.h"
#import "MyMainCollectionViewCell.h"
#import "MyCustomLayoutUGC.h"
#import <MobileCoreServices/MobileCoreServices.h>

#import "VideoFilterVC.h"
#import "VideoRecorderVC.h"
#import "AppDelegate.h"
#import "ProjectEditorClass.h"


#import "AudioSourceSelector.h"

@interface ViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate,ProjectUpdateDelegate>
{
    NSIndexPath *currentIndex;
    CGSize scrSz;
    UIImageView *imgSplashTimeLogo;
    NSTimer* startAnimationTimer;
    UIScrollView *topScrollView;
   // UICollectionView *myProjectList;
    NSArray *addListFromServer;
    NSArray *projectListFromServer;
    NSArray *projectListFromServerDt;
    
    UICollectionView *myCollectionView;
    CGSize scrSize;
    TopHeaderScroller *headerView;
    NSMutableArray *myProjectList;
    UIButton *btnLibrary;
    UIImagePickerController *videoPicker;
    AppDelegate *app;

}
@property (weak, nonatomic) IBOutlet UIView *topSideBaseView;
@property (weak, nonatomic) IBOutlet UIImageView *splashBg;
@property (weak, nonatomic) IBOutlet UIView *mainBottomView;
@property (weak, nonatomic) IBOutlet UIView *myProjectView;

@end


@implementation ViewController


- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}


-(UIImage *)generateThumbImage : (NSURL *)url
{
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = [asset duration];
    time.value = 0;
    float duration = CMTimeGetSeconds([asset duration]);
    CGImageRef imgRef = [imageGenerator copyCGImageAtTime:CMTimeMake(1.0, duration) actualTime:NULL error:nil];
    UIImage* thumbnail = [[UIImage alloc] initWithCGImage:imgRef scale:UIViewContentModeScaleAspectFit orientation:UIImageOrientationUp];
    return thumbnail;
}

-(void)writeThumb:(UIImage*)img toFile:(NSString*)thumbFile
{
    NSString *strThumb=[NSString stringWithFormat:@"th_%@.jpg",app.currentFileStartingName];
    
    NSString *path_Thumb= [app.rootFolderPath stringByAppendingString:strThumb];
    
    //[[NSFileManager defaultManager] createFileAtPath:path_video contents:file attributes:nil];
    
    [UIImageJPEGRepresentation(img, 1.0) writeToFile:path_Thumb atomically:YES];

    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  //  if(app.currentAssestInfo!=nil)
        
    if(app.currentFileName!=nil)
    {
        
    
        NSString *outputFilePath = [app.rootFolderPath stringByAppendingPathComponent:app.currentFileName];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:outputFilePath])
        {
            
           UIImage *im= [self generateThumbImage:[NSURL fileURLWithPath:outputFilePath]];
            
             NSString *strThumb=[NSString stringWithFormat:@"th_%@.jpg",app.currentFileStartingName];
            
            [self writeThumb:im toFile:strThumb];
            NSDate *dt=[NSDate date];
            NSDictionary *dct=nil;
            
            NSString *strnm=nil;
            if(myProjectList.count==1)
            {
         
                strnm=@"My Pro -  1";
            }
            else
            {
                
                strnm=[NSString stringWithFormat:@"My Pro -  %lu",myProjectList.count+1];

            }
             dct=[NSDictionary dictionaryWithObjectsAndKeys:app.currentFileName,@"fileName",strnm,@"projectName",dt,@"creationDate",strThumb,@"thumbName",@YES,@"fromLocal", nil];
            
        NSLog(@"have to set new project");
        [app writeCurrentProjectWithFileName:app.currentFileName creationDate:dt andProjectName:strnm andThumbName:strThumb];
        [myProjectList insertObject:dct atIndex:0];
            app.currentFileName=nil;

  //UIImage *imag=(UIImage*)[app.currentAssestInfoobjectForKey:@"thumbImage"];
       // [btnLibrary setBackgroundImage:imag forState:UIControlStateNormal];
        [self addNewAtLast];
        }
    }
    
   // app.currentAssestInfo=[NSDictionary dictionaryWithObjectsAndKeys:outputURL,@"assetUrl",img_screenShot,"thumbImage",@YES,@"fromLocal", nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"viewDidLoad viewDidLoad");

    scrSz=UIScreen.mainScreen.bounds.size;
    app=(AppDelegate*)[UIApplication sharedApplication].delegate;
    app.currentFileName=nil;
    NSArray *arr=nil;
    arr=[app getStoredProjectList];
    NSLog(@"arr=%@",arr);
    myProjectList=nil;

    if(arr==nil||arr.count==0)
    myProjectList=[[NSMutableArray alloc] init];
    else
    myProjectList=[arr mutableCopy];

    NSLog(@"myProjectList=%@",myProjectList);

    headerView=nil;
    scrSize=UIScreen.mainScreen.bounds.size;
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"canStartAnimation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    imgSplashTimeLogo=[[UIImageView alloc] initWithFrame:CGRectMake(scrSz.width/2-(120/2), scrSz.height/2-(112/2), 120, 112)];
    imgSplashTimeLogo.image=[UIImage imageNamed:@"Icon.pdf"];
    [self.view addSubview:imgSplashTimeLogo];
    
    startAnimationTimer=nil;
    startAnimationTimer=[NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(waitForShowImageView) userInfo:nil repeats:YES];
    [UIView animateWithDuration:0.3 animations:^{
        
        self->imgSplashTimeLogo.transform = CGAffineTransformMakeScale(1.6, 1.6);
    }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:2.0 animations:^{
                             self->imgSplashTimeLogo.transform = CGAffineTransformMakeScale(1, 1);
                            
                             [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"canStartAnimation"];
                             [[NSUserDefaults standardUserDefaults] synchronize];
                             
                         }];
                     }];
}

-(void)waitForShowImageView
{
    NSLog(@"waitForShowImageView");
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"canStartAnimation"])
    {
        [startAnimationTimer invalidate];
        startAnimationTimer=nil;
        [self toRemoveSplash];
    }
}
-(void)toRemoveSplash
{
    NSLog(@"toRemoveSplash");
   [_mainBottomView setHidden:NO];
    [_splashBg removeFromSuperview];
    _splashBg=nil;
    [imgSplashTimeLogo removeFromSuperview];
    imgSplashTimeLogo=nil;
    
    MyCustomLayoutUGC *myLayout=[[MyCustomLayoutUGC alloc] init];
    
    
    NSLog(@"topSideBaseView top=%f topSideBaseView height=%f",_topSideBaseView.frame.origin.y,_topSideBaseView.frame.size.height);
    myCollectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, _topSideBaseView.frame.origin.y+_topSideBaseView.frame.size.height, scrSize.width, _mainBottomView.frame.size.height-(_topSideBaseView.frame.origin.y+_topSideBaseView.frame.size.height)) collectionViewLayout:myLayout];
    
    
    [myProjectList addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"New",@"ProjectName",@YES,@"fromLocal", nil]];
    
    
    [myCollectionView setDataSource:self];
    [myCollectionView setDelegate:self];
    
    [myCollectionView registerClass:[MyMainCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [myCollectionView setBackgroundColor:[UIColor blackColor]];
    
    [_mainBottomView addSubview:myCollectionView];
    
    
    btnLibrary=[[UIButton alloc] initWithFrame:CGRectMake(30, _mainBottomView.frame.size.height-50, _mainBottomView.frame.size.width-60, 40)];
    [btnLibrary setBackgroundImage:[UIImage imageNamed:@"New Project button.pdf"] forState:UIControlStateNormal];
    [btnLibrary setTitle:@"New Project" forState:UIControlStateNormal];
    [[btnLibrary titleLabel] setFont:[UIFont systemFontOfSize:18 weight:UIFontWeightSemibold]];
    [btnLibrary addTarget:self action:@selector(addNewProject) forControlEvents:UIControlEventTouchUpInside];
    [_mainBottomView addSubview:btnLibrary];

    
    //[self setBasicUiTask];
}




-(void)addNewProject
{
    videoPicker=[[UIImagePickerController alloc] init];
    [videoPicker setMediaTypes:@[(NSString *)kUTTypeMovie]];
    videoPicker.delegate=self;
   videoPicker.allowsEditing = YES;
    videoPicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:videoPicker animated:YES completion:^{
        [self->headerView toStartStopTimer:YES];
    }];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [headerView toStartStopTimer:NO];
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerController *,id> *)info
{
    
    NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
    NSData *file = [NSData dataWithContentsOfURL:videoURL];
    
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    app.currentFileName = [dateFormatter stringFromDate:now];
    app.currentFileStartingName=app.currentFileName;
    
    
    NSString *fileName =[NSString stringWithFormat:@"%@.mov",app.currentFileName];// @"/selectVideo.mov";
    app.currentFileName=fileName;
    /*NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path_video = [path objectAtIndex:0];
    path_video = [path_video stringByAppendingString:fileName];*/
    NSString *path_video= [app.rootFolderPath stringByAppendingString:fileName];
        
    [[NSFileManager defaultManager] createFileAtPath:path_video contents:file attributes:nil];
    
  //[self performSegueWithIdentifier:@"filterSegue" sender:path_video];

        [self performSegueWithIdentifier:@"controllertoaudiosource" sender:videoURL];
    });
   
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"controllertoaudiosource"])
    {
        AudioSourceSelector *controller = (AudioSourceSelector *)segue.destinationViewController;
        controller.selectedVideoUrl=sender;
    }
    
    else if([segue.identifier isEqualToString:@"filterSegue"])
    {
        VideoFilterVC *controller = (VideoFilterVC *)segue.destinationViewController;
        controller.videoUrl=[NSURL fileURLWithPath:sender];
    }
    else if([segue.identifier isEqualToString:@"projecteditor"])
    {
        ProjectEditorClass *controller = (ProjectEditorClass *)segue.destinationViewController;
        controller.selectedDictionary=sender;
        controller.selectedIndex=currentIndex;
        controller.projectUpdateDelegate=self;
    }
    
}
-(void)projectDetailModifiew:(NSDictionary*)dct forIndex:(NSIndexPath*)idx
{
    [app updateProjectInfo:dct forIndex:(int)idx.row-2];
    [myProjectList replaceObjectAtIndex:idx.row-2 withObject:dct];
    [headerView toStartStopTimer:YES];
    [headerView removeFromSuperview];
    headerView=nil;
    [myCollectionView reloadData];
}
-(void)addNewAtLast
{
    if(myProjectList.count>1)
    {
        [headerView toStartStopTimer:YES];
        [headerView removeFromSuperview];
        headerView=nil;
        [myCollectionView reloadData];
        
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2+myProjectList.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // MyMainCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    
    __block MyMainCollectionViewCell *cell=(MyMainCollectionViewCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    cell.tag=indexPath.row+100;
    
    cell.backView.frame=CGRectMake(0,0, cell.frame.size.width, cell.frame.size.height);
    
    
    cell.mainContainerView.frame=CGRectMake(3, 2, cell.frame.size.width-6, cell.frame.size.height-4);
    
    cell.imgViewPreview.hidden=YES;
    cell.imgViewPreview.image=nil;
    if(indexPath.row==0) //for header view at 0
    {
        [cell.backView setBackgroundColor:[UIColor whiteColor]];
        [cell.mainContainerView setBackgroundColor:[UIColor whiteColor]];
        cell.lblName.text=@"";
        if(headerView==nil)
        {
            NSLog(@"headerView==nil");
            headerView=[[TopHeaderScroller alloc] initWithFrame:CGRectMake(2, 2, cell.frame.size.width-4, cell.frame.size.height-4)];
            if(cell.tag==100)
            {
                [cell.mainContainerView addSubview:headerView];
            }
            
        }
        
        
        if([cell.mainContainerView isHidden])
        {
            NSLog(@"mainContainerView isHidden");
            
        }
        else
        {
            NSLog(@"mainContainerView is not Hidden");
        }
    }
    else if(indexPath.row==1) //for label
    {
        [cell.backView setBackgroundColor:[UIColor blackColor]];
        [cell.mainContainerView setBackgroundColor:[UIColor blackColor]];
        cell.lblName.frame=CGRectMake(10, cell.mainContainerView.frame.size.height/2-20, cell.mainContainerView.frame.size.width-40, 40);
        cell.lblName.text=@"My Projects";
        [cell.lblName setFont:[UIFont systemFontOfSize:25 weight:UIFontWeightBold]];
        [cell.lblName setTextColor:[UIColor whiteColor]];
    }
    else
    {
        [cell.lblName setFont:[UIFont systemFontOfSize:22 weight:UIFontWeightBold]];

        NSDictionary *dc=[myProjectList objectAtIndex:indexPath.row-2];
        [cell.backView setBackgroundColor:[UIColor blackColor]];
        cell.mainContainerView.frame=CGRectMake(6, 4, cell.frame.size.width-12, cell.frame.size.height-8);
        
        [cell.mainContainerView.layer setCornerRadius:10];
        [cell.mainContainerView setClipsToBounds:YES];
        cell.mainContainerView.contentMode = UIViewContentModeScaleAspectFill;
        
        
        [cell.mainContainerView setBackgroundColor:[UIColor whiteColor]];
        
        cell.imgViewPreview.frame=CGRectMake(6, 4, cell.frame.size.width-12, cell.frame.size.height-8);
        [cell.imgViewPreview.layer setCornerRadius:10];
        cell.imgViewPreview.contentMode = UIViewContentModeScaleAspectFill;
        [cell.imgViewPreview setClipsToBounds:YES];

        cell.lblName.frame=CGRectMake(20, cell.mainContainerView.frame.size.height-35, cell.mainContainerView.frame.size.width-40, 30);
        cell.lblName.text=[dc objectForKey:@"projectName"];
        NSLog(@"dc=%@ row=%d name=",dc,indexPath.row);
        cell.imgViewPreview.hidden=YES;
        
        if([[dc objectForKey:@"fromLocal"] isEqual:@YES]&&indexPath.row<1+myProjectList.count)
        {
            NSLog(@"in set name");
          
            [cell.lblName setTextColor:[UIColor whiteColor]];
            
            NSString *path_Thumb= [app.rootFolderPath stringByAppendingString:[dc objectForKey:@"thumbName"]];

            cell.imgViewPreview.image=[UIImage imageWithContentsOfFile:path_Thumb];
            
            cell.imgViewPreview.hidden=NO;
         //   cell.imgViewPreview.image=(UIImage*)[dc objectForKey:@"thumbImage"];
        }
        else
        {
        cell.imgViewPreview.image=nil;
        //cell.lblName.backgroundColor=[UIColor redColor];
        cell.lblName.text=@"New Project";

        }
    }
    [cell.mainContainerView setHidden:NO];
    
    
    return cell;
 
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didSelectItemAtIndexPath =%d",indexPath.row);
    
    if(indexPath.row>1)
    {
        NSDictionary *dc=[myProjectList objectAtIndex:indexPath.row-2];
        NSLog(@"didSelectItemAtIndexPath =%@",dc);
        if([[dc allKeys] containsObject:@"thumbName"])
        {
            NSLog(@"show player abd title edit");
            
            currentIndex=indexPath;
            [self performSegueWithIdentifier:@"projecteditor" sender:dc];

            /*
             ProjectEditorClass *prjEdit=[[ProjectEditorClass alloc] init];
            prjEdit.selectedDictionary=dc;
            prjEdit.selectedIndex=indexPath.row-2;
            [self.navigationController pushViewController:prjEdit animated:YES];
             */
        }
      }
}
// app.currentAssestInfo=[NSDictionary dictionaryWithObjectsAndKeys:outputURL,@"assetUrl",img_screenShot,"thumbImage",@YES,@"fromLocal", nil];
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(MyCustomLayoutUGC *)collectionViewLayout preferredSizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
        return CGSizeMake(_mainBottomView.frame.size.width, _mainBottomView.frame.size.width*(3.0/4.0));
    else if(indexPath.row==1)
        return CGSizeMake(_mainBottomView.frame.size.width, 70);
    else
        return CGSizeMake(_mainBottomView.frame.size.width/2, _mainBottomView.frame.size.width/2);
    
}







/*
-(void)setBasicUiTask
{
    topScrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, _topSideBaseView.frame.size.width, _topSideBaseView.frame.size.height)];
    [_topSideBaseView addSubview:topScrollView];
    
    addListFromServer=@[@"Image from server.pdf",@"Image from server.pdf",@"Image from server.pdf"];
    
    projectListFromServer=@[@"Project 1",@"Project 2",@"Project 3",@"Project 4",@"Project 5",@"Project 6"];
    
    projectListFromServerDt=@[@"01 Jan,2019",@"02 Jan,2019",@"03 Jan,2019",@"03 Jan,2019",@"04 Jan,2019",@"05 Jan,2019"];
    int i=0;
    for(NSString * str in addListFromServer)
    {
        UIButton *btn=[[UIButton alloc] initWithFrame:CGRectMake(5+(i*topScrollView.frame.size.width), 0, topScrollView.frame.size.width-10, topScrollView.frame.size.height)];
        [topScrollView addSubview:btn];
        [btn setBackgroundImage:[UIImage imageNamed:str] forState:UIControlStateNormal];
        [btn setContentMode:UIViewContentModeScaleAspectFill];
        i++;
    }
    
    [topScrollView setContentSize:CGSizeMake(topScrollView.frame.size.width*i, topScrollView.frame.size.height)];
    topScrollView.bounces=NO;
    topScrollView.pagingEnabled=YES;
    
    MyCustomLayoutHorizontal *myLayout=[[MyCustomLayoutHorizontal alloc] init];
    myProjectList=[[UICollectionView alloc] initWithFrame:CGRectMake(0,0,_myProjectView.frame.size.width,_myProjectView.frame.size.height) collectionViewLayout:myLayout];
    
    [myProjectList setDataSource:self];
    [myProjectList setDelegate:self];
    
    [myProjectList registerClass:[ProjectCollectionViewCell class] forCellWithReuseIdentifier:@"cell20"];
    [myProjectList setBackgroundColor:[UIColor clearColor]];
    
    [_myProjectView addSubview:myProjectList];

    
}

# pragma collectionview function

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return projectListFromServer.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    __block ProjectCollectionViewCell *cell=(ProjectCollectionViewCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"cell20" forIndexPath:indexPath];
    
    cell.previewImageView.frame=CGRectMake(5, 5, cell.frame.size.width-10, cell.frame.size.height-10);
    [cell.previewImageView.layer setCornerRadius:10];
    [cell.previewImageView setClipsToBounds:YES];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = cell.previewImageView.bounds;
    
    gradient.colors = @[(id)[UIColor colorWithRed:207.0/255.0 green:119.0/255.0 blue:253.0/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:39.0/255.0 green:156.0/255.0 blue:246.0/255.0 alpha:1.0].CGColor];
    [cell.previewImageView.layer insertSublayer:gradient atIndex:0];
    
    
    cell.nameLabel.frame=CGRectMake(25, cell.frame.size.height-65, cell.frame.size.width-25, 40);
    cell.nameLabel.text=[projectListFromServer objectAtIndex:indexPath.row];
    [cell.nameLabel setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightSemibold]];

    cell.dtLabel.frame=CGRectMake(25, cell.frame.size.height-30, cell.frame.size.width-25, 20);
    cell.dtLabel.text=[projectListFromServerDt objectAtIndex:indexPath.row];
    [cell.dtLabel setFont:[UIFont systemFontOfSize:18 weight:UIFontWeightRegular]];

    

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(MyCustomLayoutHorizontal *)collectionViewLayout preferredSizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake((myProjectList.frame.size.width/2),myProjectList.frame.size.height);

}

*/


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
