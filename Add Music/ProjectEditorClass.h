//
//  ProjectEditorClass.h
//  Add Music
//
//  Created by Mac-Mini on 15/01/19.
//  Copyright © 2019 Atul. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol ProjectUpdateDelegate <NSObject>
-(void)projectDetailModifiew:(NSDictionary*)dct forIndex:(NSIndexPath*)idx;
@end

@interface ProjectEditorClass : UIViewController
{
    id <ProjectUpdateDelegate> _projectUpdateDelegate;
}
@property (nonatomic, strong) id <ProjectUpdateDelegate>projectUpdateDelegate;

@property(nonatomic)NSDictionary *selectedDictionary;
@property(nonatomic)NSIndexPath *selectedIndex;
@end

NS_ASSUME_NONNULL_END
