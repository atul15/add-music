//
//  MyCustomLayoutUGC.m
//  Messagif
//
//  Created by Puneet Kalra on 18/05/18.
//  Copyright © 2018 Aaplabs. All rights reserved.
//

#import "MyCustomLayoutUGC.h"
#define INSET_TOP3 0
#define INSET_LEFT3 0
#define INSET_BOTTOM3 0
#define INSET_RIGHT3 0


@interface MyCustomLayoutUGC()
{
    CGRect **_itemFrameSections;
    NSInteger _numberOfItemFrameSections;
    NSMutableArray *leftArray,*rightArray,*finalArray;
    NSString *currentSearchString;
}
@property (nonatomic) CGSize contentSize;

@property (nonatomic, strong) NSArray *headerFrames;
@property (nonatomic, strong) NSArray *footerFrames;
@property (nonatomic) NSDictionary *layoutInformation;
@property (nonatomic) NSInteger maxNumRows;
@property (nonatomic) UIEdgeInsets insets;

@end

@implementation MyCustomLayoutUGC
-(id)init {
    if(self = [super init]) {
        self.insets = UIEdgeInsetsMake(INSET_TOP3, INSET_LEFT3, INSET_BOTTOM3, INSET_RIGHT3);
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    
    leftArray=[[NSMutableArray alloc] init];
    rightArray=[[NSMutableArray alloc] init];
    finalArray=[[NSMutableArray alloc] init];
    self.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.minimumLineSpacing = 0;
    self.minimumInteritemSpacing = 0;
    
    self.footerReferenceSize = CGSizeZero;
    self.scrollDirection = UICollectionViewScrollDirectionVertical;
}

- (void)prepareLayout
{
    [super prepareLayout];
    [self clearItemFrames];
    [self helloCalculateAndStoreFrame];
    
    NSLog(@"prepareLayout _numberOfItemFrameSections");
    
    
}

-(void)helloCalculateAndStoreFrame
{
    
    CGFloat idealHeight = self.preferredRowSize;
    idealHeight = CGRectGetHeight(self.collectionView.bounds) / 3.0;
    
    float lftTop,rtTop;
    lftTop=self.minimumLineSpacing;
    rtTop=self.minimumLineSpacing;
  //  currentSearchString=[self.delegate getSearchStringName];
    for(int i=0;i<[self.collectionView numberOfItemsInSection:0];i++)
    {
        
        CGSize actualSize = [self.delegate collectionView:self.collectionView layout:self preferredSizeForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
        
        CGRect frame;
        //frame = CGRectMake(self.minimumLineSpacing, lftTop, actualSize.width, actualSize.height);
        //lftTop+= actualSize.height + self.minimumInteritemSpacing;

       if(i==0||i==1)
        {
            frame = CGRectMake(self.minimumLineSpacing, lftTop, actualSize.width, actualSize.height);
            lftTop+= actualSize.height + self.minimumInteritemSpacing;
            rtTop+= actualSize.height + self.minimumInteritemSpacing;
            
            [leftArray addObject:[NSValue valueWithCGRect:frame]];
            
        }
        else
        {
            if(i%2==0)
            {
                frame = CGRectMake(self.minimumLineSpacing, lftTop, actualSize.width, actualSize.height);
                lftTop+= actualSize.height + self.minimumInteritemSpacing;
                [leftArray addObject:[NSValue valueWithCGRect:frame]];
                
                
            }
            else
            {
                frame = CGRectMake(self.minimumLineSpacing*2+self.collectionView.frame.size.width/2-3, rtTop, actualSize.width, actualSize.height);
                
                rtTop+= actualSize.height + self.minimumInteritemSpacing;
                [rightArray addObject:[NSValue valueWithCGRect:frame]];
                
            }
            
        }
        
        [finalArray addObject:[NSValue valueWithCGRect:frame]];
        
    }
    if(lftTop>rtTop)
        self.contentSize = CGSizeMake(self.collectionView.frame.size.width, self.minimumLineSpacing+lftTop);
    else
       self.contentSize = CGSizeMake(self.collectionView.frame.size.width, self.minimumLineSpacing+rtTop);
    
    
    
}


- (NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
    
    NSMutableArray *layoutAttributes = [NSMutableArray array];
    
    NSLog(@"layoutAttributesForElementsInRect");
    
    for (int i = 0; i < [self.collectionView numberOfItemsInSection:0]; i++) {
        CGRect itemFrame=[[finalArray objectAtIndex:i] CGRectValue];
        if (CGRectIntersectsRect(rect, itemFrame))
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            [layoutAttributes addObject:[self layoutAttributesForItemAtIndexPath:indexPath]];
        }
    }
    
    
    
    
    return layoutAttributes;
    
}
- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    
    attributes.frame =[[finalArray objectAtIndex:indexPath.row] CGRectValue];
    //   NSLog(@"layoutAttributesForItemAtIndexPath indexpath=%ld attributes.frame x=%f y =%f width=%f height=%f",(long)indexPath.row,attributes.frame.origin.x,attributes.frame.origin.y,attributes.frame.size.width,attributes.frame.size.height);
    
    return attributes;
}




- (void)clearItemFrames
{
    [finalArray removeAllObjects];

    
}

- (void)dealloc
{
    [self clearItemFrames];
}


- (CGSize)collectionViewContentSize {
    return self.contentSize;
    
}
#pragma mark - Delegate

- (id<MyCustomProtocol>)delegate
{
    return (id<MyCustomProtocol>)self.collectionView.delegate;
}

@end
