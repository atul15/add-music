//  ConstantFile.h
//  VideoMaker
//  Created by Sohil on 7/9/17.
//  Copyright © 2017 iAppz Studio. All rights reserved.


#ifndef ConstantFile_h
#define ConstantFile_h

#import "AppDelegate.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/ALAsset.h>
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioServices.h>
#import <AudioToolbox/AudioServices.h>
#import "MBProgressHUD.h"

#define ScreenWidth  [UIScreen mainScreen].bounds.size.width
#define ScreenHeight [UIScreen mainScreen].bounds.size.height
#define APPDELEGATE_SHARE  ((AppDelegate *)[[UIApplication sharedApplication]delegate])

//True
#define AD_BANNER_UNIT_ID         @"ca-app-pub-5183339170462669/8148378916"
#define AD_INTERSTITIAL_UNIT_ID   @"ca-app-pub-5183339170462669/3535180632"
#define AD_VIDEO_UNIT_ID          @"ca-app-pub-5183339170462669/2355341432"


#define ORATORSTD_FONT            @"OratorStd"

#endif /* ConstantFile_h */
