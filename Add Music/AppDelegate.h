//
//  AppDelegate.h
//  Add Music
//
//  Created by Puneet Kalra on 06/01/19.
//  Copyright © 2019 Atul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "ConstantFile.h"

@import GoogleMobileAds;
@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property(nonatomic)NSDictionary *currentAssestInfo;

@property (strong, nonatomic) UIWindow *window;
@property(strong, nonatomic) NSString *drawLineColor;
@property(nonatomic) NSString *projectMediaInfoFilePath,*currentFileName,*currentFileStartingName,*rootFolderPath;
-(void)writeCurrentProjectWithFileName:(NSString*)fileName creationDate:(NSDate*)dtOfCreation andProjectName:(NSString*)projectName andThumbName:(NSString*)strThumb;
-(NSArray*)getStoredProjectList;
-(void)updateProjectInfo:(NSDictionary*)projectDict forIndex:(int)idx;
@end

