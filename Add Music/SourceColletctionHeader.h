//
//  SourceColletctionHeader.h
//  RecordAndUpload
//
//  Created by Mac-Mini on 28/01/19.
//  Copyright © 2019 Fortune. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SourceColletctionHeader : UICollectionReusableView
@property(nonatomic) UILabel *lbl;
@end

NS_ASSUME_NONNULL_END
